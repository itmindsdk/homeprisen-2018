﻿/// <binding ProjectOpened='watch, sass' />
/*
This file in the main entry point for defining Gulp tasks and using Gulp plugins.
Click here to learn more. http://go.microsoft.com/fwlink/?LinkId=518007
*/

require('es6-promise').polyfill();
var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var browsersync = require('browser-sync').create();

gulp.task('sass', function() {
    gulp.src('Content/Site.scss')
        .pipe(sass({
            includePaths: ['Content/site/**/'],
            outputStyle: 'compressed'
            }
        ).on('error', sass.logError))
        .pipe(autoprefixer(
            "last 1 version",
            "> 1%",
            "ie 8",
            "ie 7"
        ))
        .pipe(gulp.dest('Content'))
        .pipe(browsersync.stream());
});

gulp.task('browsersync-init', function(done) {
    browsersync.init({
        proxy: {
            target: 'http://localhost:52665',
            middleware: function(req, res, next) {
                res.setHeader('Access-Control-Allow-Origin', '*');
                next();
            }
        }
    }, done);
});

gulp.task('sass-watch', ['sass'], function(done) {
    done();
});

gulp.task('watch', ['browsersync-init'], function () {

    // watch for sass changes
    gulp.watch('Content/**/*.scss', ['sass-watch']);

    gulp.watch('Views/**/*.cshtml').on('change', browsersync.reload);

    gulp.watch(['scrips/**/*.js', 'scripts/site/**/*.js']).on('change', browsersync.reload);
});