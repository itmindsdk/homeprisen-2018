﻿using AutoMapper;
using Core.ApplicationServices;
using Core.DomainModel;
using Core.DomainServices;
using Infrastructure.DataAccess;
using Presentation.Web.App_Start;
using Presentation.Web.Models.Nominate;
using Presentation.Web.Models.Nominee;
using Presentation.Web.Models.NomineeAdmin;
using System.Web;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(MappingConfig), "Start")]

namespace Presentation.Web.App_Start
{
    /// <summary>
    /// Automapper is a convention-based object-object mapper.
    /// It's used to map properties from your domain models to your view models and vice versa.
    /// Using the same name for properties in both your domain and view models is highly recommended,
    /// as this will allow for convention-based mapping without any setup.
    /// Sometimes more complex configrations are needed, which can also be configured here.
    /// AutoMapper configuration wiki: https://github.com/AutoMapper/AutoMapper/wiki/Configuration
    /// </summary>
    public class MappingConfig
    {
        public static void Start()
        {
            Mapper.Initialize(cfg =>
            {
                // Write your AutoMapper configurations here.
                cfg.CreateMap<IUnitOfWork, UnitOfWork>().ReverseMap();

                // ViewModel Mappings
                cfg.CreateMap<IldsjaelNominee, NominateIldsjaelViewModel>().ReverseMap()
                    .ForMember(dest => dest.Picture, opt => opt.Ignore())
                    .ReverseMap()
                    .ForMember(dest => dest.Picture, opt => opt.Ignore());

                cfg.CreateMap<ForeningNominee, NominateForeningViewModel>().ReverseMap()
                    .ForMember(dest => dest.Picture, opt => opt.Ignore())
                    .ReverseMap()
                    .ForMember(dest => dest.Picture, opt => opt.Ignore());

                cfg.CreateMap<IldsjaelNominee, AcceptIldsjaelViewModel>()
                    .ForMember(dest => dest.Picture, opt => opt.Ignore())
                    .ReverseMap()
                    .ForMember(dest => dest.Picture, opt => opt.Ignore())
                    .ForMember(dest => dest.Description, opt => opt.Ignore())
                    .ForMember(dest => dest.NominatorName, opt => opt.Ignore());

                cfg.CreateMap<ForeningNominee, AcceptForeningViewModel>()
                    .ForMember(dest => dest.Picture, opt => opt.Ignore())
                    .ReverseMap()
                    .ForMember(dest => dest.Picture, opt => opt.Ignore())
                    .ForMember(dest => dest.NominatorName, opt => opt.Ignore());

                cfg.CreateMap<Vote, NomineeVotesViewModel>();
                cfg.CreateMap<Vote, ContestVotesViewModel>();

                //.ForMember(dest => dest.Picture, opt => opt.MapFrom(src => src.Picture));
                cfg.CreateMap<HttpPostedFileBase, AcceptIldsjaelViewModel>().ReverseMap();
                cfg.CreateMap<IldsjaelNominee, IldsjaelDetailsViewModel>().ReverseMap();
                cfg.CreateMap<ForeningNominee, ForeningDetailsViewModel>().ReverseMap();
                cfg.CreateMap<Nominee, NomineeAdminViewModel>().ReverseMap();
                cfg.CreateMap<IldsjaelNominee, IldsjaelNomineeAdminViewModel>().ReverseMap();
                cfg.CreateMap<ForeningNominee, ForeningNomineeAdminViewModel>().ReverseMap();
                cfg.CreateMap<Contest, ContestAdminViewModel>().ReverseMap();
                cfg.CreateMap<Comment, CommentAdminViewModel>().ReverseMap();

                cfg.CreateMap<ZipCodeRealtorService.RealtorPayload, Realtor>()
                    .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Organisationsnavn))
                    .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.Emailadresse));
            });
        }
    }
}
