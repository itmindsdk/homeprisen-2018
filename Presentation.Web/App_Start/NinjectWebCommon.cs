using AutoMapper;
using Core.ApplicationServices.Facebook;
using Core.ApplicationServices.Images;
using Core.ApplicationServices.NomineeFiles;
using Core.ApplicationServices.Pdf;
using Core.ApplicationServices.Poster;
using Ganss.XSS;
using System.IO;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(Presentation.Web.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(Presentation.Web.App_Start.NinjectWebCommon), "Stop")]

namespace Presentation.Web.App_Start
{
    using Controllers;
    using Core.ApplicationServices;
    using Core.DomainServices;
    using Infrastructure.DataAccess;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using Microsoft.AspNet.Identity.Owin;
    using Microsoft.Owin.Security;
    using Microsoft.Web.Infrastructure.DynamicModuleHelper;
    using Ninject;
    using Ninject.Web.Common;
    using Postal;
    using System;
    using System.Web;
    using System.Web.Configuration;
    using System.Web.SessionState;
    public static class NinjectWebCommon
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start()
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }

        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }

        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            var baseDir = AppDomain.CurrentDomain.BaseDirectory;

            var twitterIldsjaelShareMessage = WebConfigurationManager.AppSettings["TwitterIldsjaelShareMessage"];
            var twitterForeningShareMessage = WebConfigurationManager.AppSettings["TwitterForeningShareMessage"];

            var defaultPictureIldsjael = WebConfigurationManager.AppSettings["MissingImageUrlIldsjael"];
            var defaultPictureForening = WebConfigurationManager.AppSettings["MissingImageUrlForening"];
            var baseUrl = new Uri(WebConfigurationManager.AppSettings["BaseUrl"]);
            var maxUploadSize = int.Parse(WebConfigurationManager.AppSettings["MaxUploadSizeMb"]);

            var useRealtorMailService = bool.Parse(WebConfigurationManager.AppSettings["UseRealtorMailService"]);

            // Ildsjael Banners URL and path
            var ildsjaelFrontpageBannerUrl = WebConfigurationManager.AppSettings["IldsjaelFrontpageBannerUrl"];
            var ildsjaelFrontpageBannerPath = Path.Combine(baseDir, WebConfigurationManager.AppSettings["IldsjaelFrontpageBannerPath"]);
            var ildsjaelAboutBannerUrl = WebConfigurationManager.AppSettings["IldsjaelAboutBannerUrl"];
            var ildsjaelAboutBannerPath = Path.Combine(baseDir, WebConfigurationManager.AppSettings["IldsjaelAboutBannerPath"]);

            // Forening Banners URL and path
            var foreningFrontpageBannerUrl = WebConfigurationManager.AppSettings["ForeningFrontpageBannerUrl"];
            var foreningAboutBannerUrl = WebConfigurationManager.AppSettings["ForeningAboutBannerUrl"];

            kernel.Bind<ApplicationContext>().ToSelf().InRequestScope();
            kernel.Bind<HttpContext>().ToMethod(c => HttpContext.Current);
            kernel.Bind<HttpSessionState>().ToMethod(c => HttpContext.Current.Session);
            kernel.Bind<IUnitOfWork>().To<UnitOfWork>();
            // Binding trick to avoid having to bind all usages of IGenericRepository. This binds them all!
            kernel.Bind(typeof(IGenericRepository<>)).To(typeof(GenericRepository<>));

            // Identity
            kernel.Bind(typeof(IUserStore<>)).To(typeof(UserStore<>)).InRequestScope().WithConstructorArgument("context", kernel.Get<ApplicationContext>());
            kernel.Bind<IAuthenticationManager>().ToMethod(c => HttpContext.Current.GetOwinContext().Authentication).InRequestScope();
            kernel.Bind<ApplicationUserManager>().ToMethod(c => HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>());

            // AutoMapper
            kernel.Bind<IEmailService>().To<EmailService>();
            kernel.Bind<IImageValidator>().To<ImageValidator>();
            kernel.Bind<IImageRotate>().To<ImageRotate>();
            kernel.Bind<IImageCropper>().To<ImageCropper>();
            kernel.Bind<IImageResize>().To<ImageResize>();
            kernel.Bind<IFacebookProfileIdExtract>().To<FacebookProfileIdExtract>();
            kernel.Bind<IFacebookAuthHelper>().To<FacebookAuthHelper>();
            kernel.Bind<IContestHelper>().To<ContestHelper>();
            kernel.Bind<ISettingHelper>().To<SettingHelper>();
            kernel.Bind<ICommentHelper>().To<CommentHelper>();
            kernel.Bind<IVoteHelper>().To<VoteHelper>();
            kernel.Bind<IPosterGenerator>().To<PosterGenerator>();
            kernel.Bind<IPdfThumbGenerator>().To<PdfThumbGenerator>();
            kernel.Bind<IRealtorService>().To<ZipCodeRealtorService>();
            kernel.Bind<IMapper>().ToMethod(context => Mapper.Instance);
            kernel.Bind<IFileLocator>().To<FileLocator>()
                .WithConstructorArgument("uploadDir", c => HttpContext.Current.Server.MapPath(WebConfigurationManager.AppSettings["UploadDir"])); // must be done this way as HttpContext doesn't exist yet
            kernel.Bind<INomineeFiles>().To<ForeningFiles>()
                .WhenInjectedInto<NominateForeningController>();
            kernel.Bind<INomineeFiles>().To<ForeningFiles>()
                .WhenInjectedInto<AdminNomineeController>();
            kernel.Bind<INomineeFiles>().To<ForeningFiles>()
                .WhenInjectedInto<AdminContestController>();
            kernel.Bind<AdminNomineeController>().ToSelf()
                .WithConstructorArgument("baseUrl", baseUrl);

            // Nominate

            kernel.Bind<NominateForeningController>().ToSelf()
                .WithConstructorArgument("defaultPicture", defaultPictureForening)
                .WithConstructorArgument("baseUrl", baseUrl)
                .WithConstructorArgument("maxUploadSize", maxUploadSize)
                .WithConstructorArgument("useRealtorMailService", useRealtorMailService);

            // Nominee

            kernel.Bind<NomineeForeningController>().ToSelf()
                .WithConstructorArgument("defaultPicture", defaultPictureForening)
                .WithConstructorArgument("baseUrl", baseUrl)
                .WithConstructorArgument("bannerUrl", foreningFrontpageBannerUrl)
                .WithConstructorArgument("twitterShareMessage", twitterForeningShareMessage);

            // About

            kernel.Bind<AboutController>().ToSelf()
                .WithConstructorArgument("ildsjaelBannerUrl", ildsjaelAboutBannerUrl)
                .WithConstructorArgument("foreningBannerUrl", foreningAboutBannerUrl);

            // Admin

            kernel.Bind<AdminSettingsController>().ToSelf()
                .WithConstructorArgument("ildsjaelFrontpageBannerPath", ildsjaelFrontpageBannerPath)
                .WithConstructorArgument("ildsjaelAboutBannerPath", ildsjaelAboutBannerPath)
                .WithConstructorArgument("foreningFrontpageBannerPath", c => HttpContext.Current.Server.MapPath(WebConfigurationManager.AppSettings["ForeningFrontpageBannerPath"]))
                .WithConstructorArgument("foreningAboutBannerPath", c => HttpContext.Current.Server.MapPath(WebConfigurationManager.AppSettings["ForeningAboutBannerPath"]));

            kernel.Bind<IHtmlSanitizer>().To<HtmlSanitizer>().InRequestScope();
        }
    }
}
