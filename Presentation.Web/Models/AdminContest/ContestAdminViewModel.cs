﻿using Core.DomainModel;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Presentation.Web.Models.NomineeAdmin
{
    public class ContestAdminViewModel
    {
        public int Id { get; set; }

        [DisplayName("Navn")]
        public string Name { get; set; }

        [DisplayName("Starter")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime Starts { get; set; }

        [DisplayName("Slutter")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime Ends { get; set; }

        [DisplayName("Aktiveret")]
        public bool IsActive { get; set; }

        [DisplayName("Type")]
        public ContestType Type { get; set; }
    }
}