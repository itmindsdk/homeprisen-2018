﻿using System;
using System.ComponentModel;

namespace Presentation.Web.Models.NomineeAdmin
{
    public class CommentAdminViewModel
    {
        public int Id { get; set; }

        [DisplayName("Sendt")]
        public DateTime Submitted { get; set; }

        [DisplayName("Facebook ID")]
        public string FacebookUserId { get; set; }

        [DisplayName("Nomineret ID")]
        public Guid NomineeId { get; set; }

        [DisplayName("Navn")]
        public string Name { get; set; }

        [DisplayName("By")]
        public string City { get; set; }

        [DisplayName("Kommentar")]
        public string Text { get; set; }
    }
}