﻿using System;
using Presentation.Web.Models.Shared;

namespace Presentation.Web.Models.Nominee
{
    // TODO: Comment
    public class DetailsViewModelBase
    {
        public bool IsActive { get; set; }
        public string BannerPhotoUrl { get; set; }
        public Guid Guid { get; set; }
        public string Name { get; set; }
        public string City { get; set; }
        public string Description { get; set; }
        public int VoteCount { get; set; }
        public int Rank { get; set; }
        public int TotalNominees { get; set; }
        public string AbsoluteUrl { get; set; }
        public string RelativeUrl { get; set; }
        public string TwitterShareMessage { get; set; }

        public bool AllowVoting { get; set; }

        public string PictureUrl { get; set; }
        public string PictureAbsoluteUrl { get; set; }
        public PictureViewModel PosterSolid { get; set; }
        public PictureViewModel PosterFbPic { get; set; }
        public PictureViewModel FlyerSolid { get; set; }
        public PictureViewModel FlyerFbPic { get; set; }
        public PictureViewModel FbCover { get; set; }
        public PictureViewModel FbCoverSolid { get; set; }

        public DetailsViewModelBase()
        {
            PosterSolid = new PictureViewModel();
            PosterFbPic = new PictureViewModel();
            FlyerSolid = new PictureViewModel();
            FlyerFbPic = new PictureViewModel();
            FbCover = new PictureViewModel();
            FbCoverSolid = new PictureViewModel();
            IsActive = false;
        }
    }
}
