﻿
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Presentation.Web.Models.Nominee
{
    public class IndexViewModel
    {
        public bool IsActive { get; set; }
        public string PhotoUrl { get; set; }
        public string WinnerName { get; set; }
        public string WinnerCity { get; set; }

        [DataType(DataType.MultilineText)]
        public string FrontpageText { get; set; }

        [DataType(DataType.MultilineText)]
        public string FrontpageTitle { get; set; }

        public IndexViewModel()
        {
            IsActive = false;
        }
    }
}