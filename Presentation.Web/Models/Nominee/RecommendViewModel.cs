﻿using System;

namespace Presentation.Web.Models.Nominee
{
    // TODO: Comment
    public class RecommendViewModel
    {
        public Guid Guid { get; set; }
        public string SenderName { get; set; }
        public string SenderEmail { get; set; }
        public string RecipientName { get; set; }
        public string RecipientEmail { get; set; }
        public string Message { get; set; }
    }
}