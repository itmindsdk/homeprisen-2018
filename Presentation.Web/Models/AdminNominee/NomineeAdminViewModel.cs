﻿using Core.DomainModel;
using System;
using System.ComponentModel;

namespace Presentation.Web.Models.NomineeAdmin
{
    public class NomineeAdminViewModel
    {
        public virtual Guid Guid { get; set; }
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual string Email { get; set; }
        public virtual string Phone { get; set; }
        public virtual string Address { get; set; }
        public virtual string Zipcode { get; set; }
        public virtual string City { get; set; }
        public virtual string Description { get; set; }
        public virtual bool IsAccepted { get; set; }
        public virtual int VoteCount { get; set; }
        public virtual int Rank { get; set; }
        public virtual string NominatorName { get; set; }
        public virtual string NominatorEmail { get; set; }
        public string NominatorPhone { get; set; }
        public virtual DateTime Nominated { get; set; }
        public virtual DateTime Accepted { get; set; }
        public virtual Contest Contest { get; set; }
        public virtual int ContestId { get; set; }

        [DisplayName("Link til at acceptere/rette")]
        public string AcceptDirectLink { get; set; }
        public string Category { get; set; }
        public string Municipality { get; set; }
    }
}
