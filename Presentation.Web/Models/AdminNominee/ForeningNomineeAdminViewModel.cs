﻿using Core.DomainModel;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Presentation.Web.Models.NomineeAdmin
{
    public class ForeningNomineeAdminViewModel : NomineeAdminViewModel
    {
        public Guid Guid { get; set; }
        public int Id { get; set; }

        [DisplayName("Navn")]
        public string Name { get; set; }

        [DisplayName("Navn på formand / ansvarlig")]
        public string ResponsibleName { get; set; }

        [DisplayName("Formandens e-mail")]
        public string Email { get; set; }

        [DisplayName("Formandens telefonnummer")]
        public string Phone { get; set; }

        [DisplayName("Formandens adresse")]
        public string Address { get; set; }

        [DisplayName("Postnummer")]
        public string Zipcode { get; set; }

        [DisplayName("By")]
        public string City { get; set; }

        [DisplayName("Begrundelse")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [DisplayName("Accepteret")]
        public bool IsAccepted { get; set; }

        [DisplayName("Stemmer")]
        public int VoteCount { get; set; }

        [DisplayName("Placering")]
        public int Rank { get; set; }

        [DisplayName("Nominator navn")]
        public string NominatorName { get; set; }

        [DisplayName("Nominator e-mail")]
        public string NominatorEmail { get; set; }

        [DisplayName("Nominator telefon")]
        public string NominatorPhone { get; set; }

        [DisplayName("Nomineret tid")]
        public DateTime Nominated { get; set; }

        [DisplayName("Accepteret tid")]
        public DateTime Accepted { get; set; }

        // The contest to be nominated in
        [DisplayName("Konkurrence")]
        public virtual Contest Contest { get; set; }
        public int ContestId { get; set; }

        [DisplayName("Forening/formål")]
        public ForeningType ForeningType { get; set; }

        [DisplayName("Antal medlemmer")]
        public int MemberCount { get; set; }

        public string Category { get; set; }
        public string Municipality { get; set; }
    }
}