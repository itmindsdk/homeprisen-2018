﻿using Core.DomainModel;
using System;
using System.ComponentModel;

namespace Presentation.Web.Models.NomineeAdmin
{
    public class NomineeVotesViewModel
    {
        public DateTime Created { get; set; }
        public string FacebookUserId { get; set; }
    }
}
