﻿using Core.DomainModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;

namespace Presentation.Web.Models.SettingsAdmin
{
    public class SettingsAdminViewModel
    {
        [DisplayName("Forening forside tekst")]
        [AllowHtml]
        [DataType(DataType.MultilineText)]
        public string FrontpageTextForening { get; set; }

        [DisplayName("Ildsjæl forside tekst")]
        [AllowHtml]
        [DataType(DataType.MultilineText)]
        public string FrontpageTextIldsjael { get; set; }

        [DisplayName("Forening forside overskrift")]
        [AllowHtml]
        [DataType(DataType.MultilineText)]
        public string FrontpageTitleForening { get; set; }

        [DisplayName("Ildsjæl forside overskrift")]
        [AllowHtml]
        [DataType(DataType.MultilineText)]
        public string FrontpageTitleIldsjael { get; set; }

        [DisplayName("Om Os Forening tekst")]
        [AllowHtml]
        [DataType(DataType.MultilineText)]
        public string ForeningAboutText { get; set; }

        [DisplayName("Om Os Ildsjæl tekst")]
        [AllowHtml]
        [DataType(DataType.MultilineText)]
        public string IldsjaelAboutText { get; set; }

        [DisplayName("Startside konkurrence")]
        public ContestType DefaultContestType { get; set; }

        [DisplayName("Upload nyt Ildsjæl Forside billede")]
        public virtual HttpPostedFileBase IldsjaelFrontpagePicture { get; set; }

        [DisplayName("Upload nyt Ildsjæl Om konkurrencen billede")]
        public virtual HttpPostedFileBase IldsjaelAboutPicture { get; set; }

        [DisplayName("Upload nyt Forening Forside billede")]
        public virtual HttpPostedFileBase ForeningFrontpagePicture { get; set; }

        [DisplayName("Upload nyt Forening Om konkurrencen billede")]
        public virtual HttpPostedFileBase ForeningAboutPicture { get; set; }

        [DisplayName("Skjul \"Årets Forening/Andre Lokale Formål\" på Ildsjæl forsiden?")]
        public bool ForeningHidden { get; set; }

        [DisplayName("Skjul \"Årets Ildsjæl\" på Forening/Formål forsiden?")]
        public bool IldsjaelHidden { get; set; }

        public string Message { get; set; }
    }
}
