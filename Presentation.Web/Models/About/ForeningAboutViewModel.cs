﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Presentation.Web.Models.About
{
    public class ForeningAboutViewModel : AboutViewModel
    {
        public string PictureUrl { get; set; }
    }
}