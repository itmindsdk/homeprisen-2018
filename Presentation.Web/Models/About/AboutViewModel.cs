﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Presentation.Web.Models.About
{
    public class AboutViewModel
    {
        public string AboutText { get; set; }
        public string photoUrl { get; set; }
    }
}