﻿namespace Presentation.Web.Models.Shared
{
    public class PictureViewModel
    {
        public string ThumbUrl { get; set; }
        public string LinkUrl { get; set; }
    }
}