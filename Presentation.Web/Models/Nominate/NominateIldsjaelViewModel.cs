﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;
using Presentation.Web.Validators;

namespace Presentation.Web.Models.Nominate
{
    // TODO: Comment
    public class NominateIldsjaelViewModel : NominateViewModel
    {
        [Required(ErrorMessage = "Indtast venligst den nomineredes navn")]
        [StringLength(40, ErrorMessage = "Navn må max. indeholde 40 karakterer")]
        [DisplayName("Navn")]
        public override string Name { get; set; }

        [Required(ErrorMessage = "Indtast venligst din kandidats mailadresse")]
        [EmailAddress(ErrorMessage = "Indtast venligst en gyldig e-mail adresse")]
        [StringLength(100, ErrorMessage = "E-mail må max. indeholde 100 karakterer")]
        [Remote("CheckEmailAvailability", "Contest", AdditionalFields = "ContestType", HttpMethod = "GET", ErrorMessage = "Personen med denne e-mail adresse er allerede nomineret")]
        [DisplayName("E-mail")]
        public override string Email { get; set; }

        [DisplayName("Titel / Rolle")]
        [StringLength(30, ErrorMessage = "Titel må max. indeholde 30 karakterer")]
        [MaxWords(2, "Den nomineredes titel eller rolle må højest være på to ord adskilt af mellemrum")]
        public string Title { get; set; }

        [Required(ErrorMessage = "Indtast venligst navnet på den ny, som den nominerede er fra")]
        [StringLength(100, ErrorMessage = "By må max. indeholde 100 karakterer")]
        [DisplayName("By")]
        public override string City { get; set; }

        [Required(ErrorMessage = "Indtast venligst en begrundelse for at nominere denne person")]
        [DisplayName("Hvorfor skal han/hun være Årets Ildsjæl?")]
        public override string Description { get; set; }

        [Required(ErrorMessage = "Indtast venligst dit navn")]
        [StringLength(40, ErrorMessage = "Navn må max. indeholde 40 karakterer")]
        [DisplayName("Dit navn")]
        public override string NominatorName { get; set; }

        [Required(ErrorMessage = "Indtast mailadresse")]
        [StringLength(100, ErrorMessage = "E-mail må max. indeholde 100 karakterer")]
        [EmailAddress(ErrorMessage = "Indtast venligst en gyldig e-mail adresse")]
        [DisplayName("Din e-mail")]
        public override string NominatorEmail { get; set; }

        [Required(ErrorMessage = "Indtast venligst dit telefonnummer")]
        [MaxLength(12,ErrorMessage = "Telefon må max. indeholde 12 karakterer"), ]
        [MinLength(8, ErrorMessage = "Telefon skal minimum indeholde 8 karakterer")]
        [DisplayName("Dit telefonnummer")]
        [Range(0, double.MaxValue, ErrorMessage = "Telefonnummer må kun indeholde tal")]
        public override string NominatorPhone { get; set; }

        [EnforceTrue(ErrorMessage = "Accepter venligst betingelserne for at fortsætte")]
        public override bool TosChkBox { get; set; }

        [DisplayName("Eller indsæt Facebook-link")]
        public override string FacebookPictureUrl { get; set; }

        [DisplayName("Upload billede")]
        public override HttpPostedFileBase Picture { get; set; }
    }
}
