﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Core.DomainModel;
using Presentation.Web.Validators;

namespace Presentation.Web.Models.Nominate
{
    // TODO: Comment
    public class NominateForeningViewModel : NominateViewModel
    {
        [Required(ErrorMessage = "Vælg venligst foreningens type")]
        [DisplayName("Forening/formål")]
        public ForeningType ForeningType { get; set; }

        [Required(ErrorMessage = "Indtast venligst foreningens navn")]
        [StringLength(40, ErrorMessage = "Navn må max. indeholde 40 karakterer")]
        [DisplayName("Projektets navn:")]
        public override string Name { get; set; }

        [Required(ErrorMessage = "Indtast venligst den projektansvarliges navn")]
        [StringLength(40, ErrorMessage = "Navn må max. indeholde 40 karakterer")]
        [DisplayName("Navn på projektansvarlig:")]
        public string ResponsibleName { get; set; }

        [Required(ErrorMessage = "Indtast ansvarligs email adresse")]
        [EmailAddress(ErrorMessage = "Indtast venligst en gyldig e-mail adresse")]
        [StringLength(100, ErrorMessage = "Email må max. indeholde 100 karakterer")]
        [Remote("CheckEmailAvailability", "Contest", AdditionalFields = "ContestType", HttpMethod = "GET", ErrorMessage = "Personen med denne e-mail adresse er allerede nomineret")]
        [DisplayName("Email:")]
        public override string Email { get; set; }

        [Required(ErrorMessage = "Indtast venligst post nr på den by hvor projektet er fra")]
        [MaxLength(4, ErrorMessage = "Post nr må max. indeholde 4 karakterer"),]
        [MinLength(4, ErrorMessage = "Post nr skal minimum indeholde 4 karakterer")]
        [Range(0, double.MaxValue, ErrorMessage = "Post nr må kun indeholde tal")]
        [DisplayName("Postnummer:")]
        public override string Zipcode { get; set; }

        [Required(ErrorMessage = "Indtast venligst navnet på den by hvor foreningen/formålet er fra")]
        [StringLength(100, ErrorMessage = "By må max. indeholde 100 karakterer")]
        [DisplayName("By:")]
        public override string City { get; set; }

        [Required(ErrorMessage = "Indtast venligst en begrundelse for at nominere dette projekt")]
        [DisplayName("Begrundelse for at vinde (maks. 1000 anslag)*:")]
        public override string Description { get; set; }

        [Required(ErrorMessage = "Indtast venligst dit navn")]
        [StringLength(40, ErrorMessage = "Navn må max. indeholde 40 karakterer")]
        [DisplayName("Dit navn:")]
        public override string NominatorName { get; set; }

        [Required(ErrorMessage = "Indtast venligst din email adresse")]
        [StringLength(100, ErrorMessage = "Email må max. indeholde 100 karakterer")]
        [EmailAddress(ErrorMessage = "Indtast venligst en gyldig e-mail adresse")]
        [DisplayName("Din email:")]
        public override string NominatorEmail { get; set; }

        [Required(ErrorMessage = "Indtast venligst dit telefonnummer")]
        [MaxLength(12, ErrorMessage = "Telefon må max. indeholde 12 karakterer"),]
        [MinLength(8, ErrorMessage = "Telefon skal minimum indeholde 8 karakterer")]
        [DisplayName("Dit telefonnummer:")]
        [Range(0, double.MaxValue, ErrorMessage = "Telefonnummer må kun indeholde tal")]
        public override string NominatorPhone { get; set; }

        [EnforceTrue(ErrorMessage = "Accepter venligst betingelserne for at fortsætte")]
        public override bool TosChkBox { get; set; }
    }
}