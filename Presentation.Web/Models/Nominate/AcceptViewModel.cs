﻿using System;
using System.Web;
using Core.DomainModel;

namespace Presentation.Web.Models.Nominate
{
    // TODO: Comment
    public abstract class AcceptViewModel
    {
        public ContestType ContestType { get; set; }
        public Guid Guid { get; set; }
        public string NominatorName { get; set; }
        public bool IsAccepted { get; set; }
        public bool CanDecline { get { return !IsAccepted; } }

        public virtual string Name { get; set; }
        public virtual string Description { get; set; }
        public virtual string Email { get; set; }
        public virtual string Phone { get; set; }
        public virtual string Address { get; set; }
        public virtual string Zipcode { get; set; }
        public virtual string City { get; set; }

        public virtual bool TosChkBox { get; set; }
        public virtual string FacebookPictureUrl { get; set; }
        public virtual HttpPostedFileBase Picture { get; set; }
        public string PictureUrl { get; set; }

        public virtual string[] AvailableCategories { get; set; }
        public virtual string Category { get; set; }

        public virtual string[] AvailableMunicipalities { get; set; }
        public virtual string Municipality { get; set; }
    }
}