﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web;
using Core.DomainModel;
using Presentation.Web.Validators;

namespace Presentation.Web.Models.Nominate
{
    // TODO: Comment
    public class AcceptForeningViewModel : AcceptViewModel
    {
        [DisplayName("Type")]
        public ForeningType ForeningType { get; set; }

        [Required(ErrorMessage = "Forening")]
        [StringLength(40, ErrorMessage = "Navn må max. indeholde 40 karakterer")]
        [DisplayName("Projektets navn:")]
        public override string Name { get; set; }

        [DisplayName("Antal medlemmer")]
        public int MemberCount { get; set; }

        [DisplayName("begrundelse for hvorfor dit projekt skal vinde (maks. 1000 anslag):")]
        public override string Description { get; set; }

        [Required(ErrorMessage = "Indtast venligst din e-mail adresse")]
        [StringLength(100, ErrorMessage = "E-mail må max. indeholde 100 karakterer")]
        [EmailAddress(ErrorMessage = "Indtast venligst en gyldig e-mail adresse")]
        [DisplayName("Din email:")]
        public override string Email { get; set; }

        [Required(ErrorMessage = "Indtast venligst dit telefonnummer")]
        [MaxLength(12, ErrorMessage = "Telefon må max. indeholde 12 karakterer"),]
        [MinLength(8, ErrorMessage = "Telefon skal minimum indeholde 8 karakterer")]
        [DisplayName("Telefonnummer:")]
        [Range(0, double.MaxValue, ErrorMessage = "Telefonnummer må kun indeholde tal")]
        public override string Phone { get; set; }

        [Required(ErrorMessage = "Indtast venligst din adresse")]
        [StringLength(100, ErrorMessage = "Adresse må max. indeholde 100 karakterer")]
        [DisplayName("Adresse:")]
        public override string Address { get; set; }

        [Required(ErrorMessage = "Indtast venligst dit postnummer")]
        [MinLength(4, ErrorMessage = "Postnummer skal indeholde præcis 4 karakterer")]
        [MaxLength(4, ErrorMessage = "Postnummer skal indeholde præcis 4 karakterer")]
        [Range(0, double.MaxValue, ErrorMessage = "Postnummer må kun indeholde tal")]
        [DisplayName("Postnummer:")]
        public override string Zipcode { get; set; }

        [Required(ErrorMessage = "Indtast venligst navnet på din hjemby")]
        [StringLength(100, ErrorMessage = "By må max. indeholde 100 karakterer")]
        [DisplayName("By:")]
        public override string City { get; set; }

        [EnforceTrue(ErrorMessage = "Accepter venligst betingelserne for at fortsætte")]
        public override bool TosChkBox { get; set; }

        [DisplayName("Upload billede*:")]
        public override HttpPostedFileBase Picture { get; set; }

        public override string[] AvailableCategories { get; set; }
        [DisplayName("Vælg venligst en passende kategori:")]
        public override string Category { get; set; }

        public override string[] AvailableMunicipalities { get; set; }
        [DisplayName("Kommune:")]
        [Required(ErrorMessage = "Indtast venligst navnet på din kommune")]
        public override string Municipality { get; set; }
    }
}