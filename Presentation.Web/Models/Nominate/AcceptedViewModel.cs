﻿namespace Presentation.Web.Models.Nominate
{
    public class AcceptedViewModel
    {
        public string NomineeDetailsUrl { get; set; }
        public string NomineeName { get; set; }
    }
}