﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web;
using Presentation.Web.Validators;

namespace Presentation.Web.Models.Nominate
{
    // TODO: Comment
    public class AcceptIldsjaelViewModel : AcceptViewModel
    {
        [Required(ErrorMessage = "Indtast venligst dit navn")]
        [StringLength(40, ErrorMessage = "Navn må max. indeholde 40 karakterer")]
        [DisplayName("Dit fulde navne")]
        public override string Name { get; set; }

        [MaxWords(2, "Din titel eller rolle må højest være på to ord adskilt af mellemrum")]
        [StringLength(30, ErrorMessage = "Titel må max. indeholde 30 karakterer")]
        [DisplayName("Titel / Rolle")]
        public string Title { get; set; }

        [DisplayName("Begrundelse for hvorfor du skal vinde")]
        public override string Description { get; set; }

        [Required(ErrorMessage = "Indtast venligst din e-mail adresse")]
        [StringLength(100, ErrorMessage = "E-mail må max. indeholde 100 karakterer")]
        [EmailAddress(ErrorMessage = "Indtast venligst en gyldig e-mail adresse")]
        [DisplayName("Din e-mail")]
        public override string Email { get; set; }

        [Required(ErrorMessage = "Indtast venligst dit telefonnummer")]
        [MaxLength(12, ErrorMessage = "Telefon må max. indeholde 12 karakterer"),]
        [MinLength(8, ErrorMessage = "Telefon skal minimum indeholde 8 karakterer")]
        [Range(0, double.MaxValue, ErrorMessage = "Telefonnummer må kun indeholde tal")]
        [DisplayName("Dit telefonnummer")]
        public override string Phone { get; set; }

        [Required(ErrorMessage = "Indtast venligst din adresse")]
        [StringLength(100, ErrorMessage = "Adresse må max. indeholde 100 karakterer")]
        [DisplayName("Adresse")]
        public override string Address { get; set; }

        [Required(ErrorMessage = "Indtast venligst dit postnummer")]
        [MinLength(4, ErrorMessage = "Postnummer skal indeholde præcis 4 karakterer")]
        [MaxLength(4, ErrorMessage = "Postnummer skal indeholde præcis 4 karakterer")]
        [DisplayName("Postnummer")]
        [Range(0, double.MaxValue, ErrorMessage = "Postnummer må kun indeholde tal")]
        public override string Zipcode { get; set; }

        [Required(ErrorMessage = "Indtast venligst navnet på din hjemby")]
        [StringLength(100, ErrorMessage = "By må max. indeholde 100 karakterer")]
        [DisplayName("By")]
        public override string City { get; set; }

        [EnforceTrue(ErrorMessage = "Accepter venligst betingelserne for at fortsætte")]
        public override bool TosChkBox { get; set; }

        [DisplayName("Eller indsæt Facebook-link")]
        public override string FacebookPictureUrl { get; set; }

        [DisplayName("Upload billede")]
        public override HttpPostedFileBase Picture { get; set; }
    }
}
