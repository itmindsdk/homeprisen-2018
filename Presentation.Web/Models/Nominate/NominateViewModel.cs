﻿using System.Web;
using Core.DomainModel;

namespace Presentation.Web.Models.Nominate
{
    // TODO: Comment
    public abstract class NominateViewModel
    {
        public ContestType ContestType { get; set; }

        public virtual string Name { get; set; }
        public virtual string Email { get; set; }
        public virtual string Zipcode { get; set; }
        public virtual string City { get; set; }
        public virtual string Description { get; set; }
        public virtual string NominatorName { get; set; }
        public virtual string NominatorEmail { get; set; }
        public virtual string NominatorPhone { get; set; }

        public virtual bool TosChkBox { get; set; }
        public virtual string FacebookPictureUrl { get; set; }
        public virtual HttpPostedFileBase Picture { get; set; }
        public string PictureUrl { get; set; }
    }
}
