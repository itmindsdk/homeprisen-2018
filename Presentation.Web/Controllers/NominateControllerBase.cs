﻿using AutoMapper;
using Core.ApplicationServices;
using Core.ApplicationServices.Images;
using Core.ApplicationServices.NomineeFiles;
using Core.DomainModel;
using Core.DomainServices;
using Presentation.Web.Models.Nominate;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Mvc;
using Newtonsoft.Json.Linq;

namespace Presentation.Web.Controllers
{
    /// <summary>
    /// The whole flow of nominating and accepting a nomination is handled
    /// through this controller. Works for both Ildsjael and Forening
    /// through implementation of generics.
    /// Some logic which is individual for the different types is implemented
    /// through overriding abstract methods defined in this base class.
    /// </summary>
    /// <typeparam name="TDomainModel">Domainmodel to work with</typeparam>
    /// <typeparam name="TNominateViewModel">Viewmodel to work with when nominating</typeparam>
    /// <typeparam name="TAcceptViewModel">Viewmodel to work with when accepting a nomination</typeparam>
    public abstract class NominateControllerBase<TDomainModel, TNominateViewModel, TAcceptViewModel> : Controller
        where TDomainModel : Nominee, new()
        where TNominateViewModel : NominateViewModel, new()
        where TAcceptViewModel : AcceptViewModel
    {
        private readonly ContestType _contestType;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IGenericRepository<Nominee> _nomineeRepo;
        private readonly IGenericRepository<Picture> _pictureRepo;
        private readonly IGenericRepository<Category> _categoryRepo; 
        private readonly IContestHelper _contestHelper;
        private readonly IFileLocator _fileLocator;
        private readonly IImageValidator _imgValidator;
        private readonly IImageRotate _imgRotate;
        private readonly INomineeFiles _nomineeFiles;
        private readonly string _defaultPicture;
        private readonly int _maxUploadSize;
        private readonly IRealtorService _realtorService;
        private readonly bool _useRealtorMailService;

        protected NominateControllerBase(
            ContestType contestType,
            IMapper mapper,
            IUnitOfWork unitOfWork,
            IGenericRepository<Nominee> nomineeRepo,
            IGenericRepository<Picture> pictureRepo,
            IGenericRepository<Category> categoryRepo,
            INomineeFiles nomineeFiles,
            IContestHelper contestHelper,
            IFileLocator fileLocator,
            IImageValidator imgValidator,
            IImageRotate imgRotate,
            string defaultPicture,
            int maxUploadSize,
            IRealtorService realtorService,
            bool useRealtorMailService)
        {
            _contestType = contestType;
            _mapper = mapper;
            _unitOfWork = unitOfWork;
            _nomineeRepo = nomineeRepo;
            _pictureRepo = pictureRepo;
            _categoryRepo = categoryRepo;
            _nomineeFiles = nomineeFiles;
            _contestHelper = contestHelper;
            _fileLocator = fileLocator;
            _imgValidator = imgValidator;
            _imgRotate = imgRotate;
            _defaultPicture = defaultPicture;
            _maxUploadSize = maxUploadSize;
            _realtorService = realtorService;
            _useRealtorMailService = useRealtorMailService;
        }

        /// <summary>
        /// Send a "You have been nominated" e-mail to a nominee
        /// </summary>
        /// <param name="nominee">Nominee</param>
        protected abstract void SendNominationEmail(Nominee nominee);

        /// <summary>
        /// Send an "You have accepted your nomination" e-mail to a nominee
        /// </summary>
        /// <param name="nominee">Nominee</param>
        protected abstract void SendNomineeAcceptedEmail(Nominee nominee);

        /// <summary>
        /// Send an "Your candidate has accepted your nomination" e-mail to nominator
        /// </summary>
        /// <param name="nominee">Nominee</param>
        protected abstract void SendNominatorAcceptedEmail(Nominee nominee);

        /// <summary>
        /// Send an "A candidate has accepted a nomination" e-mail to realtor
        /// </summary>
        /// <param name="nominee">Nominee</param>
        /// <param name="realtorList">List<Realtor></Realtor></param>
        protected abstract void SendRealtorAcceptedEmail(Nominee nominee, IList<Realtor> realtorList);

        /// <summary>
        /// Send an "Thank you for nominating a candidate" e-mail to nominator
        /// </summary>
        /// <param name="nominee"></param>
        protected abstract void SendNominatorThanksEmail(Nominee nominee);

        /// <summary>
        /// Returns a fully-qualified URL to the details-page of a nominee
        /// </summary>
        /// <param name="nominee">Nominee</param>
        /// <returns>Fully-qualified URL</returns>
        protected abstract string GetDetailsUrl(Nominee nominee);

        /// <summary>
        /// Create a stream from a URL.
        /// </summary>
        /// <param name="url">Http URL to request</param>
        /// <returns>Readable stream</returns>
        protected Stream GetStream(string url)
        {
            var request = WebRequest.Create(url);
            var response = request.GetResponse();
            var stream = response.GetResponseStream();
            var mem = new MemoryStream();
            stream.CopyTo(mem);
            return mem;
        }

        /// <summary>
        /// Given a stream, populate a Picture object with Width and Height, and copy
        /// stream data to harddrive. If Picture is null, creates a new Picture object
        /// </summary>
        /// <param name="stream">Stream to copy image data from</param>
        /// <param name="picture">Picture to use. May be null</param>
        /// <returns>Picture object with Width and Height attributes populated</returns>
        protected Picture CreatePictureFromStream(Stream stream, Picture picture = null)
        {
            var size = _imgValidator.GetSize(stream);
            if (picture == null)
                picture = new Picture() { Width = size.Width, Height = size.Height };

            // Rotate image accordin to EXIF data
            stream = _imgRotate.RotateToExitOrientation(stream);

            // Save to disc
            var path = _fileLocator.GetPath(picture, FileVariant.OriginalPicture);
            _fileLocator.EnterFolder(picture);

            using (var writer = new FileStream(path, FileMode.OpenOrCreate))
            {
                stream.Position = 0;
                stream.CopyTo(writer);
            }

            return picture;
        }

        /// <summary>
        /// Show formular for nominating.
        /// </summary>
        public virtual ActionResult Nominate()
        {
            var contest = _contestHelper.GetActiveContest(_contestType);
            if (contest == null)
                return View("NoActiveContest");

            var vm = new TNominateViewModel();
            vm.PictureUrl = _defaultPicture;
            vm.ContestType = _contestType;

            return View(vm);
        }

        /// <summary>
        /// Handle formular submit for nominating.
        /// </summary>
        /// <param name="vm">POST data</param>
        /// <returns>View with validation errors, or redirect</returns>
        [ValidateAntiForgeryToken]
        public virtual ActionResult Nominate(TNominateViewModel vm)
        {
            var r = Request.Params["g-recaptcha-response"];
            // ... validate null or empty value if you want
            // then
            // make a request to recaptcha api
            using (var wc = new WebClient())
            {
                var validateString = string.Format(
                    "https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}",
                   "6LeCM1UUAAAAAFZYSh_tb-Pbc1k2NFj0te1b21Zu",    // secret recaptcha key
                   r); // recaptcha value
                       // Get result of recaptcha
                var recaptcha_result = wc.DownloadString(validateString);
                // Just check if request make by user or bot
                if (recaptcha_result.ToLower().Contains("false"))
                {
                    ModelState.AddModelError("reCaptha", "Validering fejlede");
                    return View(vm);
                }
            }
            Picture picture = null;
            Stream pictureStream = null;

            // Contest may not be active at the moment (not started yet, already ended, whatever reason)
            var contest = _contestHelper.GetActiveContest(_contestType);
            if (contest == null)
                return View("NoActiveContest");

            // Get picture stream, either from form upload or from URL
            if (vm.Picture != null && vm.Picture.ContentLength > 0)
            {
                if (vm.Picture.ContentLength > _maxUploadSize * 1024 * 1024)
                    ModelState.AddModelError("Picture", string.Format("Billedet må max.fylde {0} Mb.", _maxUploadSize));
                else
                    pictureStream = vm.Picture.InputStream;
            }
            else if (!String.IsNullOrWhiteSpace(vm.FacebookPictureUrl))
                pictureStream = GetStream(vm.FacebookPictureUrl);

            // Validate picture
            if (pictureStream != null && !_imgValidator.IsImage(pictureStream))
                ModelState.AddModelError("Picture", "Billedet kunne ikke indlæses. Prøv venligst igen.");

            // E-mails are unique
            if (_contestHelper.HasNominatedEmail(contest, vm.Email))
                ModelState.AddModelError("Email", "Denne person er allerede nomineret, og kan derfor ikke nomineres igen");

            // If form validation failed, show the form again with validation errors
            if (!ModelState.IsValid)
            {
                vm.PictureUrl = _defaultPicture;
                return View(vm);
            }

            if (pictureStream != null)
            {
                picture = CreatePictureFromStream(pictureStream);
                _pictureRepo.Insert(picture);
            }

            // Create new nominee
            var nominee = _mapper.Map<TDomainModel>(vm);
            nominee.Contest = contest;
            nominee.Picture = picture;
            nominee.IsAccepted = false;
            nominee.Nominated = DateTime.Now;

            _nomineeRepo.Insert(nominee);
            _unitOfWork.Save();

            // Send email to nominee
            SendNominationEmail(nominee);

            // Send email to nominator
            SendNominatorThanksEmail(nominee);

            return RedirectToAction("Thanks", new { nomineeName = nominee.Name });
        }

        /// <summary>
        /// Show a confirmation that a user has been nominated.
        /// This action is shown when Nominate() has been completed.
        /// </summary>
        /// <param name="nomineeName">Name of the nominee</param>
        /// <returns>View</returns>
        public virtual ActionResult Thanks(string nomineeName)
        {
            return View(new ThanksViewModel() { NomineeName = nomineeName });
        }

        /// <summary>
        /// Show a formular for accepting a nomination. Also allows the nominee to edit details.
        /// Shows a Decline button if the nominee has not already been accepted.
        /// </summary>
        /// <param name="guid">Guid of nominee</param>
        /// <returns>View</returns>
        public virtual ActionResult Accept(Guid guid)
        {
            var nominee = _nomineeRepo.AsQueryable()
                .OfType<TDomainModel>()
                .FirstOrDefault(n => n.Guid == guid);
            if (nominee == null)
                return new HttpNotFoundResult();

            var vm = _mapper.Map<TAcceptViewModel>(nominee);
            vm.ContestType = _contestType;

            var categories = _categoryRepo.Get();
            vm.AvailableCategories = categories.Select(cat => cat.Name).ToArray();

            // Show uploaded picture, or default picture
            if (nominee.Picture != null)
                vm.PictureUrl = _fileLocator.GetUrl(nominee, FileVariant.OriginalPicture);
            else
                vm.PictureUrl = _defaultPicture;

            // populate municipality
            var municipalities = GetMunicipalitiesFromZipCode(vm.Zipcode);
            municipalities = municipalities != null ? municipalities : new List<string>(); 
            vm.Municipality = municipalities.Count > 0 ? municipalities.First() : "";
            vm.AvailableMunicipalities = municipalities.Count > 1 ? municipalities.ToArray() : new string[] {};

            return View(vm);
        }

        /// <summary>
        /// Handle formular submit for accepting.
        /// </summary>
        /// <param name="vm">POST data</param>
        /// <returns>View</returns>
        [ValidateAntiForgeryToken]
        public virtual ActionResult Accept(TAcceptViewModel vm)
        {
            // Contest may not be active at the moment (not started yet, already ended, whatever reason)
            var contest = _contestHelper.GetActiveContest(_contestType);
            if (contest == null)
                return View("NoActiveContest");

            // Get nominee
            var nominee = _nomineeRepo.AsQueryable()
                .OfType<TDomainModel>()
                .FirstOrDefault(n => n.Guid == vm.Guid);
            if (nominee == null)
                return new HttpNotFoundResult();

            vm.IsAccepted = nominee.IsAccepted;
            bool alreadyAccepted = nominee.IsAccepted;
            Stream pictureStream = null;

            // Get picture stream, either from form upload or from URL
            if (vm.Picture != null && vm.Picture.ContentLength > 0)
            {
                if (vm.Picture.ContentLength > _maxUploadSize * 1024 * 1024)
                    ModelState.AddModelError("Picture", string.Format("Billedet må max.fylde {0} Mb.", _maxUploadSize));
                else
                    pictureStream = vm.Picture.InputStream;
            }
            else if (!String.IsNullOrWhiteSpace(vm.FacebookPictureUrl))
                pictureStream = GetStream(vm.FacebookPictureUrl);

            // Validate picture
            if (pictureStream != null && !_imgValidator.IsImage(pictureStream))
                ModelState.AddModelError("Picture", "Billedet kunne ikke indlæses. Prøv venligst igen.");
            else if (pictureStream == null && nominee.Picture == null)
                ModelState.AddModelError("Picture", "Du skal uploade et billede for at acceptere din nominering.");

            // Check if e-mail is used by another nominated
            if (_contestHelper.HasNominatedEmail(contest, vm.Email, nominee))
                ModelState.AddModelError("Email", "Denne e-mail er allerede optaget");

            // If form validation failed, show the form again with validation errors
            if (!ModelState.IsValid)
                return View(vm);

            // Save picture to disc
            if (pictureStream != null)
            {
                var newPicture = (nominee.Picture == null);
                nominee.Picture = CreatePictureFromStream(pictureStream, nominee.Picture);
                if (newPicture)
                    _pictureRepo.Insert(nominee.Picture);
                else
                    _pictureRepo.Update(nominee.Picture);
            }

            // Update nominee
            _mapper.Map<TAcceptViewModel, TDomainModel>(vm, nominee);
            nominee.Accepted = (alreadyAccepted) ? nominee.Accepted : DateTime.Now;
            nominee.IsAccepted = true;

            // Commit to DB
            _nomineeRepo.Update(nominee);
            _unitOfWork.Save();

            // Maintain ranks of nominees in this contest
            _contestHelper.UpdateRanks(contest);

            // Re-generate files to be sure they are syncronized with
            // picture, name and other details has been changed
            _nomineeFiles.GenerateFiles(nominee);

            if (!alreadyAccepted)
            {
                // Send mail to nominated.
                // This email contains a confirmation of hes accept and a link to the
                // same accept page, as this also allows him to change hes details.
                SendNomineeAcceptedEmail(nominee);

                // Send mail to nominator.
                // This contains a link to the URL at which he can vote for the nominated.
                SendNominatorAcceptedEmail(nominee);

                // Send mail to realtor.
                // This contains a link to the URL at which he can see the nominated.
                if (_useRealtorMailService)
                {
                    var realtorList = _realtorService.GetRealtorsAsync(nominee).Result;
                    SendRealtorAcceptedEmail(nominee, realtorList);
                }
            }
            var acceptedVm = new AcceptedViewModel()
            {
                NomineeDetailsUrl = GetDetailsUrl(nominee),
                NomineeName = nominee.Name
            };
            return View("Accepted", acceptedVm);
        }

        /// <summary>
        /// Decline a nomination. Can only be done if the nominee has not
        /// already accepted the nomination.
        /// </summary>
        /// <returns>View</returns>
        public virtual ActionResult Decline(Guid guid)
        {
            var nominee = _nomineeRepo.AsQueryable()
                .OfType<TDomainModel>()
                .FirstOrDefault(n => n.Guid == guid && n.IsAccepted == false);
            if (nominee == null)
                return new HttpNotFoundResult();

            _nomineeFiles.RemoveFiles(nominee);
            _nomineeRepo.DeleteByKey(guid);
            _unitOfWork.Save();

            return View();
        }

        private List<String> GetMunicipalitiesFromZipCode(string ZipCode)
        {
            using (var client = new HttpClient())
            {
                List<string> municipalities = new List<string>();
                try
                {
                    var url = $"https://dawa.aws.dk/postnumre?nr={ZipCode}";
                    var response = client.GetAsync(url).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var json = response.Content.ReadAsStringAsync().Result;
                        var arr = JArray.Parse(json);
                        var muns = arr[0]["kommuner"];
                        foreach (var municipality in muns)
                        {
                            municipalities.Add(municipality["navn"].ToString());
                        }
                        return municipalities;
                    }
                    return null;
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }
    }
}
