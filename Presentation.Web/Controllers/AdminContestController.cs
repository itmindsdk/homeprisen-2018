﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using PagedList;
using Core.DomainModel;
using Core.DomainServices;
using AutoMapper;
using Presentation.Web.Models.NomineeAdmin;
using Core.ApplicationServices;
using Core.ApplicationServices.NomineeFiles;

namespace Presentation.Web.Controllers
{
    [Authorize]
    public class AdminContestController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IGenericRepository<Nominee> _nomineeRepo;
        private readonly IGenericRepository<Contest> _contestRepo;
        private readonly IGenericRepository<Vote> _voteRepo;
        private readonly INomineeFiles _nomineeFiles;
        private readonly IContestHelper _contestHelper;

        public AdminContestController(
            IMapper mapper,
            IUnitOfWork unitOfWork,
            IGenericRepository<Nominee> nomineeRepo,
            IGenericRepository<Contest> contestRepo,
            IGenericRepository<Vote> voteRepo,
            INomineeFiles nomineeFiles,
            IContestHelper contestHelper
            )
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
            _nomineeRepo = nomineeRepo;
            _contestRepo = contestRepo;
            _voteRepo = voteRepo;
            _nomineeFiles = nomineeFiles;
            _contestHelper = contestHelper;
        }

        [Route("~/admin/konkurrencer")]
        public ActionResult Index()
        {
            var contests = _contestRepo.AsQueryable().OrderByDescending(x => x.Id);
            var mapped = _mapper.Map<IEnumerable<ContestAdminViewModel>>(contests);
            return View(mapped);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ContestAdminViewModel vm)
        {
            var contest = _mapper.Map<Contest>(vm);

            if (_contestHelper.HasContestInPeriod(contest.Type, contest.Starts, contest.Ends))
                ModelState.AddModelError("Starts", "Der er allerede en konkurrence af denne type i perioden");

            if (ModelState.IsValid)
            {
                _contestRepo.Insert(contest);
                _unitOfWork.Save();
                return RedirectToAction("Index");
            }

            return View(vm);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var contest = _contestRepo.AsQueryable().FirstOrDefault(x => x.Id == id);
            var nomineeVm = _mapper.Map<ContestAdminViewModel>(contest);

            if (contest == null)
                return HttpNotFound();

            return View(nomineeVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ContestAdminViewModel vm)
        {
            var contest = _mapper.Map<Contest>(vm);

            if (_contestHelper.HasContestInPeriod(contest.Type, contest.Starts, contest.Ends, contest))
                ModelState.AddModelError("Starts", "Der er allerede en konkurrence af denne type i perioden");

            if (ModelState.IsValid)
            {
                _contestRepo.Update(contest);
                _unitOfWork.Save();
                return RedirectToAction("Index");
            }

            return View(vm);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var contest = _contestRepo.AsQueryable().FirstOrDefault(x => x.Id == id);
            if (contest == null)
                return HttpNotFound();

            var vm = _mapper.Map<ContestAdminViewModel>(contest);
            return View(vm);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var contest = _contestRepo.AsQueryable().FirstOrDefault(x => x.Id == id);
            var nominees = _nomineeRepo.AsQueryable().Where(x => x.ContestId == id).ToList();

            foreach(var nominee in nominees)
                _nomineeFiles.RemoveFiles(nominee);

            _contestRepo.DeleteByKey(id);
            _unitOfWork.Save();

            return RedirectToAction("Index");
        }

        public ActionResult ShowVotes(int id)
        {
            var votes = _voteRepo.AsQueryable().Where(x => x.Nominee.ContestId == id).OrderBy(x => x.Created);
            var mapped = Mapper.Map<IEnumerable<ContestVotesViewModel>>(votes);

            return View(mapped);
        }
    }
}
