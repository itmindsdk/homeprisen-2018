﻿using System.Web.Mvc;
using Facebook;
using System.Net;
using System.Text.RegularExpressions;
using Core.ApplicationServices.Facebook;

namespace Presentation.Web.Controllers
{
    public class FacebookController : Controller
    {
        private readonly IFacebookAuthHelper _fbAuth;
        private readonly IFacebookProfileIdExtract _idExtract;

        public FacebookController(IFacebookAuthHelper fbAuth, IFacebookProfileIdExtract idExtract)
        {
            _fbAuth = fbAuth;
            _idExtract = idExtract;
        }

        [HttpPost]
        public ActionResult Auth(string accessToken)
        {
            if (string.IsNullOrWhiteSpace(accessToken))
                return Json(new { status = "error" });

            // TODO: What if access token is invalid?
            var client = new FacebookClient(accessToken);

            // TODO: What if API is down or there is no internet connection?
            // TODO: What exceptions are thrown by this method?
            dynamic user = client.Get("me");

            _fbAuth.Register(accessToken, (string)user.name, (string)user.id);

            return Json(new { status = "ok" });
        }

        /// <summary>
        /// Tries to retrieve a Facebook user id from a direct link to a profile.
        /// </summary>
        /// <param name="facebookProfileUrl">Direct link to Facebook profile</param>
        /// <returns>Json encoded object</returns>
        [HttpGet]
        public ActionResult GetFacebookProfilePictureUrl(string facebookProfileUrl)
        {
            // TODO: Refactor

            if (facebookProfileUrl == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var profileId = _idExtract.ExtractFromUrl(facebookProfileUrl);

            if (profileId == null)
            {
                var client = new WebClient();
                client.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");

                // m.facebook.com does not contain profile ID, so remove the m.
                var regex = new Regex("m.facebook");
                facebookProfileUrl = regex.Replace(facebookProfileUrl, "facebook");

                try
                {
                    var source = client.DownloadString(facebookProfileUrl);
                    profileId = _idExtract.ExtractFromSource(source);
                }
                catch (WebException)
                { }
            }

            if (profileId != null)
            {
                var pictureUrl = _idExtract.CreateProfilePictureUrl(profileId);
                return Json(new { PictureUrl = pictureUrl }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { PictureUrl = "" }, JsonRequestBehavior.AllowGet);
        }
    }
}
