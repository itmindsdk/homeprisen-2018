﻿using System.Web.Mvc;
using Core.DomainModel;
using Core.ApplicationServices;

namespace Presentation.Web.Controllers
{
    /// <summary>
    /// Redirects client to the proper frontpage.
    /// Frontpage is chosen by an administrator.
    /// </summary>
    public class HomeController : Controller
    {
        private ISettingHelper _settingsHelper { get; set; }

        public HomeController(ISettingHelper settingsHelper)
        {
            _settingsHelper = settingsHelper;
        }

        [HttpGet]
        public ActionResult Index()
        {
            var type = _settingsHelper.GetDefaultContestType();

            return RedirectToAction("Index", "NomineeForening");
        }
    }
}
