﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using Core.ApplicationServices;
using Core.ApplicationServices.Facebook;
using Core.DomainModel;
using Core.DomainServices;
using Presentation.Web.Models.Nominee;

namespace Presentation.Web.Controllers
{
    /// <summary>
    /// Listing a nominees and displaying of details is handled through this base class.
    /// Works for both Ildsjael and Forening through implementation of generics.
    /// Some logic which is individual for the different types is implemented
    /// through overriding abstract methods defined in this base class.
    /// </summary>
    /// <typeparam name="TDataModel">Domainmodel to work with</typeparam>
    /// <typeparam name="TDetailsViewModel">Viewmodel to work with when listing nominees and displaying details</typeparam>
    /// <typeparam name="TIndexViewModel">Viewmodel to work with when compiling index page</typeparam>
    public abstract class NomineeControllerBase<TDataModel, TDetailsViewModel, TIndexViewModel> : Controller
        where TDataModel : Nominee
        where TDetailsViewModel : DetailsViewModelBase
        where TIndexViewModel : IndexViewModel, new()
    {
        private readonly ContestType _contestType;
        private readonly IMapper _mapper;
        private readonly IGenericRepository<Nominee> _nomineeRepo;
        private readonly IContestHelper _contestHelper;
        private readonly IVoteHelper _voteHelper;
        private readonly IFacebookAuthHelper _fbAuth;
        private readonly IFileLocator _fileLocator;
        private readonly ISettingHelper _settingsHelper;
        private readonly string _defaultPicture;
        private readonly Uri _baseUrl;
        private readonly string _bannerUrl;
        private readonly string _twitterShareMessage;

        protected NomineeControllerBase(
            ContestType contestType,
            IMapper mapper,
            IGenericRepository<Nominee> nomineeRepo,
            IContestHelper contestHelper,
            IVoteHelper voteHelper,
            IFacebookAuthHelper fbAuth,
            IFileLocator fileLocator,
            ISettingHelper settingsHelper,
            string defaultPicture,
            Uri baseUrl,
            string bannerUrl,
            string twitterShareMessage)
        {
            _contestType = contestType;
            _mapper = mapper;
            _nomineeRepo = nomineeRepo;
            _contestHelper = contestHelper;
            _voteHelper = voteHelper;
            _fbAuth = fbAuth;
            _fileLocator = fileLocator;
            _settingsHelper = settingsHelper;
            _defaultPicture = defaultPicture;
            _baseUrl = baseUrl;
            _bannerUrl = bannerUrl;
            _twitterShareMessage = twitterShareMessage;
        }

        /// <summary>
        /// Sends a recommendation e-mail containing a direct link to a nominee's details page.
        /// </summary>
        /// <param name="nominee">Nominee to recomment</param>
        /// <param name="vm">POST data</param>
        protected abstract void SendRecommendEmail(Nominee nominee, RecommendViewModel vm);

        /// <summary>
        /// Returns a fully qualified URL to a nominee's details page
        /// </summary>
        /// <param name="nominee">Nominee</param>
        /// <returns>Fully qualified URL</returns>
        protected string GetDetailsUrl(Nominee nominee)
        {
            var path = Url.Action(nameof(Details), new { slug = nominee.Slug, id = nominee.Id });
            var url = new Uri(_baseUrl, path);
            return url.ToString();
        }

        /// <summary>
        /// Show an index page for either Ildsjael og Forening containing a list of nominees.
        /// Nominees are not compiled through this action, but are rather fetched through AJAX
        /// with a call to List().
        /// </summary>
        /// <returns>View</returns>
        [HttpGet]
        public virtual ActionResult Index()
        {
            var contest = _contestHelper.GetCurrentContest(_contestType);
            var vm = new TIndexViewModel();

            vm.PhotoUrl = _bannerUrl;
            vm.IsActive = (contest != null) ? _contestHelper.ContestIsActive(contest) : false;

            if (vm is ForeningIndexViewModel)
            {
                vm.FrontpageText = _settingsHelper.GetFrontpageTextForening();
                vm.FrontpageTitle = _settingsHelper.GetFrontpageTitleForening();
            }

            return View(vm);
        }

        /// <summary>
        /// Returns a Json-encoded list of nominees. The list may optionally
        /// be filtered to produce different results.
        /// </summary>
        /// <param name="offset">Zero-based query offset</param>
        /// <param name="limit">Query length limit</param>
        /// <param name="order">How to order results, can be "latest", "oldest", "top" or "random"</param>
        /// <param name="randomizeToken">Token to randomize results</param>
        /// <param name="phrase">Search name and city for this text</param>
        /// <returns></returns>
        [HttpGet]
        public virtual ActionResult List(int offset = 0, int limit = 15, string order = "latest", int randomizeToken = 225, string phrase = null)
        {
            var query = _nomineeRepo.AsQueryable().OfType<TDataModel>();

            // Filter out non-accepted nominations
            query = query.Where(n => n.IsAccepted);

            // Text search by name or city
            if (!string.IsNullOrWhiteSpace(phrase))
            {
                phrase = phrase.Trim().ToLower();
                query = query.Where(n => n.Name.ToLower().Contains(phrase) ||
                                         n.City.ToLower().Contains(phrase) ||
                                         n.Zipcode.Contains(phrase));
            }

            // Order results
            switch (order.ToLower())
            {
                case "latest":
                    query = query.OrderByDescending(n => n.Accepted);
                    break;
                case "oldest":
                    query = query.OrderBy(n => n.Accepted);
                    break;
                case "top":
                    query = query.OrderBy(n => n.Rank);
                    break;
                case "random":
                default:
                    query = query.OrderBy(n => n.RandomToken % randomizeToken).ThenBy(n => n.Guid);
                    break;
            }

            // Count total results without filtering
            var total = query.Count();

            // Offset and limit
            query = query.Skip(offset).Take(limit);

            // Make json
            var responseData = new
            {
                total = total,
                offset = offset,
                entries = GetNomineeViewModelList(query.ToList()),
            };

            return Json(responseData, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Create mapping from a list of Nominee data models to
        /// a list of NomineeViewModel view models. This takes care
        /// of populating the view model with attributes which the
        /// automapper can't handle.
        /// </summary>
        /// <param name="nominees">The list of nominees to map to view models</param>
        /// <returns>The list of view models</returns>
        private IList<TDetailsViewModel> GetNomineeViewModelList(IList<TDataModel> nominees)
        {
            var entries = new List<TDetailsViewModel>();

            foreach (var nominee in nominees)
            {
                var vm = _mapper.Map<TDetailsViewModel>(nominee);
                vm.PictureUrl = _fileLocator.GetUrl(nominee, FileVariant.ProfilePicture);
                vm.RelativeUrl = Url.Action("Details", new {slug = nominee.Slug, id = nominee.Id});
                vm.AbsoluteUrl = GetDetailsUrl(nominee);
                vm.TwitterShareMessage = String.Format(_twitterShareMessage, nominee.Name);
                entries.Add(vm);
            }

            return entries;
        }

        /// <summary>
        /// Place a vote on a nominee. The voting person must be
        /// authenticated via Facebook. Also, the contest, in which
        /// the nominee is nominated within, must be active. And lastly,
        /// the voting person must not have voted on a nominee before.
        /// </summary>
        /// <param name="guid">Guid of nominee to vote on</param>
        /// <returns>Json encoded response (see implementation for details)</returns>
        [HttpPost]
        public virtual ActionResult PlaceVote(Guid guid)
        {
            var query = _nomineeRepo.AsQueryable();
            var nominee = query.SingleOrDefault(n => n.Guid == guid && n.IsAccepted);

            // Invalid (unknown) Guid provided by client
            if (nominee == null)
                return Json(new { guid = guid, success = false, error = 1, message = "Nominee not found" });

            // Contest is inactive
            if (!_contestHelper.ContestIsActive(nominee.Contest))
                return Json(new { guid = nominee.Guid, success = false, error = 2, message = "Contest is currently not active" });

            // User is not authenticated via Facebook
            if (!_fbAuth.IsRegistered())
                return Json(new { guid = nominee.Guid, success = false, error = 3, message = "User is not authenticated via Facebook" });

            // User has already voted on this nominee
            if (_voteHelper.HasVotedOn(_fbAuth.UserId, nominee))
                return Json(new { guid = nominee.Guid, success = false, error = 4, message = "User already voted on this nominee" });

            // Validation success!
            _voteHelper.PlaceVote(nominee, _fbAuth);
            _contestHelper.UpdateRanks(nominee.Contest);

            return Json(new
            {
                success = true,
                guid = nominee.Guid,
                voteCount = nominee.VoteCount,
                rank = nominee.Rank,
                name = nominee.Name,
            });
        }

        /// <summary>
        /// Retrieve a Json-encoded list of nominees ranks.
        /// </summary>
        /// <param name="guids">Array of nominated's Guids to get rank for</param>
        /// <returns>Json encoded list of objects having Guid and Rank keys</returns>
        [HttpGet]
        public virtual ActionResult GetRanks(Guid[] guids)
        {
            var query = from nominee in _nomineeRepo.AsQueryable()
                        where guids.Contains(nominee.Guid)
                        select new { Guid = nominee.Guid, Rank = nominee.Rank };

            return Json(query.ToList(), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Show details about a nominee
        /// </summary>
        /// <param name="slug">URL slug</param>
        /// <param name="id">Nominee ID</param>
        /// <returns>View</returns>
        [HttpGet]
        public virtual ActionResult Details(string slug, int id)
        {
            var nominee = _nomineeRepo.AsQueryable()
                .OfType<TDataModel>()
                .FirstOrDefault(n => n.Id == id && n.IsAccepted);

            if (nominee == null)
                return HttpNotFound();

            // If slug does not match expedted slug, 301 redirect to proper URL.
            // This is to avoid multiple URLs with the same content, which can
            // be seen as duplicate content in the eyes of search engines, and
            // may be punished.
            if (slug != nominee.Slug)
            {
                var url = Url.Action(nameof(Details), new { slug = nominee.Slug, id = nominee.Id });
                return RedirectPermanent(url);
            }

            var vm = _mapper.Map<TDetailsViewModel>(nominee);
            vm.BannerPhotoUrl = _bannerUrl;
            vm.AbsoluteUrl = GetDetailsUrl(nominee);
            vm.IsActive = nominee.Contest.IsActive;
            vm.TwitterShareMessage = String.Format(_twitterShareMessage, nominee.Name);

            if (nominee.Picture != null)
            {
                vm.PictureUrl = _fileLocator.GetUrl(nominee, FileVariant.ProfilePicture);
                vm.PictureAbsoluteUrl = new Uri(_baseUrl, vm.PictureUrl).ToString();

                vm.PosterSolid.ThumbUrl = _fileLocator.GetUrl(nominee, FileVariant.PosterSolidThumb);
                vm.PosterSolid.LinkUrl = _fileLocator.GetUrl(nominee, FileVariant.PosterSolid);

                vm.PosterFbPic.ThumbUrl = _fileLocator.GetUrl(nominee, FileVariant.PosterPictureThumb);
                vm.PosterFbPic.LinkUrl = _fileLocator.GetUrl(nominee, FileVariant.PosterPicture);

                vm.FlyerSolid.ThumbUrl = _fileLocator.GetUrl(nominee, FileVariant.FlyerSolidThumb);
                vm.FlyerSolid.LinkUrl = _fileLocator.GetUrl(nominee, FileVariant.FlyerSolid);

                vm.FlyerFbPic.ThumbUrl = _fileLocator.GetUrl(nominee, FileVariant.FlyerPictureThumb);
                vm.FlyerFbPic.LinkUrl = _fileLocator.GetUrl(nominee, FileVariant.FlyerPicture);

                vm.FbCover.ThumbUrl = _fileLocator.GetUrl(nominee, FileVariant.FacebookCoverPicture);
                vm.FbCover.LinkUrl = _fileLocator.GetUrl(nominee, FileVariant.FacebookCoverPicture);

                vm.FbCoverSolid.ThumbUrl = _fileLocator.GetUrl(nominee, FileVariant.FacebookCoverSolid);
                vm.FbCoverSolid.LinkUrl = _fileLocator.GetUrl(nominee, FileVariant.FacebookCoverSolid);
            }
            else
            {
                vm.PictureUrl = _defaultPicture;
            }
            vm.TotalNominees = _contestHelper.NumberOfNominees(nominee.Contest.Id);

            return View(vm);
        }

        /// <summary>
        /// Handles a recommendation formular submit
        /// </summary>
        /// <param name="vm">Viewmodel</param>
        /// <returns>JSON encoded data with status</returns>
        [HttpPost]
        public virtual ActionResult Recommend(RecommendViewModel vm)
        {
            var nominee = _nomineeRepo.AsQueryable()
                .OfType<TDataModel>()
                .FirstOrDefault(n => n.Guid == vm.Guid);

            // Invalid (unknown) Guid provided by client
            if (nominee == null)
                return Json(new { guid = vm.Guid, success = false, error = 1, message = "Nominee not found" });

            SendRecommendEmail(nominee, vm);

            return Json(new { success = true });
        }
    }
}
