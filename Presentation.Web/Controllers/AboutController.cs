﻿using System.Web.Mvc;
using Presentation.Web.Models.About;
using Core.ApplicationServices;

namespace Presentation.Web.Controllers
{
    public class AboutController : Controller
    {
        private readonly ISettingHelper _settingsHelper;
        private readonly string _ildsjaelBannerUrl;
        private readonly string _foreningBannerUrl;

        public AboutController(
            ISettingHelper settingsHelper,
            string ildsjaelBannerUrl,
            string foreningBannerUrl)
        {
            _settingsHelper = settingsHelper;
            _ildsjaelBannerUrl = ildsjaelBannerUrl;
            _foreningBannerUrl = foreningBannerUrl;
        }

        [Route("~/forening/om")]
        public ActionResult ForeningAbout()
        {
            var vm = new ForeningAboutViewModel()
            {
                photoUrl = _foreningBannerUrl,
                AboutText = _settingsHelper.GetForeningAboutText()
            };

            return View(vm);
        }
    }
}