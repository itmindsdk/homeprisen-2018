﻿using System;
using System.Web.Mvc;
using System.Linq;
using Core.ApplicationServices;
using Core.DomainModel;
using Core.DomainServices;

namespace Presentation.Web.Controllers
{
    public class ContestController : Controller
    {
        private readonly IGenericRepository<Nominee> _nomineeRepo;
        private readonly IContestHelper _contestHelper;

        public ContestController(IGenericRepository<Nominee> nomineeRepo, IContestHelper contestHelper)
        {
            _nomineeRepo = nomineeRepo;
            _contestHelper = contestHelper;
        }

        [HttpGet]
        public ActionResult CheckEmailAvailability(ContestType contestType, string email, Guid? Guid = null)
        {
            bool available = false;
            var contest = _contestHelper.GetActiveContest(contestType);

            if (contest != null)
            {
                if(Guid != null)
                {
                    var nominee = _nomineeRepo.AsQueryable().FirstOrDefault(x => x.Guid == Guid);
                    available = !_contestHelper.HasNominatedEmail(contest, email, nominee);
                }
                else
                {
                    available = !_contestHelper.HasNominatedEmail(contest, email);
                }
            }

            return Json(available, JsonRequestBehavior.AllowGet);
        }
    }
}