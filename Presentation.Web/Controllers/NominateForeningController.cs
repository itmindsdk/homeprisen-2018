﻿using AutoMapper;
using Core.ApplicationServices;
using Core.ApplicationServices.Images;
using Core.ApplicationServices.NomineeFiles;
using Core.DomainModel;
using Core.DomainServices;
using Postal;
using Presentation.Web.Emails;
using Presentation.Web.Models.Nominate;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Presentation.Web.Controllers
{
    public class NominateForeningController : NominateControllerBase<ForeningNominee, NominateForeningViewModel, AcceptForeningViewModel>
    {
        private readonly IEmailService _emailService;
        private readonly Uri _baseUrl;

        public NominateForeningController(
            IMapper mapper,
            IUnitOfWork unitOfWork,
            IGenericRepository<Nominee> nomineeRepo,
            IGenericRepository<Picture> pictureRepo,
            IGenericRepository<Category> categoryRepo,
            INomineeFiles nomineeFiles,
            IContestHelper contestHelper,
            IFileLocator fileLocator,
            IImageValidator imgValidator,
            IImageRotate imgRotate,
            IEmailService emailService,
            string defaultPicture,
            int maxUploadSize,
            Uri baseUrl,
            IRealtorService realtorService,
            bool useRealtorMailService)
            : base(ContestType.Forening,
                   mapper,
                   unitOfWork,
                   nomineeRepo,
                   pictureRepo,
                   categoryRepo,
                   nomineeFiles,
                   contestHelper,
                   fileLocator,
                   imgValidator,
                   imgRotate,
                   defaultPicture,
                   maxUploadSize,
                   realtorService,
                   useRealtorMailService)
        {
            _emailService = emailService;
            _baseUrl = baseUrl;
        }

        [HttpGet]
        [Route("~/forening/nominer")]
        public override ActionResult Nominate()
        {
            return base.Nominate();
        }

        [HttpPost]
        [Route("~/forening/nominer")]
        public override ActionResult Nominate(NominateForeningViewModel vm)
        {
            return base.Nominate(vm);
        }

        [HttpGet]
        [Route("~/forening/tak")]
        public override ActionResult Thanks(string NomineeName)
        {
            return base.Thanks(NomineeName);
        }

        [HttpGet]
        [Route("~/forening/accepter")]
        public override ActionResult Accept(Guid guid)
        {
            return base.Accept(guid);
        }

        [HttpPost]
        [Route("~/forening/accepter")]
        public override ActionResult Accept(AcceptForeningViewModel vm)
        {
            return base.Accept(vm);
        }

        [HttpGet]
        [Route("~/forening/afvis")]
        public override ActionResult Decline(Guid guid)
        {
            return base.Decline(guid);
        }

        protected override void SendNominatorThanksEmail(Nominee nominee)
        {
            var email = new ForeningNominatorThanksEmail()
            {
                Nominee = (ForeningNominee)nominee,
            };
            _emailService.Send(email);
        }

        protected override void SendNominationEmail(Nominee nominee)
        {
            var acceptUrl = new Uri(_baseUrl, Url.Action(nameof(Accept), new { guid = nominee.Guid }));
            var aboutUrl = new Uri(_baseUrl, Url.Action(nameof(AboutController.ForeningAbout), nameof(AboutController).Replace("Controller", "")));
            var email = new ForeningNominationEmail()
            {
                Nominee = (ForeningNominee)nominee,
                AcceptUrl = acceptUrl,
                AboutUrl = aboutUrl
            };
            _emailService.Send(email);
        }

        protected override void SendNomineeAcceptedEmail(Nominee nominee)
        {
            var url = new Uri(_baseUrl, Url.Action(nameof(Accept), new {guid = nominee.Guid}));
            var DetailsUrl = new Uri(_baseUrl,
                Url.Action("Details", "NomineeForening", new { slug = nominee.Slug, id = nominee.Id }));
            var email = new ForeningNomineeAcceptedEmail()
            {
                Nominee = (ForeningNominee)nominee,
                EditUrl = url,
                NomineeUrl = DetailsUrl
            };
            _emailService.Send(email);
        }

        protected override void SendNominatorAcceptedEmail(Nominee nominee)
        {
            var url = new Uri(_baseUrl,
                Url.Action("Details", "NomineeForening", new {slug = nominee.Slug, id = nominee.Id}));
            var email = new ForeningNominatorAcceptedEmail()
            {
                Nominee = (ForeningNominee)nominee,
                NomineeUrl = url,
            };
            _emailService.Send(email);
        }

        protected override void SendRealtorAcceptedEmail(Nominee nominee, IList<Realtor> realtorList)
        {
            var url = new Uri(_baseUrl,
                Url.Action("Details", "NomineeForening", new { slug = nominee.Slug, id = nominee.Id }));

            foreach (var realtor in realtorList)
            {
                var email = new RealtorAcceptedEmail()
                {
                    Nominee = (ForeningNominee)nominee,
                    NomineeUrl = url,
                    Realtor = realtor
                };
                _emailService.Send(email);
            }
        }

        protected override string GetDetailsUrl(Nominee nominee)
        {
            object param = new { slug = nominee.Slug, id = nominee.Id };
            var uri = new Uri(_baseUrl, Url.Action("Details", "NomineeForening", param));
            return uri.ToString();
        }
    }
}
