﻿using System.IO;
using System.Web;
using System.Web.Mvc;
using Core.ApplicationServices;
using Ganss.XSS;
using Presentation.Web.Models.SettingsAdmin;

namespace Presentation.Web.Controllers
{
    [Authorize]
    public class AdminSettingsController : Controller
    {
        private readonly ISettingHelper _settingsHelper;
        private readonly string _ildsjaelFrontpageBannerPath;
        private readonly string _ildsjaelAboutBannerPath;
        private readonly string _foreningFrontpageBannerPath;
        private readonly string _foreningAboutBannerPath;

        public AdminSettingsController(
            ISettingHelper settingsHelper,
            string ildsjaelFrontpageBannerPath,
            string ildsjaelAboutBannerPath,
            string foreningFrontpageBannerPath,
            string foreningAboutBannerPath)
        {
            _settingsHelper = settingsHelper;
            _ildsjaelFrontpageBannerPath = ildsjaelFrontpageBannerPath;
            _ildsjaelAboutBannerPath = ildsjaelAboutBannerPath;
            _foreningFrontpageBannerPath = foreningFrontpageBannerPath;
            _foreningAboutBannerPath = foreningAboutBannerPath;
        }

        [Route("~/admin/settings")]
        public ActionResult Index()
        {
            var vm = new SettingsAdminViewModel()
            {
                FrontpageTextForening = _settingsHelper.GetFrontpageTextForening(),
                FrontpageTextIldsjael = _settingsHelper.GetFrontpageTextIldsjael(),
                FrontpageTitleForening = _settingsHelper.GetFrontpageTitleForening(),
                FrontpageTitleIldsjael = _settingsHelper.GetFrontpageTitleIldsjael(),
                ForeningAboutText = _settingsHelper.GetForeningAboutText(),
                IldsjaelAboutText = _settingsHelper.GetIldsjaelAboutText(),
                DefaultContestType = _settingsHelper.GetDefaultContestType(),
                ForeningHidden = _settingsHelper.GetForeningHidden(),
                IldsjaelHidden = _settingsHelper.GetIldsjaelHidden()
            };

            return View(vm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("~/admin/settings")]
        public ActionResult Index(SettingsAdminViewModel vm)
        {
            var sanitizer = new HtmlSanitizer();

            _settingsHelper.SetFrontpageTextForening(sanitizer.Sanitize(vm.FrontpageTextForening));
            _settingsHelper.SetFrontpageTextIldsjael(sanitizer.Sanitize(vm.FrontpageTextIldsjael));
            _settingsHelper.SetFrontpageTitleForening(sanitizer.Sanitize(vm.FrontpageTitleForening));
            _settingsHelper.SetFrontpageTitleIldsjael(sanitizer.Sanitize(vm.FrontpageTitleIldsjael));
            _settingsHelper.SetForeningAboutText(sanitizer.Sanitize(vm.ForeningAboutText));
            _settingsHelper.SetIldsjaelAboutText(sanitizer.Sanitize(vm.IldsjaelAboutText));
            _settingsHelper.SetDefaultContestType(vm.DefaultContestType);
            _settingsHelper.SetForeningHidden(vm.ForeningHidden);
            _settingsHelper.SetIldsjaelHidden(vm.IldsjaelHidden);

            if (vm.IldsjaelFrontpagePicture != null)
                HandleFileUpload(vm.IldsjaelFrontpagePicture, _ildsjaelFrontpageBannerPath);

            if (vm.IldsjaelAboutPicture != null)
                HandleFileUpload(vm.IldsjaelAboutPicture, _ildsjaelAboutBannerPath);

            if (vm.ForeningFrontpagePicture != null)
                HandleFileUpload(vm.ForeningFrontpagePicture, _foreningFrontpageBannerPath);

            if (vm.ForeningAboutPicture != null)
                HandleFileUpload(vm.ForeningAboutPicture, _foreningAboutBannerPath);

            vm.Message = "Indstillinger blev gemt";

            return View(vm);
        }

        private void HandleFileUpload(HttpPostedFileBase file, string filePath)
        {
            if (file.ContentLength > 0)
            {
                using (var writer = new FileStream(filePath, FileMode.OpenOrCreate))
                {
                    file.InputStream.Position = 0;
                    file.InputStream.CopyTo(writer);
                }
            }
        }
    }
}
