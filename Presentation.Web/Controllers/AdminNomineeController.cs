﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using PagedList;
using Core.DomainModel;
using Core.DomainServices;
using AutoMapper;
using Presentation.Web.Models.NomineeAdmin;
using Core.ApplicationServices.NomineeFiles;

namespace Presentation.Web.Controllers
{
    [Authorize]
    public class AdminNomineeController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IGenericRepository<Nominee> _nomineeRepo;
        private readonly IGenericRepository<Contest> _contestRepo;
        private readonly IGenericRepository<Vote> _voteRepo;
        private readonly INomineeFiles _nomineeFiles;
        private readonly Uri _baseUrl;

        public AdminNomineeController(
            IMapper mapper,
            IUnitOfWork unitOfWork,
            IGenericRepository<Nominee> nomineeRepo,
            IGenericRepository<Contest> contestRepo,
            IGenericRepository<Vote> voteRepo,
            INomineeFiles nomineeFiles,
            Uri baseUrl)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
            _nomineeRepo = nomineeRepo;
            _contestRepo = contestRepo;
            _voteRepo = voteRepo;
            _nomineeFiles = nomineeFiles;
            _baseUrl = baseUrl;
        }

        [Route("~/admin/nominerede")]
        public ActionResult Index(string query = "", string sort = "", int? contestId = null, int page = 1)
        {
            int pageSize = 100;
            var nominees = _nomineeRepo.AsQueryable();

            // Filter by free text search query
            if(!string.IsNullOrWhiteSpace(query))
            {
                query = query.ToLower();
                nominees = nominees.Where(x => x.Name.ToLower().Contains(query) ||
                                               x.Email.ToLower().Contains(query) ||
                                               x.Phone.ToLower().Contains(query) ||
                                               x.City.ToLower().Contains(query));
            }

            // Order results
            switch(sort)
            {
                case "name":
                    nominees = nominees.OrderBy(x => x.Name);
                    break;
                case "votes":
                    nominees = nominees.OrderByDescending(x => x.VoteCount);
                    break;
                default:
                    nominees = nominees.OrderByDescending(x => x.Nominated);
                    break;
            }

            // Filter by contest
            if (contestId != null)
                nominees = nominees.Where(x => x.ContestId == contestId);

            var pagedList = nominees.ToPagedList(page, pageSize);
            var mapped = _mapper.Map<IEnumerable<NomineeAdminViewModel>>(nominees);

            ViewBag.Query = query;
            ViewBag.Sort = sort;
            ViewBag.ContestId = contestId;

            // Build SelectList for contests
            var contests = _contestRepo.AsQueryable().ToList();
            ViewBag.ContestSelect = new SelectList(contests, "Id", "Name", contestId);

            // Build SelectList for sorting options
            var sortSelect = new List<SelectListItem>()
            {
                new SelectListItem() { Value = "name", Text = "Navn" },
                new SelectListItem() { Value = "votes", Text = "Antal stemmer" },
                new SelectListItem() { Value = "nominated", Text = "Nyeste" },
            };
            ViewBag.SortSelect = new SelectList(sortSelect, "Value", "Text", sort);

            return View(mapped.ToPagedList(page, pageSize));
        }

        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var contests = _contestRepo.AsQueryable().ToList();
            var nominee = _nomineeRepo.AsQueryable().FirstOrDefault(x => x.Guid == id);
            Uri acceptDirectLink;

            if (nominee == null)
            {
                return HttpNotFound();
            }

            ViewBag.ContestId = new SelectList(contests, "Id", "Name", nominee.ContestId);
            var vm = _mapper.Map<ForeningNomineeAdminViewModel>(nominee);
            var path = Url.Action(nameof(NominateForeningController.Accept), "NominateForening",
                new { Guid = nominee.Guid });
            vm.AcceptDirectLink = new Uri(_baseUrl, path).ToString();
            return View("EditForening", vm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditForening(ForeningNomineeAdminViewModel vm)
        {
            var nominee = _mapper.Map<ForeningNominee>(vm);

            if (ModelState.IsValid)
            {
                _nomineeRepo.Update(nominee);
                _unitOfWork.Save();
                return RedirectToAction("Index");
            }

            var contests = _contestRepo.AsQueryable().ToList();
            ViewBag.ContestId = new SelectList(contests, "Id", "Name", nominee.ContestId);

            return View(vm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditIldsjael(IldsjaelNomineeAdminViewModel vm)
        {
            var nominee = _mapper.Map<IldsjaelNominee>(vm);

            if (ModelState.IsValid)
            {
                _nomineeRepo.Update(nominee);
                _unitOfWork.Save();
                return RedirectToAction("Index");
            }

            var contests = _contestRepo.AsQueryable().ToList();
            ViewBag.ContestId = new SelectList(contests, "Id", "Name", nominee.ContestId);

            return View(vm);
        }

        public ActionResult Delete(Guid? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var nominee = _nomineeRepo.AsQueryable().FirstOrDefault(x => x.Guid == id);
            if (nominee == null)
                return HttpNotFound();

            var vm = _mapper.Map<NomineeAdminViewModel>(nominee);
            return View(vm);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            var nominee = _nomineeRepo.AsQueryable().FirstOrDefault(x => x.Guid == id);
            if (nominee != null)
                _nomineeFiles.RemoveFiles(nominee);

            _nomineeRepo.DeleteByKey(id);
            _unitOfWork.Save();

            return RedirectToAction("Index");
        }

        public ActionResult ShowVotes(Guid id)
        {
            var votes = _voteRepo.AsQueryable().Where(x => x.NomineeGuid == id).OrderBy(x => x.Created);
            var mapped = Mapper.Map<IEnumerable<NomineeVotesViewModel>>(votes);

            return View(mapped);
        }
    }
}
