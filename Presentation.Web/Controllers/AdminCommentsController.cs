﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using PagedList;
using Core.DomainModel;
using Core.DomainServices;
using AutoMapper;
using Presentation.Web.Models.NomineeAdmin;
using Core.ApplicationServices;

namespace Presentation.Web.Controllers
{
    [Authorize]
    public class AdminCommentsController : Controller
    {
        private IMapper _mapper { get;  set; }
        private IUnitOfWork _unitOfWork { get; set; }
        private IGenericRepository<Comment> _commentRepo { get; set; }

        public AdminCommentsController(IMapper mapper, IUnitOfWork unitOfWork, IGenericRepository<Comment> commentRepo)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
            _commentRepo = commentRepo;
        }

        [Route("~/admin/kommentarer")]
        public ActionResult Index()
        {
            var comments = _commentRepo.AsQueryable().OrderByDescending(x => x.Id);
            var mapped = _mapper.Map<IEnumerable<CommentAdminViewModel>>(comments);
            return View(mapped);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var comment = _commentRepo.AsQueryable().FirstOrDefault(x => x.Id == id);
            if (comment == null)
                return HttpNotFound();

            var vm = _mapper.Map<CommentAdminViewModel>(comment);
            return View(vm);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            _commentRepo.DeleteByKey(id);
            _unitOfWork.Save();

            return RedirectToAction("Index");
        }
    }
}
