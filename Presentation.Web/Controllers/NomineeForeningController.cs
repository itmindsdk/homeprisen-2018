﻿using System;
using AutoMapper;
using Core.ApplicationServices;
using Core.DomainModel;
using Core.DomainServices;
using Presentation.Web.Models.Nominee;
using System.Web.Mvc;
using Core.ApplicationServices.Facebook;
using Presentation.Web.Emails;

namespace Presentation.Web.Controllers
{
    public class NomineeForeningController : NomineeControllerBase<ForeningNominee, ForeningDetailsViewModel, ForeningIndexViewModel>
    {
        public NomineeForeningController(
            IMapper mapper,
            IGenericRepository<Nominee> nomineeRepo,
            IContestHelper contestHelper,
            IVoteHelper voteHelper,
            IFacebookAuthHelper fbAuth,
            IFileLocator fileLocator,
            ISettingHelper settingHelper,
            string defaultPicture,
            Uri baseUrl,
            string bannerUrl,
            string twitterShareMessage)
            : base(ContestType.Forening,
                   mapper,
                   nomineeRepo,
                   contestHelper,
                   voteHelper,
                   fbAuth,
                   fileLocator,
                   settingHelper,
                   defaultPicture,
                   baseUrl,
                   bannerUrl,
                   twitterShareMessage) {}

        [HttpGet]
        [Route("~/forening")]
        public override ActionResult Index()
        {
            return base.Index();
        }

        [HttpGet]
        [Route("~/forening/{slug}-{id:int}")]
        public override ActionResult Details(string slug, int id)
        {
            return base.Details(slug, id);
        }

        protected override void SendRecommendEmail(Nominee nominee, RecommendViewModel vm)
        {
            var email = new ForeningRecommendEmail()
            {
                Nominee = (ForeningNominee)nominee,
                NomineeDetailsUrl = GetDetailsUrl(nominee),
                SenderName = vm.SenderName,
                SenderEmail = vm.SenderEmail,
                RecipientName = vm.RecipientName,
                RecipientEmail = vm.RecipientEmail,
                Message = vm.Message,
            };

            email.Send();
        }
    }
}
