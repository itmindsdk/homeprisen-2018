﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Core.ApplicationServices;
using Presentation.Web.Models.Menu;

namespace Presentation.Web.Controllers
{
    [ChildActionOnly]
    public class MenuController : Controller
    {
        private readonly ISettingHelper _settingsHelper;

        public MenuController(ISettingHelper settingsHelper)
        {
            _settingsHelper = settingsHelper;
        }

        public ActionResult Forening()
        {
            var vm = new ForeningMenuViewModel();

            return View(vm);
        }
    }
}