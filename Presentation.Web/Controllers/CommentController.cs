﻿using System;
using System.Linq;
using System.Web.Mvc;
using Core.ApplicationServices;
using Core.ApplicationServices.Facebook;
using Core.DomainModel;
using Core.DomainServices;

namespace Presentation.Web.Controllers
{
    // TODO: Comment
    public class CommentController : Controller
    {
        private readonly IGenericRepository<Nominee> _nomineeRepo;
        private readonly IGenericRepository<Comment> _commentRepo;
        private readonly IContestHelper _contestHelper;
        private readonly ICommentHelper _commentHelper;
        private readonly IVoteHelper _voteHelper;
        private readonly IFacebookAuthHelper _fbAuth;

        public CommentController(
            IGenericRepository<Nominee> nomineeRepo,
            IGenericRepository<Comment> commentRepo,
            IContestHelper contestHelper,
            ICommentHelper commentHelper,
            IVoteHelper voteHelper,
            IFacebookAuthHelper fbAuth)
        {
            _nomineeRepo = nomineeRepo;
            _commentRepo = commentRepo;
            _contestHelper = contestHelper;
            _commentHelper = commentHelper;
            _voteHelper = voteHelper;
            _fbAuth = fbAuth;
        }

        /// <summary>
        /// Place a comment on a nominee. The voting person must be
        /// authenticated via Facebook and must have placed a vote on
        /// the nominee to be able to comment. Also, the contest, in which
        /// the nominee is nominated within, must be active.
        /// </summary>
        /// <param name="guid">Guid of nominee to comment on</param>
        /// <returns>Json encoded response (see implementation for details)</returns>
        [HttpPost]
        public ActionResult PlaceComment(Guid guid, string comment)
        {
            var query = _nomineeRepo.AsQueryable();
            var nominee = query.SingleOrDefault(n => n.Guid == guid && n.IsAccepted);

            // Invalid (unknown) Guid provided by client
            if (nominee == null)
                return Json(new { guid = guid, success = false, error = 1, message = "Nominee not found" });

            // Contest is inactive
            if (!_contestHelper.ContestIsActive(nominee.Contest))
                return Json(new { guid = nominee.Guid, success = false, error = 2, message = "Contest is currently not active" });

            // User is not authenticated via Facebook
            if (!_fbAuth.IsRegistered())
                return Json(new { guid = nominee.Guid, success = false, error = 3, message = "User is not authenticated via Facebook" });

            // User has not voted on this nominee
            if (!_voteHelper.HasVotedOn(_fbAuth.UserId, nominee))
                return Json(new { guid = nominee.Guid, success = false, error = 4, message = "User has not voted on this nominee" });

            // User has not voted on this nominee
            if (_commentHelper.HasCommentedOn(_fbAuth.UserId, nominee))
                return Json(new { guid = nominee.Guid, success = false, error = 5, message = "User has already commented on this nominee" });

            // Validation success!
            _commentHelper.PlaceComment(nominee, _fbAuth, comment);
            return Json(new { guid = nominee.Guid, success = true });
        }

        // TODO: Comment
        [HttpGet]
        public ActionResult GetComments(Guid nomineeId, int offset = 0, int limit = 10)
        {
            var query = _commentRepo.AsQueryable()
                .Where(c => c.NomineeId == nomineeId)
                .OrderByDescending(c => c.Submitted);
            var total = query.Count();

            // Make json
            var responseData = new
            {
                total = total,
                offset = offset,
                entries = query.Skip(offset).Take(limit).ToList(),
            };

            return Json(responseData, JsonRequestBehavior.AllowGet);
        }
    }
}
