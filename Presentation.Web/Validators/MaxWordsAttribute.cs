﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Presentation.Web.Validators
{
    public class MaxWordsAttribute : ValidationAttribute, IClientValidatable
    {
        public MaxWordsAttribute(int wordCount, string errorMsg)
        {
            WordCount = wordCount;
            ErrorMsg = errorMsg;
        }

        public int WordCount { get; set; }
        public string ErrorMsg { get; set; }

        protected override ValidationResult IsValid(object value,ValidationContext validationContext)
        {
            if (value != null)
            {
                var wordCount = value.ToString().Split(' ').Length;

                if (wordCount > WordCount)
                {
                    return new ValidationResult(
                        ErrorMsg
                    );
                }
            }

            return ValidationResult.Success;
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metaData,ControllerContext context)
        {
            var rule = new ModelClientValidationRule();
            rule.ErrorMessage = ErrorMsg;
            rule.ValidationParameters.Add("wordcount",WordCount);
            rule.ValidationType = "maxwords";
            yield return rule;
        }
    }
}