﻿using Core.DomainModel;
using Postal;
using System;

namespace Presentation.Web.Emails
{
    public class IldsjaelNominationEmail : Email
    {
        public IldsjaelNominee Nominee { get; set; }
        public Uri AcceptUrl { get; set; }

        public IldsjaelNominationEmail() : base("IldsjaelNominationEmail") { }
    }
}