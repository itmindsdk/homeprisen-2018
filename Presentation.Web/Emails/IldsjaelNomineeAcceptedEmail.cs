﻿using Core.DomainModel;
using Postal;
using System;

namespace Presentation.Web.Emails
{
    public class IldsjaelNomineeAcceptedEmail : Email
    {
        public IldsjaelNominee Nominee { get; set; }
        public Uri EditUrl { get; set; }
        public Uri NomineeUrl { get; set; }
        public IldsjaelNomineeAcceptedEmail() : base("IldsjaelNomineeAcceptedEmail") { }
    }
}