﻿using Core.DomainModel;
using Postal;

namespace Presentation.Web.Emails
{
    public class IldsjaelNominatorThanksEmail : Email
    {
        public IldsjaelNominee Nominee { get; set; }

        public IldsjaelNominatorThanksEmail() : base("IldsjaelNominatorThanksEmail") { }
    }
}