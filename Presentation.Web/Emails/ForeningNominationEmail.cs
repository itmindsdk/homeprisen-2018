﻿using Core.DomainModel;
using Postal;
using System;

namespace Presentation.Web.Emails
{
    public class ForeningNominationEmail : Email
    {
        public ForeningNominee Nominee { get; set; }
        public Uri AcceptUrl { get; set; }
        public Uri AboutUrl { get; set; }

        public ForeningNominationEmail() : base("ForeningNominationEmail") { }
    }
}