﻿using Core.DomainModel;
using Postal;
using System;

namespace Presentation.Web.Emails
{
    public class ForeningRecommendEmail : Email
    {
        public ForeningNominee Nominee { get; set; }
        public string NomineeDetailsUrl { get; set; }

        public string SenderName { get; set; }
        public string SenderEmail { get; set; }
        public string RecipientName { get; set; }
        public string RecipientEmail { get; set; }
        public string Message { get; set; }

        public ForeningRecommendEmail() : base("ForeningRecommendEmail") { }
    }
}