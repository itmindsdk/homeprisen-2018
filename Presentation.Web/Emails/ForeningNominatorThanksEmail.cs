﻿using Core.DomainModel;
using Postal;

namespace Presentation.Web.Emails
{
    public class ForeningNominatorThanksEmail : Email
    {
        public ForeningNominee Nominee { get; set; }

        public ForeningNominatorThanksEmail() : base("ForeningNominatorThanksEmail") { }
    }
}