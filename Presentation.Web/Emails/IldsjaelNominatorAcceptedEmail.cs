﻿using Core.DomainModel;
using Postal;
using System;

namespace Presentation.Web.Emails
{
    public class IldsjaelNominatorAcceptedEmail : Email
    {
        public IldsjaelNominee Nominee { get; set; }
        public Uri NomineeUrl { get; set; }

        public IldsjaelNominatorAcceptedEmail() : base("IldsjaelNominatorAcceptedEmail") { }
    }
}