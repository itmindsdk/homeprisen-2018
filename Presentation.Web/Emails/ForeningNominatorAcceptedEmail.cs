﻿using Core.DomainModel;
using Postal;
using System;

namespace Presentation.Web.Emails
{
    public class ForeningNominatorAcceptedEmail : Email
    {
        public ForeningNominee Nominee { get; set; }
        public Uri NomineeUrl { get; set; }

        public ForeningNominatorAcceptedEmail() : base("ForeningNominatorAcceptedEmail") { }
    }
}