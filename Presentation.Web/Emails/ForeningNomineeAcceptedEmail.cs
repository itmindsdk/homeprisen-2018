﻿using Core.DomainModel;
using Postal;
using System;

namespace Presentation.Web.Emails
{
    public class ForeningNomineeAcceptedEmail : Email
    {
        public ForeningNominee Nominee { get; set; }
        public Uri EditUrl { get; set; }
        public Uri NomineeUrl { get; set; }
        public ForeningNomineeAcceptedEmail() : base("ForeningNomineeAcceptedEmail") { }
    }
}