﻿using Core.DomainModel;
using Postal;
using System;

namespace Presentation.Web.Emails
{
    public class RealtorAcceptedEmail : Email
    {
        public ForeningNominee Nominee { get; set; }
        public Uri NomineeUrl { get; set; }
        public Realtor Realtor { get; set; }
        public RealtorAcceptedEmail() : base("RealtorAcceptedEmail") { }
    }
}