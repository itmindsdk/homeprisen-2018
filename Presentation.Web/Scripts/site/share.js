﻿
function showShareMailModal() {
    var url = $(this).attr('data-url');
    var guid = $(this).attr('data-guid');
    var name = $(this).attr('data-name');

    var modal = $('#ShareMailModal');
    var form = modal.find('form');
    var inputs = form.find('input,textarea');
    var errorMsg = form.find('span');
    var feedback = modal.find('.mail-feedback');
    modal.find('.nominee-name').text(name);

    feedback.hide();

    // Reset input values
    inputs.val('');
    errorMsg.remove();
    // Set form action
    form.attr('action', url);

    // Set Nominee guid
    form.find('[name="Guid"]').attr('value', guid);
    form.show();
    modal.modal("show");
}

function showShareMailFeedback() {
    $('#ShareMailModal form').hide();
    $('.mail-feedback').show();
}

function showShareFacebookModal() {
    FB.ui({
        method: 'share',
        href: $(this).attr('data-url')
    }, function (response) { });
}


function generateTwitterUrl(msg,url) {
    return 'https://twitter.com/intent/tweet?text=' + encodeURI(msg) + '&url=' + encodeURI(url);
}


function applyTwitterProperties(e) {
    var msg = e.attr('data-msg');
    var url = e.attr('data-url');
    e.attr("href", generateTwitterUrl(msg, url));
}

function validateMailForm() {
    $('#ShareMailModal form').validate({
        rules: {
            SenderName: {
                required: true
            },
            SenderEmail: {
                required: true,
                email: true
            },
            RecipientName: {
                required: true
            },
            RecipientEmail: {
                required: true,
                email: true
            },
            Message: {
                required: true
            }
        },
        messages: {
            SenderName: "Indtast dit fulde navn",
            SenderEmail: {
                required: "Indtast din mailadresse",
                email: "Indtast venligst en gyldig mailadresse"
            },
            RecipientName: "Indtast din vens fulde navn",
            RecipientEmail: {
                required: "Indtast din vens mailadresse",
                email: "indtast venligst en gyldig mailadresse"
            },
            Message: "Indtast en besked til din ven"
        },
        focusInvalid: true,
        errorClass: "font-myriad-pro-regular text-danger error",
        errorElement: "span",
        submitHandler: function () {
            $('#ShareMailModal form').ajaxSubmit();
            showShareMailFeedback();
        }
    });
}

$(function() {
    validateMailForm();
    if (!$('.inActive').length) {
        $('.mail-share').on('click', showShareMailModal);
        $('.twitter-link').each(function() {
            applyTwitterProperties($(this));
        });
    } else {
        $('.twitter-link').each(function () {
            $(this).disabled = "true";
        });
    }
});
