﻿$(function () {
    var collapse = $('.collapse-navbar-mobile');

    $('.navbar-toggle').on('click', function () {
        if (collapse.hasClass("in") ||
            collapse.hasClass("collapsing")) {
            $(this).children().removeClass("bg-white");
        } else {
            $(this).children().addClass("bg-white");
        }
       
    });
});