﻿initRte("#IldsjaelAboutText");
initRte("#ForeningAboutText");
initRte('#FrontpageTextForening');
initRte('#FrontpageTextIldsjael');
initRte('#FrontpageTitleForening');
initRte('#FrontpageTitleIldsjael');

function initRte(selectorId) {
    tinymce.init({
        selector: selectorId,
        plugins: [
            'advlist autolink lists link image charmap print preview hr anchor pagebreak',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime media nonbreaking save table contextmenu directionality',
            'emoticons template paste textcolor colorpicker textpattern imagetools'
        ],
        toolbar1: 'bold, italic, underline, alignleft, aligncenter, alignright, alignjustify, styleselect, formatselect, bullist, numlist',
        toolbar2: 'forecolor backcolor',
        font_formats:
            "DIN Com Engschrift=DIN1451ComEngschrift;" +
            "Myriad Pro=MyriadPro;" +
            "Myriad Pro Light=MyriadProLight;"
    });
}