﻿
$(function () {
    var fbLinkTrigger = $('.facebook-link-trigger');
    var fbLink = $('#FacebookPictureUrl');
    var fbSpinner = $('.facebook-load-image-spinner');
    var fbError = $('.facebook-load-image-error');
    var fbSericeUrl = fbLinkTrigger.attr('data-service-url');
    var sumcontainer = $('.summary-container');
    var previewContainer = $('.image-container');
    var preview = $('.nominee-image');
    var uploadButtonTrigger = $(".UploadBtnTrigger");
    var uploadButton = $('#UploadBtn');
    var zipCodeInput = $('#nominee-zipcode-input');
    var municipalitiesContainer = $('.accept-nomination-municipalities');
    var municipalitiesDropdown = municipalitiesContainer.find('.municipalities-dropdown');
    var municipalitiesInput = municipalitiesContainer.find('.municipalities-input');
    var noMunicipalitiesError = municipalitiesContainer.find('.error-msg');

    // refresh municipality options on changed zipcode
    zipCodeInput.blur((e) => $.ajax({
        type: 'GET',
        url: "https://dawa.aws.dk/postnumre?nr=" + e.currentTarget.value,
        dataType: 'json',
        success: function (data) {
            data && data.length > 0 && data[0].kommuner.length > 0
                ? data[0].kommuner.length > 1
                    ? multipleMunicipalitiesFound(data[0].kommuner)
                    : singleMunicipalityFound(data[0].kommuner[0])
                : noMunicipalitiesFound()
        },
        error: noMunicipalitiesFound()
    }));

    municipalitiesDropdownChangeHandler = function (e) {
        municipalitiesInput.val(e.target.value);
    }

    multipleMunicipalitiesFound = function (municipalities) {
        municipalitiesDropdown.empty();
        municipalities.forEach(function (municipality) {
            municipalitiesDropdown.append($("<option />").val(municipality.navn).text(municipality.navn));
        });

        municipalitiesDropdown.change(municipalitiesDropdownChangeHandler);
        municipalitiesDropdown.fadeIn(300);

        municipalitiesInput.val(municipalities[0].navn);

        municipalitiesContainer
            .addClass('multiple-municipalities')
            .removeClass('no-municipalities')
            .removeClass('single-municipality');
    }

    singleMunicipalityFound = function (municipality) {
        municipalitiesContainer
            .removeClass('multiple-municipalities')
            .removeClass('no-municipalities')
            .addClass('single-municipality');
        
        municipalitiesDropdown.off('change');
        municipalitiesDropdown.empty();


        municipalitiesInput.val(municipality.navn);
        municipalitiesInput.prop('readonly', true);

    }

    noMunicipalitiesFound = function () {
        municipalitiesContainer
            .removeClass('multiple-municipalities')
            .addClass('no-municipalities')
            .removeClass('single-municipality');

        municipalitiesDropdown.off('change');
        municipalitiesDropdown.empty();

        municipalitiesInput.val('');
        municipalitiesInput.prop('readonly', false);
    }

    $('.decline-button').on('click', function (e) {
        e.preventDefault();

        var modal = $('#DeclineModal').first();

        modal.modal("show");

        $('.decline-btn').on('click', function () {
            $('#declineLink').trigger("click");
        });

        $('.cancel-btn').on('click', function () {
            modal.modal("hide");
        });
    });

    //show validation summary if there are errors
    if (sumcontainer.children('.field-validation-error').length > 0) {
        sumcontainer.show();
    } else {
        sumcontainer.hide();
    }

    //show validation summary on submit click to show client side validation errors
    $(":submit").on("click", function () {
        sumcontainer.show();
    });
    $.data($('#NominateForm')[0], 'validator').settings.submitHandler = function (form) {
        $('.submit-row').addClass('hidden');
        $('.loader-row').removeClass('hidden');
        form.submit();
    };

    var invokeFacebookPictureLinkService = function (profileUrl) {
        if (profileUrl === '') {
            fbError.addClass('hidden');
            return;
        }

        fbSpinner.removeClass('hidden');

        $.ajax({
            type: 'GET',
            url: fbSericeUrl,
            data: { facebookProfileUrl: profileUrl },
            dataType: 'json',
            success: function (data) {
                fbSpinner.addClass('hidden');
                if (data.PictureUrl) {
                    var t = new Date().getTime();
                    var url = data.PictureUrl + '&?' + t;
                    $('.nominee-image').attr('src', url);
                    fbLink.val(data.PictureUrl);
                    uploadButton.val('');
                    fbError.addClass('hidden');
                } else {
                    fbError.removeClass('hidden');
                }
            }
        });
    };

    // Propagates change event on facebook link to hidden text element
    fbLinkTrigger.on('input', function () {
        var profileUrl = $(this).val();

        if (profileUrl.indexOf('facebook.com/') !== -1)
            invokeFacebookPictureLinkService(profileUrl);
    });


    var displayUploadImage = function (file, options) {
        var _changeImage = function (img) {
            if ($(img).is('canvas')) {
                var newImg = new Image();
                newImg.src = img.toDataURL('image/jpeg');
                img = newImg;
            }

            previewContainer.empty();
            preview = $(img);
            preview.addClass('nominee-image');

            previewContainer.each(function () {
                var e = $(this);
                if (e.is(':visible'))
                    e.append(preview);
            });
        };

        loadImage(file, _changeImage, options);
    };

    var handleChooseUploadImage = function (e) {
        e.preventDefault();
        e = e.originalEvent;

        var target = e.dataTransfer || e.target;
        var file = target && target.files && target.files[0];
        var options = {};

        if (!file)
            return;

        loadImage.parseMetaData(file, function (data) {
            if (data.exif)
                options.orientation = data.exif.get('Orientation');

            displayUploadImage(file, options);
        });

        fbLinkTrigger.val('');
        fbLink.val('');
    };

    uploadButton.on('change', handleChooseUploadImage);
    uploadButtonTrigger.on('click', function () {
        uploadButton.trigger('click');
    });
});
