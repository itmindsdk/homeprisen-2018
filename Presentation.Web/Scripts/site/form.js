﻿
$(function() {
    $('input,textarea').on('blur', function() {
        var t = $(this);
        t.val(t.val().trim());
    });
});
