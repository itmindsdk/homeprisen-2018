﻿
function NomineeAjaxListing(e, state, appendEntries) {
    var loaderElem = e.find(".loader").first();
    var loadMoreElem = e.find(".load-more").first();

    var _state = state;

    var onLoadNomineesSuccess = function (data) {
        _state.remaining = data.total - (data.offset + _state.limit);
        _state.entries = $.merge(_state.entries, data.entries);
        appendEntries(_state, data.entries);
    };

    var onLoadNomineesFail = function () {
        loaderElem.hide();
        loadMoreElem.show();
    };

    this.next = function () {
        loadMoreElem.hide();
        loaderElem.show();

        var data = {
            offset: _state.offset,
            limit: _state.limit,
            phrase: _state.phrase,
            order: _state.order,
            randomizeToken: _state.randomizeToken,
        };

        $.get(GetNomineesUrl, data)
            .done(onLoadNomineesSuccess)
            .fail(onLoadNomineesFail);

        _state.offset += _state.limit;
    };

    this.setState = function (s) {
        _state = s;
    };
}


function handleClickVote(e) {
    e.preventDefault();

    var guid = $(this).attr("data-guid");
    var c = $(this).closest(".nominee-container");
    var rank = c.find(".rank");
    var voteCount = c.find(".voteCount");
    var nomineeName = $(this).attr("data-nomineeName");
    var nomineeUrl = $(this).attr("data-nomineeUrl");
    vote(guid, nomineeName, nomineeUrl, function(data) {
        rank.text(data.rank);
        voteCount.text(data.voteCount);
        updateRanks();
    });
}


function updateRanks() {
    var nominees = $(".nominee-container");
    var nomineeDict = {};
    var guids = new Array();

    // Construct a list of guid's and map nominees by guid
    nominees.each(function (i, e) {
        var nominee = $(e);
        var guid = nominee.attr("data-guid");
        guids.push(guid);
        nomineeDict[guid] = nominee;
    });

    $.ajax({
        method: "GET",
        url: GetRanksUrl,
        traditional: true, // required for ASP.NET to unmarshal the list
        data: { guids: guids },
        success: function (data) {
            // Update ranks in DOM tree
            for (var i = 0; i < data.length; i++) {
                var nominee = nomineeDict[data[i].Guid];
                if (nominee !== undefined) {
                    nominee.find(".rank").text(data[i].Rank);
                }
            }

            // Update history API with states
            var state = History.getState();
            for (var i = 0; i < state.data.entries.length; i++) {
                var guid = state.data.entries[i].Guid;
                var nominee = nomineeDict[guid];
                if (nominee !== undefined) {
                    state.data.entries[i].VoteCount = nominee.find(".voteCount").text();
                    state.data.entries[i].Rank = nominee.find(".rank").text();
                }
            }
            History.replaceState(state.data, state.title, state.url);
        }
    });
}


function shorten(s) {
    var maxLength = 110;
    if (s.length > maxLength)
        s = s.substr(0, maxLength) + "...";
    return s;
}


function newState() {
    return {
        offset: 0,
        limit: 6,
        order: "random",
        phrase: "",
        randomizeToken: Math.floor((Math.random() * 22) + 3), // [3;22]
        entries: [],
        remaining: 0,
    };
}


function getState() {
    var savedState = History.getState();

    if (savedState.data.entries !== undefined)
        return savedState.data;

    return newState();
}


$(function () {
    var template = $(".nominee-template").children().first();

    var list = $("#NomineeList");
    var containerElem = $(".nominees").first();
    var noResultsElem = $(".no-results").first();
    var loaderElem = $(".loader").first();
    var loadMoreElem = $(".load-more").first();
    var formElem = $("form.search-desktop").first();
    var formElemMobile = $("form.search-mobile").first();
    var searchInputElem = formElem.find("input.phrase");
    var searchInputElemMobile = formElemMobile.find("input.phrase");
    var top15Elem = formElem.find(".top-15").first();
    var top15ElemMobile = formElemMobile.find(".top-15").first();
    var contestElem = $(".contestActiveElem").first();

    if (contestElem.attr("data-contest") === "False") {
        $(".index-container").first().addClass("inActive");
        $(".btn-search").attr("disabled", "true");
        $(".phrase").attr("disabled", "true");
    } else {
        $(".index-container").first().removeClass("inActive");
    }

    // -- Methods ---------------------------------------------------

    var clearEntries = function () {
        containerElem.empty();
    };

    function appendEntries(state, entries) {
        if (entries.length === 0) {
            containerElem.addClass("hidden");
            noResultsElem.removeClass("hidden");
        } else {
            containerElem.removeClass("hidden");
            noResultsElem.addClass("hidden");
        }

        if (state.remaining <= 0) {
            // No more results
            loaderElem.hide();
            loadMoreElem.hide();
        } else {
            // More results
            loaderElem.hide();
            loadMoreElem.show();
        }

        for (var i = 0; i < entries.length; i++)
            appendSingleEntry(entries[i]);

        History.replaceState(state, $("title").text(), "?");
    }

    function appendSingleEntry(e) {
        var n = template.clone();

        n.attr("data-guid", e.Guid);
        n.find(".mail-share").first().attr("data-name", e.Name);
        n.find(".name").first().text(e.Name + ",");
        n.find(".city").first().text(e.City);
        n.find(".rank").first().text(e.Rank);
        n.find(".voteCount").first().text(e.VoteCount);
        n.find(".description").first().text(shorten(e.Description));
        n.find(".picture").first().css("background-image", "url(" + e.PictureUrl + ")").attr("href", e.RelativeUrl);
        n.find(".voteBtn").first()
            .attr("data-guid", e.Guid).attr("data-nomineeName", e.Name)
            .attr("data-nomineeUrl", e.AbsoluteUrl)
            .on("click", handleClickVote);
        n.find(".index-share-panel-ul").first().attr("id", e.Guid);
        n.find(".share-li").first().attr("data-target", "#" + e.Guid);
        n.find(".mail-share").first().attr("data-guid", e.Guid);
        n.find(".morelink").each(function(index) {
            $(this).attr("href", e.RelativeUrl);
        }); 
        n.find(".twitter-link").first()
            .attr("data-msg", e.TwitterShareMessage)
            .attr("data-url", e.AbsoluteUrl);
        applyTwitterProperties(n.find(".twitter-link").first());
        var shareButton = n.find(".share-button").first();
        shareButton.on("click", function () {
            $("#NomineeList").first().find(".index-share-panel-ul").collapse("hide");
        });
        n.hide();
        containerElem.append(n);
        n.fadeIn(500);

        validateMailForm();
        n.find(".mail-share").on("click", showShareMailModal);
        n.find(".fb-share").attr("data-url", e.AbsoluteUrl).on("click", showShareFacebookModal);;
        n.find(".twitter-link").each(function () {
            applyTwitterProperties($(this));
        });
    }


    var state = getState();
    var listing = new NomineeAjaxListing(list, state, appendEntries);

    if (state.entries.length > 0)
        appendEntries(state, state.entries);
    else
        listing.next();


    // -- Event handling --------------------------------------------

    // Load more event
    loadMoreElem.find("a").on("click", function (e) {
        e.preventDefault();
        listing.next();
    });

    // Formular submit event
    formElem.on("submit", function (e) {
        e.preventDefault();

        var state = newState();
        state.phrase = searchInputElem.val();

        clearEntries();

        listing.setState(state);
        listing.next();
    });

    // See Top 15 event
    top15Elem.on("click", function (e) {
        e.preventDefault();

        var state = newState();
        state.order = "top";
        state.limit = 15;

        clearEntries();

        listing.setState(state);
        listing.next();
    });

    // Formular submit event mobile
    formElemMobile.on("submit", function (e) {
        e.preventDefault();

        var state = newState();
        state.phrase = searchInputElemMobile.val();

        clearEntries();

        listing.setState(state);
        listing.next();
    });

    // See Top 15 event mobile
    top15ElemMobile.on("click", function (e) {
        e.preventDefault();

        var state = newState();
        state.order = "top";
        state.limit = 15;

        clearEntries();

        listing.setState(state);
        listing.next();
    });
});
