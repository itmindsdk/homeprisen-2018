﻿
function placeVote(nomineeId, done) {
    var data = { guid: nomineeId };
    $.post(PlaceVoteUrl, data).always(done);
}


function placeComment(nomineeId, comment, done) {
    var data = { guid: nomineeId, comment: comment };
    $.post("/Comment/PlaceComment", data).always(done);
}


/*
 * Invoke to take the user through the process of placing a vote.
 */
function vote(guid, nomineeName, nomineeUrl, onVoteSuccess, onCommentSuccess) {
    var modal = $('#VoteModal');
    var loading = modal.find('.loading');
    var facebookLogin = modal.find('.facebook-login');
    var facebookLoginButton = facebookLogin.find('button').first();
    var placeCommentModal = modal.find('.place-comment');
    var placeCommentForm = placeCommentModal.find('form');
    var placeCommentBread = modal.find('.place-comment-bread');
    var noCommentBtn = placeCommentForm.find('.btn-vote-no-comment').first();
    var placeFbCommentModal = modal.find('.place-fb-comment');
    var placeFbCommentBtn = placeFbCommentModal.find('.btn-place-fb-comment');
    var voteError = modal.find('.vote-error');
    var voteErrorNoActive = modal.find('.vote-error-no-active-contest');
    var nomineeNameSpan = modal.find('.nominee-name');
    nomineeNameSpan.text(nomineeName);

    var onCommentSubmitResult = function (data) {
        if (data.success === true) {
            // Comment placed successful
            if ($.isFunction(onCommentSuccess))
                onCommentSuccess(data);
        } else if (data.error === 1) {
            // Nominee not found
        } else if (data.error === 2) {
            // Contest is currently not active is not active
        } else if (data.error === 3) {
            // User is not authenticated via Facebook
        } else if (data.error === 4) {
            // User has not voted on this nominee
        } else if (data.error === 5) {
            // User has already commented on this nominee
        }

        placeCommentModal.hide();
        placeCommentBread.hide();
        // Share on Facebook?
        placeFbCommentModal.show();

    };

    var onVoteResult = function (data) {
        loading.hide();

        if (data.success === true) {
            // Vote cast successful
            onVoteSuccess(data);

            placeCommentModal.show();
            placeCommentBread.show();
        } else if (data.error === 1) {
            // Nominee not found
            voteError.show();
        } else if (data.error === 2) {
            // Contest is currently not active
            voteErrorNoActive.show();
        } else if (data.error === 3) {
            // User is not authenticated via Facebook
            facebookLogin.show();
        } else if (data.error === 4) {
            // User already voted on this nominee
            voteError.show();
        }
    };

    // Handling submitting of comments
    placeCommentForm.off('submit');
    placeCommentForm.on('submit', function (e) {
        e.preventDefault();
        var comment = $(this).find('textarea').first();

        if ($.trim(comment.val()) !== "") {
            placeComment(guid, comment.val(), onCommentSubmitResult);
        } else {
            var data = { success: true};
            onCommentSubmitResult(data);
        }
    });

    noCommentBtn.on('click', function() {
        placeCommentModal.hide();
        placeCommentBread.hide();
        placeFbCommentModal.show();
    });

    // Handle clicking "share on facebook"
    placeFbCommentBtn.on('click', function() {
        FB.ui({
            method: 'share',
            href: nomineeUrl,
        }, function (response) { });
    });

    // Handling clicking Login with Facebook
    facebookLoginButton.off('click');
    facebookLoginButton.on('click', function (e) {
        e.preventDefault();
        facebookSignIn(function () {
            facebookLogin.hide();
            loading.show();
            placeVote(guid, onVoteResult);
        });
    });

    // Display
    loading.show();
    facebookLogin.hide();
    placeCommentModal.hide();
    placeFbCommentModal.hide();
    voteError.hide();
    voteErrorNoActive.hide();
    placeCommentBread.hide();
    modal.modal("show");

    // Init
    placeVote(guid, onVoteResult);
}
