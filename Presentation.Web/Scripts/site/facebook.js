﻿
window.fbAsyncInit = function () {
    FB.init({
        //client app
        appId      : "649096998632600",
        //test app
        //appId       : "1051181944902515",
        cookie     : true,
        version    : "v2.5"
    });
};


(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, "script", "facebook-jssdk"));



/*
 * Prompt the user to sign in using Facebook.
 * When the user has signed in on Facebook, hes
 * Facebook user ID is saved in a session on the server.
 */
function facebookSignIn(onSignIn) {
    var onConnected = function () {
        var d = { accessToken: FB.getAuthResponse()["accessToken"] };
        $.post("/Facebook/Auth", d, function () {
            onSignIn();
        });
    };

    FB.getLoginStatus(function (response) {
        if (response.status === "connected")
            onConnected();
        else
            FB.login(function (response) {
                if (response.status === "connected") {
                    onConnected();
                }
            });
    });
}
