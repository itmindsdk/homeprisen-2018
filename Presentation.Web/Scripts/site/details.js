﻿
function CommentAjaxListing(e, appendEntries, nomineeId) {
    var loaderElem = e.find(".loader").first();
    var loadMoreElem = e.find(".load-more").first();

    var offset = 0;
    var limit = 5;

    var onLoadNomineesSuccess = function (data) {
        var remaining = data.total - (data.offset + limit);
        appendEntries(data.entries);

        loaderElem.hide();

        if (remaining > 0)
            loadMoreElem.show();
    };

    var onLoadFail = function () {
        loaderElem.hide();
        loadMoreElem.show();
    };

    this.next = function () {
        loadMoreElem.hide();
        loaderElem.show();

        var data = {
            nomineeId: nomineeId,
            offset: offset,
            limit: limit,
        };

        $.get(GetCommentsUrl, data)
            .done(onLoadNomineesSuccess)
            .fail(onLoadFail);

        offset += limit;
    };

    this.reset = function () {
        e.find('.container').first().empty();
        offset = 0;
    };

    loadMoreElem.on('click', this.next);

    loadMoreElem.find("a").on("click", function (e) {
        e.preventDefault();
    });
}


$(function () {

    var isActive = $('.contestActiveElemDetails').attr('data-isActive');

    if (isActive === "False") {
        $('.details-container').addClass("inActive");
    } else {
        $('.details-container').removeClass("inActive");
    }

    $(".voteBtn").on("click", function (e) {
        e.preventDefault();

        var nomineeName = $(this).attr("data-nomineeName");
        var nomineeUrl = $(this).attr("data-nomineeUrl");
        var guid = $(this).attr("data-guid");

        vote(guid, nomineeName, nomineeUrl, function (data) {
            $(".rank").text(data.rank);
            $(".voteCount").text(data.voteCount);
        }, function (data) {
            commentListing.reset();
            commentListing.next();
        });
    });

    if (isActive === "True") {
        $("[data-toggle='toggle']").click(function () {
            var selector = $(this).data("target");
            $(selector).toggleClass('in');
        });
    }

    $('.fb-share').on('click', showShareFacebookModal);

    var e = $('.comments');
    var containerElem = e.find('.container').first();
    var template = $(".comments-template").children().first();

    function appendEntries(entries) {
        for (var i = 0; i < entries.length; i++)
            appendSingleEntry(entries[i]);
    };

    function appendSingleEntry(e) {
        var n = template.clone();
        var fbPictureUrl = "http://graph.facebook.com/" + e.FacebookUserId + "/picture?type=normal&height=100&width=100";

        n.find(".nominee-details-comment-pic").attr("src", fbPictureUrl);
        n.find(".nominee-details-comment-text").html(e.Text);
        n.find(".nominee-details-comment-name-city").html("- " + e.Name);
        n.appendTo(containerElem).fadeIn(900);
    };

    var commentListing = new CommentAjaxListing(e, appendEntries, NomineeId);
    commentListing.next();
});
