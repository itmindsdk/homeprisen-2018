﻿using System;
using System.Diagnostics;
using System.Drawing;
using Core.ApplicationServices;
using Core.ApplicationServices.Poster;

namespace Playground
{
    class Program
    {
        static void Main(string[] args)
        {
            var i = Image.FromFile("Images/flowers.jpg");
            TestPdf(i);
            //TestFacebookCover(i);
        }

        private static void TestFacebookCover(Image i)
        {
            //new FacebookCover().Draw(i, "Lars Larsen").Save("Images/facebook-cover.jpg");
        }

        private static void TestPdf(Image i)
        {
            GoOnPoster.LogoImagePath = "../../../Presentation.Web/Content/Posters/logo.pdf";
            GoOnPoster.SplatImagePath = "../../../Presentation.Web/Content/Posters/splat.pdf";
            GoOnPoster.FontPath = @"..\..\..\Presentation.Web\Content\site\utility\fonts\DIN 1451 Com Engschrift\DIN1451Com-Engschrift.ttf";

            Console.WriteLine("Solid A4");
            new ForeningPosterSolidA4("Nørre Tranderslev Gymnatik og Idrætsforening", "Aarhus").Build("Images/f_Solid_A4.pdf");
            Console.WriteLine("Solid A3");
            new ForeningPosterSolidA3("Nørre Tranderslev Gymnatik og Idrætsforening", "Aarhus").Build("Images/f_Solid_A3.pdf");
            Console.WriteLine("Picture A4");
            new ForeningPosterPictureA4("Nørre Tranderslev Gymnatik og Idrætsforening", "Aarhus", i).Build("Images/f_Picture_A4.pdf");
            Console.WriteLine("Picture A3");
            new ForeningPosterPictureA3("Nørre Tranderslev Gymnatik og Idrætsforening", "Aarhus", i).Build("Images/f_Picture_A3.pdf");


            Console.WriteLine("Solid A4");
            new IldsjaelPosterSolidA4("John Johnson", "Aarhus").Build("Images/i_Solid_A4.pdf");
            Console.WriteLine("Solid A3");
            new IldsjaelPosterSolidA3("John Johnson", "Aarhus").Build("Images/i_Solid_A3.pdf");
            Console.WriteLine("Picture A4");
            new IldsjaelPosterPictureA4("John Johnson", "Aarhus", i).Build("Images/i_Picture_A4.pdf");
            Console.WriteLine("Picture A3");
            new IldsjaelPosterPictureA3("John Johnson", "Aarhus", i).Build("Images/i_Picture_A3.pdf");
        }
    }
}
