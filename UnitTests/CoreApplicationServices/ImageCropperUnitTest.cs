﻿using Xunit;
using Core.ApplicationServices;
using Core.ApplicationServices.Images;

namespace UnitTests.CoreApplicationServices
{
    public class ImageCropperUnitTest
    {
        private readonly ImageCropper Uut;

        /// <summary>
        /// Constructor for the test cases.
        /// For each test case, the class is instantiated and disposed.
        /// The basic naming of a test comprises of three main parts:
        /// [UnitOfWork_StateUnderTest_ExpectedBehavior]
        /// Followed by //Arrange, //Act and //Assert
        /// </summary>
        public ImageCropperUnitTest()
        {
            Uut = new ImageCropper();
        }

        [Theory]
        [InlineData(50, 25, 50, 25)]
        [InlineData(25, 50, 25, 50)]
        [InlineData(50, 50, 50, 50)]
        public void CropToFitFrameAspect_SameAspect_NoCropping(int iW, int iH, int fW, int fH)
        {
            var rect = Uut.GetCropArea(iW, iH, fW, fH);

            // Cropping at (0, 0)
            Assert.Equal(0, rect.X);
            Assert.Equal(0, rect.Y);

            // Image size is unchanged
            Assert.Equal(iW, rect.Width);
            Assert.Equal(iH, rect.Height);
        }

        [Fact]
        public void CropToFitFrameAspect_FrameThickerThanImage_CropYAxis()
        {
            var rect = Uut.GetCropArea(100, 100, 200, 100);

            Assert.Equal(25, rect.Y);
            Assert.Equal(50, rect.Height);
        }

        [Fact]
        public void CropToFitFrameAspect_FrameThickerThanImage_NoCropXAxis()
        {
            var rect = Uut.GetCropArea(100, 100, 200, 100);

            Assert.Equal(0, rect.X);
            Assert.Equal(100, rect.Width);
        }

        [Fact]
        public void CropToFitFrameAspect_FrameThinnerThanImage_CropXAxis()
        {
            var rect = Uut.GetCropArea(200, 100, 100, 100);

            Assert.Equal(50, rect.X);
            Assert.Equal(100, rect.Width);
        }

        [Fact]
        public void CropToFitFrameAspect_FrameThinnerThanImage_NoCropYAxis()
        {
            var rect = Uut.GetCropArea(200, 100, 100, 100);

            Assert.Equal(0, rect.Y);
            Assert.Equal(100, rect.Height);
        }
    }
}
