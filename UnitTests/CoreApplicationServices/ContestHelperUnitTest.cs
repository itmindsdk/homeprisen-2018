﻿using System;
using Xunit;
using Core.ApplicationServices;
using Core.DomainModel;
using Core.DomainServices;
using NSubstitute;

namespace UnitTests.CoreApplicationServices
{
    public class ContestHelperUnitTest
    {
        private readonly ContestHelper Uut;

        private readonly IUnitOfWork _unitOfWork;
        private readonly IGenericRepository<Contest> _contestRepo;
        private readonly IGenericRepository<Nominee> _nomineeRepo;

        /// <summary>
        /// Constructor for the test cases.
        /// For each test case, the class is instantiated and disposed.
        /// The basic naming of a test comprises of three main parts:
        /// [UnitOfWork_StateUnderTest_ExpectedBehavior]
        /// Followed by //Arrange, //Act and //Assert
        /// </summary>
        public ContestHelperUnitTest()
        {
            _unitOfWork = Substitute.For<IUnitOfWork>();
            _contestRepo = Substitute.For<IGenericRepository<Contest>>();
            _nomineeRepo = Substitute.For<IGenericRepository<Nominee>>();

            Uut = new ContestHelper(_unitOfWork, _contestRepo, _nomineeRepo);
        }

        [Fact]
        public void ContestIsActive_ContestIsDisabled_ReturnsFalse()
        {
            var contest = new Contest()
            {
                IsActive = false, // Considered inactive regardless of dates
            };

            var result = Uut.ContestIsActive(contest);

            Assert.Equal(false, result);
        }

        [Fact]
        public void ContestIsActive_BeginBeforeNowEndAfterNow_ReturnsTrue()
        {
            Uut.Now = new DateTime(2016, 1, 1);
            var contest = new Contest()
            {
                IsActive = true,
                Starts = new DateTime(2015, 1, 1),
                Ends = new DateTime(2017, 1, 1),
            };

            var result = Uut.ContestIsActive(contest);

            Assert.Equal(true, result);
        }

        [Fact]
        public void ContestIsActive_BeginBeforeNowEndBeforeNow_ReturnsFalse()
        {
            Uut.Now = new DateTime(2017, 1, 1);
            var contest = new Contest()
            {
                IsActive = true,
                Starts = new DateTime(2015, 1, 1),
                Ends = new DateTime(2016, 1, 1),
            };

            var result = Uut.ContestIsActive(contest);

            Assert.Equal(false, result);
        }

        [Fact]
        public void ContestIsActive_BeginAfterNowEndAfterNow_ReturnFalse()
        {
            Uut.Now = new DateTime(2015, 1, 1);
            var contest = new Contest()
            {
                IsActive = true,
                Starts = new DateTime(2016, 1, 1),
                Ends = new DateTime(2017, 1, 1),
            };

            var result = Uut.ContestIsActive(contest);

            Assert.Equal(false, result);
        }
    }
}
