﻿using System;
using Xunit;
using Core.ApplicationServices;
using Core.ApplicationServices.Facebook;
using Core.DomainModel;
using Core.DomainServices;
using NSubstitute;

namespace UnitTests.CoreApplicationServices
{
    public class FacebookImageFetcherUnitTest
    {
        private readonly FacebookProfileIdExtract Uut;

        /// <summary>
        /// Constructor for the test cases.
        /// For each test case, the class is instantiated and disposed.
        /// The basic naming of a test comprises of three main parts:
        /// [UnitOfWork_StateUnderTest_ExpectedBehavior]
        /// Followed by //Arrange, //Act and //Assert
        /// </summary>
        public FacebookImageFetcherUnitTest()
        {
            Uut = new FacebookProfileIdExtract();
        }

        [Fact]
        public void GetProfileIdFromUrl_ContainsValidIdParameter_ReturnsId()
        {
            var url = "https://www.facebook.com/profile.php?id=678287744&fref=pb&hc_location=friends_tab";

            var id = Uut.ExtractFromUrl(url);

            Assert.Equal("678287744", id);
        }

        [Fact]
        public void GetProfileIdFromUrl_ContainsInvalidIdParameter1_ReturnsNull()
        {
            var url = "https://www.facebook.com/profile.php?id=s678287744&fref=pb&hc_location=friends_tab";

            var id = Uut.ExtractFromUrl(url);

            Assert.Equal(null, id);
        }

        [Fact]
        public void GetProfileIdFromUrl_ContainsInvalidIdParameter2_ReturnsNull()
        {
            var url = "https://www.facebook.com/profile.php?id=678287744s&fref=pb&hc_location=friends_tab";

            var id = Uut.ExtractFromUrl(url);

            Assert.Equal(null, id);
        }

        [Fact]
        public void GetProfileIdFromUrl_ContainsInvalidIdParameter3_ReturnsNull()
        {
            var url = "https://www.facebook.com/profile.php?id=abc&fref=pb&hc_location=friends_tab";

            var id = Uut.ExtractFromUrl(url);

            Assert.Equal(null, id);
        }

        [Fact]
        public void GetProfileIdFromUrl_DoesNotContainsIdParameter_ReturnsNull()
        {
            var url = "https://www.facebook.com/profile.php?fref=pb&hc_location=friends_tab";

            var id = Uut.ExtractFromUrl(url);

            Assert.Equal(null, id);
        }

        [Fact]
        public void GetProfileIdFromUrl_ContainsEmptyIdParameter_ReturnsNull()
        {
            var url = "https://www.facebook.com/profile.php?id=&fref=pb&hc_location=friends_tab";

            var id = Uut.ExtractFromUrl(url);

            Assert.Equal(null, id);
        }

        [Fact]
        public void GetProfileIdFromSource_EmptySource_ReturnsNull()
        {
            var source = "";

            var id = Uut.ExtractFromSource(source);

            Assert.Equal(null, id);
        }

        [Fact]
        public void GetProfileIdFromSource_ContainsId_ReturnsId()
        {
            var source = @"Controller"",""dc7d82c6"",{ ""imp_id"":""0911a8ad"",""entity_id"":""100002363812616""}],[]],";

            var id = Uut.ExtractFromSource(source);

            Assert.Equal("100002363812616", id);
        }

        [Fact]
        public void GetProfileIdFromSource_DoesNotContainId_ReturnsNull()
        {
            var source = @"Controller"",""dc7d82c6"",{ ""imp_id"":""0911a8ad""}],[]],";

            var id = Uut.ExtractFromSource(source);

            Assert.Equal(null, id);
        }

        [Fact]
        public void GetProfileIdFromSource_ContainsInvalidId_ReturnsNull()
        {
            var source = @"Controller"",""dc7d82c6"",{ ""imp_id"":""0911a8ad"",""entity_id"":""1000023638asd12616""}],[]],";

            var id = Uut.ExtractFromSource(source);

            Assert.Equal(null, id);
        }
    }
}
