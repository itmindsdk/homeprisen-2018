﻿using Core.ApplicationServices.Pdf;
using Core.DomainModel;

namespace Core.ApplicationServices.Poster
{
    // TODO: Comment
    public class PosterGenerator : IPosterGenerator
    {
        private readonly IPdfThumbGenerator _thumbGenerator;
        protected readonly IFileLocator _fileLocator;

        public PosterGenerator(IFileLocator fileLocator, IPdfThumbGenerator thumbGenerator)
        {
            _fileLocator = fileLocator;
            _thumbGenerator = thumbGenerator;
        }

        // TODO: Comment
        public void GeneratePoster(Nominee nominee, GoOnPoster poster, FileVariant posterVariant, FileVariant thumbVariant)
        {
            var posterPath = _fileLocator.GetPath(nominee, posterVariant);
            var thumbPath = _fileLocator.GetPath(nominee, thumbVariant);

            // Generate poster
            poster.Build(posterPath);

            // Make thumbnail
            _thumbGenerator.GenerateThumbnail(posterPath, thumbPath);
        }
    }
}
