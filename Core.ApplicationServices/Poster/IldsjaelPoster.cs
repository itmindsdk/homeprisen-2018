﻿using System;
using System.Drawing;
using System.IO;
using MigraDoc.DocumentObjectModel;
using MigraDoc.Rendering;
using PdfSharp;
using PdfSharp.Drawing;
using Color = MigraDoc.DocumentObjectModel.Color;

namespace Core.ApplicationServices.Poster
{
    // TODO: Comment
    public class IldsjaelPoster : GoOnPoster
    {
        public IldsjaelPoster(string name, string city, double fontFactor = 1, PageSize pageSize = PageSize.A4, Image bgImage = null)
            : base(name, city, fontFactor, pageSize, bgImage)
        { }

        protected override void DrawContent(Rectangle contentRect, Rectangle bottomRect)
        {
            DrawSticker();
            DrawLogo();
            DrawBottomText(bottomRect);
            DrawMainText(contentRect);
            DrawPriceText(contentRect);
        }

        // TODO: Comment
        private void DrawSticker()
        {
            var path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SplatImagePath);

            // Add splat from a PDF document. This allows to use vector graphics
            var f = XPdfForm.FromFile(path);

            var wPct = Page.Width / 100;

            var w = f.PixelWidth * wPct * 0.025;
            var h = f.PixelHeight * wPct * 0.025;

            var x = wPct * 2;
            var y = wPct * 2;

            // PDF documents can be treated like images in the context
            // of adding them to another PDF document
            Gfx.DrawImage(f, x, y, w, h);
        }

        // TODO: Comment
        private void DrawLogo()
        {
            var path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, LogoImagePath);

            // Add splat from a PDF document. This allows to use vector graphics
            var f = XPdfForm.FromFile(path);

            var wPct = Page.Width / 100;
            var hPct = Page.Height / 100;

            var w = f.PixelWidth * wPct * 0.14;
            var h = f.PixelHeight * wPct * 0.14;

            var x = (wPct * 50) - (w / 2);
            var y = hPct * 65;

            // PDF documents can be treated like images in the context
            // of adding them to another PDF document
            Gfx.DrawImage(f, x, y, w, h);
        }

        // TODO: Comment
        private void DrawBottomText(Rectangle rectangle)
        {
            var rect = new XRect(rectangle.X, rectangle.Y, rectangle.Width, rectangle.Height);

            var t1 = "Go’on er tankstationen, der holder de lokale kørende, og hylder derfor dem, der gør det samme som os:\n";
            t1 += "Nemlig de mange lokale ildsjæle, der skaber liv i lokalsamfundet som for eksempel " + Name + ".\n";
            t1 += "Gå ind på støtdelokale.nu og hjælp " + Name + " med at vinde æren og 25.000 kroner.\n\n\n";

            var t2 = "* 1 års forbrug af brændstof udbetales som et Go’on brændstofkort, værdi 15.000 kr. Der findes en vinder ved tilfældig udtrækning blandt alle stemmer. Du kan kun stemme en gang.\n";
            t2 += "Udtrækning sker primo juni. Vinderen får direkte besked pr. mail og bliver offentliggjort på goon.nu. Præmien kan ikke ombyttes til kontanter. Ansatte hos Go’on kan ikke deltage i konkurrencen.";

            var mdoc = new Document();
            var msec = mdoc.AddSection();
            var mpara = msec.AddParagraph();
            var mrenderer = new DocumentRenderer(mdoc);

            var f1 = new MigraDoc.DocumentObjectModel.Font { Size = 12 * FontSizeFactor };
            var f2 = new MigraDoc.DocumentObjectModel.Font { Size = 7 * FontSizeFactor };

            var x = rect.Width * 0.1;
            var y = rect.Y + rect.Height * 0.25;
            var w = rect.Width * 0.8;

            mpara.Format.Alignment = ParagraphAlignment.Center;
            mpara.Format.Font.Name = FontName;
            mpara.Format.Font.Color = Colors.White;
            mpara.Format.Font.Bold = true;
            mpara.Format.LineSpacing = 12 * FontSizeFactor;
            mpara.Format.LineSpacingRule = LineSpacingRule.Exactly;

            mpara.AddFormattedText(t1, f1);
            mpara.AddFormattedText(t2, f2);

            mrenderer.PrepareDocument();
            mrenderer.RenderObject(Gfx, x, y, w, mpara);
        }

        // TODO: Comment
        private void DrawMainText(Rectangle rect)
        {
            var mdoc = new Document();
            var msec = mdoc.AddSection();
            var mpara = msec.AddParagraph();
            var mrenderer = new DocumentRenderer(mdoc);

            var f = new MigraDoc.DocumentObjectModel.Font
            {
                Color = new Color(255, 199, 211, 0),
                Bold = true
            };

            var x = rect.Width * 0.1;
            var y = rect.Height * 0.25;
            var w = rect.Width * 0.8;

            mpara.Format.Alignment = ParagraphAlignment.Center;
            mpara.Format.Font.Name = FontName;
            mpara.Format.Font.Size = 70 * FontSizeFactor;
            mpara.Format.Font.Color = Colors.White;
            mpara.Format.Font.Bold = true;
            mpara.Format.LineSpacing = 70 * FontSizeFactor;
            mpara.Format.LineSpacingRule = LineSpacingRule.Exactly;

            mpara.AddFormattedText("Stem på\n" + Name + "\nsom ", TextFormat.Bold);
            mpara.AddFormattedText("Årets Ildsjæl\n", f);
            mpara.AddFormattedText(" på støtdelokale.nu", TextFormat.Bold);

            mrenderer.PrepareDocument();
            mrenderer.RenderObject(Gfx, x, y, w, mpara);
        }

        // TODO: Comment
        private void DrawPriceText(Rectangle rect)
        {
            var mdoc = new Document();
            var msec = mdoc.AddSection();
            var mpara = msec.AddParagraph();
            var mrenderer = new DocumentRenderer(mdoc);

            var x = rect.Width * 0.05;
            var y = rect.Height * 0.03;
            var w = rect.Width * 0.9;

            mpara.Format.Alignment = ParagraphAlignment.Right;
            mpara.Format.Font.Name = FontName;
            mpara.Format.Font.Size = 24 * FontSizeFactor;
            mpara.Format.Font.Bold = true;
            mpara.Format.Font.Color = Colors.White;
            mpara.AddText("ÅRETS ILDSJÆL VINDER 25.000 KR");

            mrenderer.PrepareDocument();
            mrenderer.RenderObject(Gfx, x, y, w, mpara);
        }
    }

    // TODO: Comment
    public class IldsjaelPosterSolidA4 : IldsjaelPoster
    {
        public IldsjaelPosterSolidA4(string name, string city) : base(name, city, 1, PageSize.A4, null) { }
    }

    // TODO: Comment
    public class IldsjaelPosterSolidA3 : IldsjaelPoster
    {
        public IldsjaelPosterSolidA3(string name, string city) : base(name, city, 1.4, PageSize.A3, null) { }
    }

    // TODO: Comment
    public class IldsjaelPosterPictureA4 : IldsjaelPoster
    {
        public IldsjaelPosterPictureA4(string name, string city, Image backgroundImage) : base(name, city, 1, PageSize.A4, backgroundImage) { }
    }

    // TODO: Comment
    public class IldsjaelPosterPictureA3 : IldsjaelPoster
    {
        public IldsjaelPosterPictureA3(string name, string city, Image backgroundImage) : base(name, city, 1.4, PageSize.A3, backgroundImage) { }
    }
}
