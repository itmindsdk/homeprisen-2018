﻿using System;
using System.Drawing;
using System.IO;
using Core.ApplicationServices.Images;
using Core.ApplicationServices.Pdf;
using PdfSharp;
using PdfSharp.Drawing;
using PdfSharp.Pdf;

namespace Core.ApplicationServices.Poster
{
    /// <summary>
    /// Abstract class for generating the GoOn poster in PDF format.
    /// Implemented as GoF Template Method.
    /// </summary>
    public abstract class GoOnPoster : PdfBuilder
    {
        private IImageCropper Cropper { get; set; }
        protected PageSize Size { get; set; }
        private Image BackgroundImage { get; set; }
        protected string Name { get; set; }
        protected string City { get; set; }

        public static string LogoImagePath = "Content/Posters/HomePrisen-Logo.pdf";
        public static string SplatImagePath = "Content/Posters/splat.pdf";

        public static bool FontAdded = false;
        public static string FontPath = @"Content\site\utility\fonts\Taz_400_semiLight\Taz_400_.ttf";
        protected static string FontName = "Taz SemiLight";
        public static string FontLightPath = @"Content\site\utility\fonts\Taz_300_Light\Taz_300_.ttf";
        protected static string FontLightName = "Taz Light";
        public static string FontRegularPath = @"Content\site\utility\fonts\Taz_500_Regular\Taz_500_.ttf";
        protected static string FontRegularName = "Taz Regular";
        public static string FontBoldPath = @"Content\site\utility\fonts\Taz_700_Bold\Taz_700_.ttf";
        protected static string FontBoldName = "Taz Bold";
        protected double FontSizeFactor { get; set; } = 1;

        protected GoOnPoster(string name, string city, double fontFactor = 1, PageSize pageSize = PageSize.A4, Image bgImage = null)
        {
            Cropper = new ImageCropper();
            FontSizeFactor = fontFactor;
            Size = pageSize;
            BackgroundImage = bgImage;
            Name = name;
            City = city;
        }

        // TODO: Comment
        protected override PdfPage CreatePage()
        {
            var page = base.CreatePage();
            page.Size = Size;
            return page;
        }

        /// <summary>
        /// Populate the document with content
        /// </summary>
        protected override void BuildDocument()
        {
            var bottomHeight = (int)(Page.Height * 0.20);
            var pictureHeight = (int)(Page.Height * 0.25);
            var bottomRect = new Rectangle()
            {
                X = 0,
                Y = ((int)Page.Height - bottomHeight),
                Width = (int)Page.Width,
                Height = bottomHeight,
            };

            // Content will fill out the whole page except where the bottom is
            var contentRect = new Rectangle()
            {
                X = 0,
                Y = 0,
                Width = (int)Page.Width,
                Height = ((int)Page.Height - bottomHeight),
            };

            if (!FontAdded)
            {
                AddFont(FontName, Path.Combine(AppDomain.CurrentDomain.BaseDirectory, FontPath));
                AddFont(FontLightName, Path.Combine(AppDomain.CurrentDomain.BaseDirectory, FontLightPath));
                AddFont(FontRegularName, Path.Combine(AppDomain.CurrentDomain.BaseDirectory, FontRegularPath));
                AddFont(FontBoldName, Path.Combine(AppDomain.CurrentDomain.BaseDirectory, FontBoldPath));
                FontAdded = true;
            }

            // Draw content background
            if (BackgroundImage != null)
            {
                contentRect.Height -= pictureHeight;
                var pictureRect = new Rectangle()
                {
                    X = 0,
                    Y = ((int)Page.Height - (bottomHeight + pictureHeight)),
                    Width = (int)Page.Width,
                    Height = pictureHeight
                };
                DrawPictureBackground(pictureRect, BackgroundImage);
                DrawSolidBackground(contentRect);
            }
            else
                DrawSolidBackground(contentRect);

            // Draw bottom background
            DrawBottom(bottomRect);

            // Draw content on top of backgrounds
            DrawContent(contentRect, bottomRect);
        }

        // TODO: Comment
        protected abstract void DrawContent(Rectangle contentRect, Rectangle bottomRect);

        // TODO: Comment
        private void DrawSolidBackground(Rectangle rectangle)
        {
            var rect = new XRect(rectangle.X, rectangle.Y, rectangle.Width, rectangle.Height);
            var bgbrush = new XSolidBrush(XColor.FromArgb(255, 230, 35, 35));
            Gfx.DrawRectangle(bgbrush, rect);
        }

        // TODO: Comment
        private void DrawPictureBackground(Rectangle rect, Image image)
        {
            // Calculate cropping coordinates.
            // Cropping must result in background image having the
            // same aspect ratio as the frame it will be placed in.
            var cropArea = Cropper.GetCropArea(
                image.Width,
                image.Height,
                rect.Width,
                rect.Height);

            // Crop image if required to do so.
            if (Cropper.RequireCrop(cropArea))
                image = Cropper.Crop(image, cropArea);

            // Apply greyscale color filter on image
            image = new GreyscaleImageProcessor().ProcessImage(image);

            // Background image is now the same aspect ration as its content frame.
            // The image will be stretched to fill the frame.
            Gfx.DrawImage(ConvertImage(image), rect.X, rect.Y, rect.Width, rect.Height);
        }

        // TODO: Comment
        private void DrawBottom(Rectangle rectangle)
        {
            var rect = new XRect(rectangle.X, rectangle.Y, rectangle.Width, rectangle.Height);
            var bgbrush = new XSolidBrush(XColor.FromArgb(255, 255, 255, 255));
            Gfx.DrawRectangle(bgbrush, rect);
        }
    }
}
