﻿using System;
using System.Drawing;
using System.IO;
using MigraDoc.DocumentObjectModel;
using MigraDoc.Rendering;
using PdfSharp;
using PdfSharp.Drawing;
using Color = MigraDoc.DocumentObjectModel.Color;

namespace Core.ApplicationServices.Poster
{
    // TODO: Comment
    public class ForeningPosterPoster : GoOnPoster
    {
        public ForeningPosterPoster(string name, string city, double fontFactor = 1, PageSize pageSize = PageSize.A4, Image bgImage = null)
            : base(name, city, fontFactor, pageSize, bgImage)
        { }

        protected override void DrawContent(Rectangle contentRect, Rectangle bottomRect)
        {
            DrawLogo();
            DrawBottomText(bottomRect);
            DrawMainText(contentRect);
        }

        // TODO: Comment
        private void DrawLogo()
        {
            var path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, LogoImagePath);

            // Add splat from a PDF document. This allows to use vector graphics
            var f = XPdfForm.FromFile(path);

            var wPct = Page.Width / 100;
            var hPct = Page.Height / 100;

            var w = f.PixelWidth * wPct * 0.12;
            var h = f.PixelHeight * wPct * 0.12;

            var x = wPct * 25;
            var y = hPct * 64;

            // PDF documents can be treated like images in the context
            // of adding them to another PDF document
            Gfx.DrawImage(f, x, y, w, h);
        }

        // TODO: Comment
        private void DrawMainText(Rectangle rect)
        {
            var mdoc = new Document();
            var msec = mdoc.AddSection();
            var mpara = msec.AddParagraph();
            var mrenderer = new DocumentRenderer(mdoc);

            var x = rect.Width * 0.1;
            var y = rect.Height * 0.09;
            var w = rect.Width * 0.8;

            var relativeFontSize = rect.Height / 17;
            mpara.Format.Alignment = ParagraphAlignment.Left;
            mpara.Format.Font.Name = FontBoldName;
            mpara.Format.Font.Size = relativeFontSize * FontSizeFactor;
            mpara.Format.Font.Color = Colors.White;
            mpara.Format.Font.Bold = true;
            mpara.Format.LineSpacing = relativeFontSize * FontSizeFactor;
            mpara.Format.LineSpacingRule = LineSpacingRule.Exactly;

            if (Size == PageSize.A3)
            {
                mpara.AddFormattedText("Hjælp os og stem\n" + Name + ",\n" + City + "\npå vej til at vinde\nhomePrisen 2018\n\n", TextFormat.Bold);
                mpara.AddFormattedText("Gå ind på\nhome.dk/homeprisen", TextFormat.Bold);
            }
            else
            {
                mpara.Format.Font.Size = rect.Height / 12 * FontSizeFactor;
                mpara.Format.LineSpacing = rect.Height / 12 * FontSizeFactor;
                mpara.AddFormattedText("Hjælp\n" + Name + ",\n" + City + " på vej\ntil at vinde\nhomePrisen 2018\n\n", TextFormat.Bold);
                mpara.AddFormattedText("Gå ind på\nhome.dk/homeprisen", TextFormat.Bold);
            }

            mrenderer.PrepareDocument();
            mrenderer.RenderObject(Gfx, x, y, w, mpara);
        }

        // TODO: Comment
        private void DrawBottomText(Rectangle rectangle)
        {
            var rect = new XRect(rectangle.X, rectangle.Y, rectangle.Width, rectangle.Height);

            var text = @"Med homePrisen fejrer vi, at vores mange lokale ejendomsmæglere
                        i home oplever så meget lokal stolthed rundt omkring i det danske
                        landskab. Netop den stolthed tilfører energi og nye borgere til mindre
                        byer, og det hylder vi med tre kontante skulderklap. Førstepræmien
                        modtager 30.000 kroner, andenpladsen 20.000 kroner, mens tred-
                        ";

            var x = rect.Width * 0.05;
            var y = rect.Y + rect.Height * 0.15;
            var w = rect.Width * 0.46;

            AddText(rect, text, 10, 13, x, y, w, FontLightName, false, ParagraphAlignment.Justify);

            text = "jepladsen løber af sted med 10.000 kroner";
            
            y = rect.Y + rect.Height * 0.535;

            AddText(rect, text, 10, 13, x, y, w, FontLightName, false, ParagraphAlignment.Left);

            text = "Gå ind på";
            
            y = rect.Y + rect.Height * 0.7;

            AddText(rect, text, 20, 20, x, y, w, FontLightName, false, ParagraphAlignment.Left);

            text = "home.dk/homeprisen";

            x = rect.Width * 0.18;

            AddText(rect, text, 20, 20, x, y, w, FontBoldName, true, ParagraphAlignment.Left);
        }

        private void AddText(XRect rect, string text, int fontSize, int spacing, double x, double y, double w, string fontName, bool bold = false, ParagraphAlignment alignment = ParagraphAlignment.Left)
        {
            var mdoc = new Document();
            var msec = mdoc.AddSection();
            var mpara = msec.AddParagraph();
            var mrenderer = new DocumentRenderer(mdoc);

            mpara.Format.Alignment = alignment;
            mpara.Format.Font.Name = fontName;
            mpara.Format.Font.Color = Colors.Black;
            mpara.Format.Font.Size = fontSize * FontSizeFactor;
            mpara.Format.LineSpacing = spacing * FontSizeFactor;
            mpara.Format.Font.Bold = bold;
            mpara.Format.LineSpacingRule = LineSpacingRule.Exactly;

            mpara.AddFormattedText(text);

            mrenderer.PrepareDocument();
            mrenderer.RenderObject(Gfx, x, y, w, mpara);
        }
    }

    // TODO: Comment
    public class ForeningPosterSolidA4 : ForeningPosterPoster
    {
        public ForeningPosterSolidA4(string name, string city) : base(name, city, 1, PageSize.A4, null) { }
    }

    // TODO: Comment
    public class ForeningPosterSolidA3 : ForeningPosterPoster
    {
        public ForeningPosterSolidA3(string name, string city) : base(name, city, 1.4, PageSize.A3, null) { }
    }

    // TODO: Comment
    public class ForeningPosterPictureA4 : ForeningPosterPoster
    {
        public ForeningPosterPictureA4(string name, string city, Image backgroundImage) : base(name, city, 1, PageSize.A4, backgroundImage) { }
    }

    // TODO: Comment
    public class ForeningPosterPictureA3 : ForeningPosterPoster
    {
        public ForeningPosterPictureA3(string name, string city, Image backgroundImage) : base(name, city, 1.4, PageSize.A3, backgroundImage) { }
    }
}
