﻿using Core.DomainModel;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Core.ApplicationServices
{
    public interface IRealtorService
    {
        Task<IList<Realtor>> GetRealtorsAsync(Nominee nominee);
    }
}
