﻿using Core.DomainModel;

namespace Core.ApplicationServices
{
    public interface ISettingHelper
    {
        string GetFrontpageTextForening();
        void SetFrontpageTextForening(string text);

        string GetFrontpageTextIldsjael();
        void SetFrontpageTextIldsjael(string text);

        string GetFrontpageTitleForening();
        void SetFrontpageTitleForening(string text);

        string GetFrontpageTitleIldsjael();
        void SetFrontpageTitleIldsjael(string text);

        string GetForeningAboutText();
        void SetForeningAboutText(string text);

        string GetIldsjaelAboutText();
        void SetIldsjaelAboutText(string text);

        bool GetForeningHidden();
        void SetForeningHidden(bool val);

        bool GetIldsjaelHidden();
        void SetIldsjaelHidden(bool val);

        ContestType GetDefaultContestType();
        void SetDefaultContestType(ContestType type);
    }
}
