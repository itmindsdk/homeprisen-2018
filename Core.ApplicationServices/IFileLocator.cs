﻿using Core.DomainModel;
using System;

namespace Core.ApplicationServices
{
    public enum FileVariant
    {
        ProfilePicture,
        OriginalPicture,
        FacebookCoverPicture,
        FacebookCoverSolid,
        PosterPicture,
        PosterPictureThumb,
        FlyerPicture,
        FlyerPictureThumb,
        PosterSolid,
        PosterSolidThumb,
        FlyerSolid,
        FlyerSolidThumb,
    }

    /// <summary>
    /// Indexing upload files.
    /// </summary>
    public interface IFileLocator
    {
        string GetFolderPath(Picture picture);
        string GetFolderPath(Nominee nominee);
        string EnterFolder(Picture picture);
        string EnterFolder(Nominee nominee);
        string GetPath(Picture picture, FileVariant variant);
        string GetPath(Nominee nominee, FileVariant variant);
        string GetUrl(Picture picture, FileVariant variant);
        string GetUrl(Nominee nominee, FileVariant variant);
        bool Exists(Picture picture, FileVariant variant);
        bool Exists(Nominee nominee, FileVariant variant);
    }
}
