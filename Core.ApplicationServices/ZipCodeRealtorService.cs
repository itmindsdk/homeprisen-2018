﻿using AutoMapper;
using Core.DomainModel;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace Core.ApplicationServices
{
    public class ZipCodeRealtorService : IRealtorService
    {
        private const string BaseUri = "HomeDkPublicWebApi/api/MaeglerForretningsData/FindMaeglerIpostnummer?postnummer=";
        private readonly IMapper _mapper;

        public ZipCodeRealtorService(IMapper mapper)
        {
            _mapper = mapper;
        }

        public async Task<IList<Realtor>> GetRealtorsAsync(Nominee nominee)
        {
            var composedUri = BaseUri + nominee.Zipcode;

            using (var httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Accept.Clear();
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var baseAddress = new Uri("https://webservice.home.dk/");
                httpClient.BaseAddress = baseAddress;
                var response = await httpClient.GetAsync(composedUri).ConfigureAwait(false);
                if (response.IsSuccessStatusCode)
                {
                    var realtorPayload = await response.Content.ReadAsAsync<RealtorPayload>();
                    var realtor = _mapper.Map<Realtor>(realtorPayload);

                    var realtorList = new List<Realtor> { realtor };
                    return realtorList;
                }
                throw new HttpRequestException("Someting went wrong with GET!");
            }
        }

        public class RealtorPayload
        {
            public string Organisationsnavn { get; set; }
            public string Emailadresse { get; set; }
        }
    }
}
