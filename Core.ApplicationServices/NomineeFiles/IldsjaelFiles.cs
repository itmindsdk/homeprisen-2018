﻿using System;
using System.Drawing;
using Core.ApplicationServices.Facebook;
using Core.ApplicationServices.Images;
using Core.ApplicationServices.Poster;
using Core.DomainModel;

namespace Core.ApplicationServices.NomineeFiles
{
    public class IldsjaelFiles : NomineeFilesBase
    {
        private readonly IPosterGenerator _posterGenerator;

        public IldsjaelFiles(
            IPosterGenerator posterGenerator,
            IFileLocator fileLocator,
            IImageCropper imgCropper,
            IImageResize imgResize)
            : base(fileLocator, imgCropper, imgResize)
        {
            _posterGenerator = posterGenerator;
        }

        // TODO: Comment
        protected override void GenerateFacebookCover(Nominee nominee, Image source)
        {
            var pictureCover = new PictureFacebookCover((Image)source.Clone()).Draw(nominee.Name, "Årets Ildsjæl");
            pictureCover.Save(FileLocator.GetPath(nominee, FileVariant.FacebookCoverPicture));

            var solidCover = new SolidFacebookCover().Draw(nominee.Name, "Årets Ildsjæl");
            solidCover.Save(FileLocator.GetPath(nominee, FileVariant.FacebookCoverSolid));
        }

        // TODO: Comment
        protected override void GeneratePosters(Nominee nominee, Image backgroundImage)
        {
            _posterGenerator.GeneratePoster(
                nominee,
                new IldsjaelPosterSolidA3(nominee.Name, nominee.City),
                FileVariant.PosterSolid,
                FileVariant.PosterSolidThumb);

            _posterGenerator.GeneratePoster(
                nominee,
                new IldsjaelPosterSolidA4(nominee.Name, nominee.City),
                FileVariant.FlyerSolid,
                FileVariant.FlyerSolidThumb);

            _posterGenerator.GeneratePoster(
                nominee,
                new IldsjaelPosterPictureA3(nominee.Name, nominee.City, backgroundImage),
                FileVariant.PosterPicture,
                FileVariant.PosterPictureThumb);

            _posterGenerator.GeneratePoster(
                nominee,
                new IldsjaelPosterPictureA4(nominee.Name, nominee.City, backgroundImage),
                FileVariant.FlyerPicture,
                FileVariant.FlyerPictureThumb);
        }
    }
}
