﻿using Core.DomainModel;

namespace Core.ApplicationServices.NomineeFiles
{
    // TODO: Comment
    public interface INomineeFiles
    {
        // TODO: Comment
        void GenerateFiles(Nominee nominee);

        // TODO: Comment
        void RemoveFiles(Nominee nominee);

        // TODO: Comment
        void RemoveFiles(Picture picture);
    }
}
