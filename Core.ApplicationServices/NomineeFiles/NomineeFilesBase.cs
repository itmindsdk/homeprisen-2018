﻿using System.Drawing;
using System.IO;
using Core.ApplicationServices.Images;
using Core.DomainModel;

namespace Core.ApplicationServices.NomineeFiles
{
    // TODO: Comment
    public abstract class NomineeFilesBase : INomineeFiles
    {
        protected readonly IFileLocator FileLocator;
        protected readonly IImageCropper ImgCropper;
        protected readonly IImageResize ImgResize;

        protected NomineeFilesBase(IFileLocator fileLocator, IImageCropper imgCropper, IImageResize imgResize)
        {
            FileLocator = fileLocator;
            ImgCropper = imgCropper;
            ImgResize = imgResize;
        }

        // TODO: Comment
        public void GenerateFiles(Nominee nominee)
        {
            var sourcePath = FileLocator.GetPath(nominee, FileVariant.OriginalPicture);

            using (FileStream stream = new FileStream(sourcePath, FileMode.Open))
            {
                var source = Image.FromStream(stream);

                // Profile picture
                GenerateProfilePicture(nominee, source);

                // Facebook cover photo
                GenerateFacebookCover(nominee, source);

                // Posters + flyers
                GeneratePosters(nominee, source);
            }
        }

        // TODO: Comment
        protected abstract void GenerateFacebookCover(Nominee nominee, Image source);

        // TODO: Comment
        protected abstract void GeneratePosters(Nominee nominee, Image backgroundImage);

        // TODO: Comment
        protected virtual void GenerateProfilePicture(Nominee nominee, Image source)
        {
            var pic = (Image)source.Clone();
            var path = FileLocator.GetPath(nominee, FileVariant.ProfilePicture);

            // Frontpage and Details view image aspect = 1:1
            var area = ImgCropper.GetCropArea(source.Width, source.Height, 1, 1);

            // Crop if required
            if (ImgCropper.RequireCrop(area))
                pic = ImgCropper.Crop(source, area);

            // Resize
            pic = ImgResize.Resize(pic, 400, 400);

            // Greyscale
            pic = new GreyscaleImageProcessor().ProcessImage(pic);

            pic.Save(path);
        }

        // TODO: Comment
        public virtual void RemoveFiles(Nominee nominee)
        {
            if (nominee.Picture != null)
                RemoveFiles(nominee.Picture);
        }

        // TODO: Comment
        public virtual void RemoveFiles(Picture picture)
        {
            var path = FileLocator.GetFolderPath(picture);

            try
            {
                Directory.Delete(path, true);
            }
            catch (DirectoryNotFoundException) { }
        }
    }
}
