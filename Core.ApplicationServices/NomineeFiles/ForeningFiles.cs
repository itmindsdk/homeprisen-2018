﻿using System.Drawing;
using Core.ApplicationServices.Facebook;
using Core.ApplicationServices.Images;
using Core.ApplicationServices.Poster;
using Core.DomainModel;

namespace Core.ApplicationServices.NomineeFiles
{
    public class ForeningFiles : NomineeFilesBase
    {
        private readonly IPosterGenerator _posterGenerator;

        public ForeningFiles(
            IPosterGenerator posterGenerator,
            IFileLocator fileLocator,
            IImageCropper imgCropper,
            IImageResize imgResize)
            : base(fileLocator, imgCropper, imgResize)
        {
            _posterGenerator = posterGenerator;
        }

        // TODO: Comment
        protected override void GenerateFacebookCover(Nominee nominee, Image source)
        {
            var pictureCover = new PictureFacebookCover((Image)source.Clone()).Draw(nominee.Name, nominee.City);
            pictureCover.Save(FileLocator.GetPath(nominee, FileVariant.FacebookCoverPicture));

            var solidCover = new SolidFacebookCover().Draw(nominee.Name, nominee.City);
            solidCover.Save(FileLocator.GetPath(nominee, FileVariant.FacebookCoverSolid));
        }

        // TODO: Comment
        protected override void GeneratePosters(Nominee nominee, Image backgroundImage)
        {
            _posterGenerator.GeneratePoster(
                nominee,
                new ForeningPosterSolidA3(nominee.Name, nominee.City),
                FileVariant.PosterSolid,
                FileVariant.PosterSolidThumb);

            _posterGenerator.GeneratePoster(
                nominee,
                new ForeningPosterSolidA4(nominee.Name, nominee.City),
                FileVariant.FlyerSolid,
                FileVariant.FlyerSolidThumb);

            _posterGenerator.GeneratePoster(
                nominee,
                new ForeningPosterPictureA3(nominee.Name, nominee.City, backgroundImage),
                FileVariant.PosterPicture,
                FileVariant.PosterPictureThumb);

            _posterGenerator.GeneratePoster(
                nominee,
                new ForeningPosterPictureA4(nominee.Name, nominee.City, backgroundImage),
                FileVariant.FlyerPicture,
                FileVariant.FlyerPictureThumb);
        }
    }
}
