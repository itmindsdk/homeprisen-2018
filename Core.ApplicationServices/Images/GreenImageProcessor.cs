﻿using System;
using System.Drawing;

namespace Core.ApplicationServices.Images
{
    /// <summary>
    /// Apply a green "overlay" (color scheme) on a bitmap image.
    /// </summary>
    public class GreenImageProcessor : IImagePreprocessor
    {
        /// <summary>
        /// Copies to original source bitmap and applies
        /// green color to the image.
        /// </summary>
        /// <param name="source">Source image</param>
        /// <returns>A processed copy of the source image</returns>
        public Image ProcessImage(Image source, bool darker = false)
        {
            var bmpNew = new Bitmap(source);

            for (var x = 0; x < bmpNew.Width; x++)
            {
                for (var y = 0; y < bmpNew.Height; y++)
                {
                    var color = bmpNew.GetPixel(x, y);

                    var red = EnsureColorRange(color.R + 80);
                    var green = EnsureColorRange(color.G + 80);
                    var blue = EnsureColorRange(color.B + 80);

                    bmpNew.SetPixel(x, y, Color.FromArgb((byte)red, (byte)green, (byte)blue));
                }
            }

            return bmpNew;
        }

        private static int EnsureColorRange(int color)
        {
            color = Math.Max(color, 0);
            color = Math.Min(255, color);
            return color;
        }
    }
}
