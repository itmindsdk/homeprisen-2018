﻿using System.Drawing;

namespace Core.ApplicationServices.Images
{
    /// <summary>
    /// Interface for classes calculating image cropping coordinates
    /// </summary>
    public interface IImageCropper
    {
        /// <summary>
        /// Calculate if/where to crop an image to fit the aspect ration of a frame.
        /// </summary>
        /// <param name="imgWidth">Width of image to crop</param>
        /// <param name="imgHeight">Height of image to crop</param>
        /// <param name="frameWidth">Width of frame to fit image in</param>
        /// <param name="frameHeight">Height of frame to fit image in</param>
        /// <returns>
        /// A Rectangle object containing X, Y, Width and Height attributes.
        /// If X=0, Y=0, Width=imgWidth and Height=imgHeight then no cropping is needed.
        /// </returns>
        Rectangle GetCropArea(int imgWidth, int imgHeight, int frameWidth, int frameHeight);

        bool RequireCrop(Rectangle boundaries);

        Image Crop(Image image, Rectangle boundaries);
    }
}
