﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace Core.ApplicationServices.Images
{
    // TODO: Comment
    public class ImageRotate : IImageRotate
    {
        // TODO: Comment
        public Image RotateToExitOrientation(Image image)
        {
            int orientation;

            try
            {
                orientation = image.GetPropertyItem(274).Value[0];
            }
            catch (ArgumentException)
            {
                return image;
            }

            switch (orientation)
            {
                case 1:
                    // No rotation required.
                    break;
                case 2:
                    image.RotateFlip(RotateFlipType.RotateNoneFlipX);
                    break;
                case 3:
                    image.RotateFlip(RotateFlipType.Rotate180FlipNone);
                    break;
                case 4:
                    image.RotateFlip(RotateFlipType.Rotate180FlipX);
                    break;
                case 5:
                    image.RotateFlip(RotateFlipType.Rotate90FlipX);
                    break;
                case 6:
                    image.RotateFlip(RotateFlipType.Rotate90FlipNone);
                    break;
                case 7:
                    image.RotateFlip(RotateFlipType.Rotate270FlipX);
                    break;
                case 8:
                    image.RotateFlip(RotateFlipType.Rotate270FlipNone);
                    break;
            }

            // This EXIF data is now invalid and should be removed.
            image.RemovePropertyItem(274);

            return image;
        }

        // TODO: Comment
        public Stream RotateToExitOrientation(Stream stream)
        {
            var image = Image.FromStream(stream);
            var dest = new MemoryStream();

            RotateToExitOrientation(image);
            image.Save(dest, ImageFormat.Jpeg);
            dest.Seek(0, SeekOrigin.Begin);

            return dest;
        }
    }
}
