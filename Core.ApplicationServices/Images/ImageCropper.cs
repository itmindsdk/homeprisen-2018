﻿using System.Drawing;

namespace Core.ApplicationServices.Images
{
    /// <summary>
    /// Class for calculating image cropping coordinates
    /// </summary>
    public class ImageCropper : IImageCropper
    {
        /// <summary>
        /// Calculate if/where to crop an image to fit the aspect ration of a frame.
        /// </summary>
        /// <param name="imgWidth">Width of image to crop</param>
        /// <param name="imgHeight">Height of image to crop</param>
        /// <param name="frameWidth">Width of frame to fit image in</param>
        /// <param name="frameHeight">Height of frame to fit image in</param>
        /// <returns>
        /// A Rectangle object containing X, Y, Width and Height attributes.
        /// If X=0, Y=0, Width=imgWidth and Height=imgHeight then no cropping is needed.
        /// </returns>
        public Rectangle GetCropArea(int imgWidth, int imgHeight, int frameWidth, int frameHeight)
        {
            double picAspect = ((double)imgWidth) / imgHeight;
            double frameAspect = ((double)frameWidth) / frameHeight;

            int cropX = 0;
            int cropY = 0;
            int newWidth = imgWidth;
            int newHeight = imgHeight;

            if (picAspect < frameAspect)
            {
                // Image is narrower than the frame.
                // Crop image at the y-axis, making it lower.
                newHeight = (int)(imgWidth / frameAspect);
                cropY = (imgHeight - newHeight) / 2;
            }
            else if (picAspect > frameAspect)
            {
                // Image is wider than the frame.
                // Crop image at the x-axis, making it thinner.
                newWidth = (int)(frameAspect * imgHeight);
                cropX = (imgWidth - newWidth) / 2;
            }

            return new Rectangle()
            {
                X = cropX,
                Y = cropY,
                Width = newWidth,
                Height = newHeight,
            };
        }

        public bool RequireCrop(Rectangle boundaries)
        {
            return (boundaries.X != 0 || boundaries.Y != 0);
        }

        public Image Crop(Image image, Rectangle boundaries)
        {
            Bitmap clone;

            using (var bmp = new Bitmap(image))
            {
                clone = bmp.Clone(boundaries, image.PixelFormat);
            }

            return clone;
        }
    }
}
