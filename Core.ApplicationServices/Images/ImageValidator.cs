﻿using System;
using System.Drawing;
using System.IO;

namespace Core.ApplicationServices.Images
{
    /// <summary>
    /// Used for validating whether streams are actual images or not.
    /// </summary>
    public class ImageValidator : IImageValidator
    {
        public bool IsImage(Stream stream)
        {
            try
            {
                using (var img = Image.FromStream(stream))
                    return !img.Size.IsEmpty;
            }
            catch (ArgumentException)
            {
                return false;
            }
        }

        public Size GetSize(string path)
        {
            Size size;

            using (var fs = new FileStream(path, FileMode.Open))
                size = GetSize(fs);

            return size;
        }

        public Size GetSize(Stream stream)
        {
            Bitmap img = new Bitmap(stream);
            return new Size(img.Width, img.Height);
        }
    }
}
