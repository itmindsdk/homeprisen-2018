﻿using System.Drawing;

namespace Core.ApplicationServices.Images
{
    /// <summary>
    /// For processing images
    /// </summary>
    public interface IImagePreprocessor
    {
        /// <summary>
        /// ProcessImage image.
        /// </summary>
        /// <param name="source">Bitmap object to process</param>
        /// <returns>Modified/copied Bitmap object</returns>
        Image ProcessImage(Image source, bool darker = false);
    }
}
