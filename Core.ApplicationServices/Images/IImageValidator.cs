﻿using System.Drawing;
using System.IO;

namespace Core.ApplicationServices.Images
{
    /// <summary>
    /// Used for validating whether streams are actual images or not.
    /// </summary>
    public interface IImageValidator
    {
        bool IsImage(Stream stream);
        Size GetSize(Stream stream);
        Size GetSize(string path);
    }
}
