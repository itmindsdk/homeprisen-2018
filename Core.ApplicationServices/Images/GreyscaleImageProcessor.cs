﻿using System.Drawing;

namespace Core.ApplicationServices.Images
{
    // TODO: Comment
    public class GreyscaleImageProcessor : IImagePreprocessor
    {
        // TODO: Comment
        public Image ProcessImage(Image source, bool darker = false)
        {
            Bitmap bmpNew = new Bitmap(source);

            Color c;
            byte gray;

            for (int x = 0; x < bmpNew.Width; x++)
            {
                for (int y = 0; y < bmpNew.Height; y++)
                {
                    c = bmpNew.GetPixel(x, y);
                    gray = (byte) ((c.R + c.G + c.B) / 3);

                    if (darker)
                    {
                        gray /= 2;
                    }

                    bmpNew.SetPixel(x, y, Color.FromArgb(gray, gray, gray));
                }
            }

            return bmpNew;
        }
    }
}
