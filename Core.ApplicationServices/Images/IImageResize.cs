﻿using System.Drawing;

namespace Core.ApplicationServices.Images
{
    public interface IImageResize
    {
        Image Resize(Image image, int width, int height);
    }
}
