﻿using Core.DomainModel;
using System;
using System.IO;

namespace Core.ApplicationServices
{
    // TODO: Comment!
    public class FileLocator : IFileLocator
    {
        private readonly string _uploadDir;

        public FileLocator(string uploadDir)
        {
            _uploadDir = uploadDir;
        }

        // TODO: Comment!
        public string GetFolderPath(Picture picture)
        {
            return Path.Combine(_uploadDir, GetFolderName(picture));
        }

        // TODO: Comment!
        public string GetFolderPath(Nominee nominee)
        {
            return GetFolderPath(nominee.Picture);
        }

        // TODO: Comment!
        public string EnterFolder(Picture picture)
        {
            var path = GetFolderPath(picture);
            Directory.CreateDirectory(path);
            return path;
        }

        // TODO: Comment!
        public string EnterFolder(Nominee nominee)
        {
            return EnterFolder(nominee.Picture);
        }

        // TODO: Comment!
        public string GetPath(Picture picture, FileVariant variant)
        {
            return Path.Combine(GetFolderPath(picture), GetFileName(variant));
        }

        // TODO: Comment!
        public string GetPath(Nominee nominee, FileVariant variant)
        {
            return GetPath(nominee.Picture, variant);
        }

        // TODO: Comment!
        public string GetUrl(Picture picture, FileVariant variant)
        {
            // TODO: Get name of subdir from settings
            return string.Format("/Upload/{0}/{1}", GetFolderName(picture), GetFileName(variant));
        }

        // TODO: Comment!
        public string GetUrl(Nominee nominee, FileVariant variant)
        {
            return GetUrl(nominee.Picture, variant);
        }

        // TODO: Comment!
        public bool Exists(Picture picture, FileVariant variant)
        {
            return File.Exists(GetPath(picture, variant));
        }

        // TODO: Comment!
        public bool Exists(Nominee nominee, FileVariant variant)
        {
            if (nominee.Picture != null)
                return Exists(nominee.Picture, variant);
            else
                return false;
        }

        // TODO: Comment!
        private string GetFileName(FileVariant variant)
        {
            switch (variant)
            {
                case FileVariant.ProfilePicture:
                    return "profile.jpg";
                case FileVariant.OriginalPicture:
                    return "original.jpg";
                case FileVariant.FacebookCoverPicture:
                    return "facebook-cover-picture.jpg";
                case FileVariant.FacebookCoverSolid:
                    return "facebook-cover-solid.jpg";
                case FileVariant.PosterPicture:
                    return "poster-picture.pdf";
                case FileVariant.PosterPictureThumb:
                    return "poster-picture-thumb.jpg";
                case FileVariant.FlyerPicture:
                    return "flyer-picture.pdf";
                case FileVariant.FlyerPictureThumb:
                    return "flyer-picture-thumb.jpg";
                case FileVariant.PosterSolid:
                    return "poster-solid.pdf";
                case FileVariant.PosterSolidThumb:
                    return "poster-solid-thumb.jpg";
                case FileVariant.FlyerSolid:
                    return "flyer-solid.pdf";
                case FileVariant.FlyerSolidThumb:
                    return "flyer-solid-thumb.jpg";
            }

            return "unknown";
        }

        // TODO: Comment!
        private string GetFolderName(Picture picture)
        {
            return picture.Guid.ToString();
        }

        // TODO: Comment!
        private string GetFolderName(Nominee nominee)
        {
            return GetFolderName(nominee.Picture);
        }
    }
}
