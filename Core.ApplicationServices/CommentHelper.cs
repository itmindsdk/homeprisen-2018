﻿using System.Linq;
using Core.ApplicationServices.Facebook;
using Core.DomainModel;
using Core.DomainServices;

namespace Core.ApplicationServices
{
    // TODO: Comment
    public class CommentHelper : ICommentHelper
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IGenericRepository<Comment> _commentRepo;

        public CommentHelper(IUnitOfWork unitOfWork, IGenericRepository<Comment> commentRepo)
        {
            _unitOfWork = unitOfWork;
            _commentRepo = commentRepo;
        }

        // TODO: Comment
        public bool HasCommentedOn(string facebookUserId, Nominee nominee)
        {
            return _commentRepo.AsQueryable()
                .Any(c => c.FacebookUserId == facebookUserId
                          && c.Nominee.Guid == nominee.Guid);
        }

        // TODO: Comment
        public void PlaceComment(Nominee nominee, IFacebookAuthHelper fbAuth, string text)
        {
            var comment = new Comment()
            {
                Nominee = nominee,
                FacebookUserId = fbAuth.UserId,
                Name = fbAuth.FullName,
                City = "", // TODO: How to get this?!
                Text = text,
            };

            _commentRepo.Insert(comment);
            _unitOfWork.Save();
        }
    }
}
