﻿using System.Drawing;
using System.IO;
using Core.ApplicationServices.Images;
using GhostscriptSharp;
using GhostscriptSharp.Settings;

namespace Core.ApplicationServices.Pdf
{
    /// <summary>
    /// Interface for converting a PDF file to an image
    /// </summary>
    public class PdfThumbGenerator : IPdfThumbGenerator
    {
        private IImageResize Resizer { get; set; }

        public PdfThumbGenerator(IImageResize resizer)
        {
            Resizer = resizer;
        }

        /// <summary>
        /// Convert a PDF file to an PNG image
        /// </summary>
        /// <param name="source">Path to PDF file</param>
        /// <param name="dest">Path to write image to</param>
        public void GenerateThumbnail(string source, string dest)
        {
            // Use GhostscriptSharp to convert the pdf to a png
            GhostscriptWrapper.GenerateOutput(source, dest,
                new GhostscriptSettings
                {
                    Device = GhostscriptDevices.pngalpha,
                    Page = new GhostscriptPages
                    {
                        // Only make a thumbnail of the first page
                        Start = 1,
                        End = 1,
                        AllPages = false
                    },
                    Resolution = new Size
                    {
                        // Render at 72x72 dpi
                        Height = 72,
                        Width = 72
                    },
                    Size = new GhostscriptPageSize
                    {
                        // The dimentions of the incoming PDF must be
                        // specified. The example PDF is US Letter sized.
                        Native = GhostscriptPageSizes.letter
                    }
                }
            );

            // Resize to thumbnail size and overwrite (220 x 310 px)
            Image img;
            using (var fs = new FileStream(dest, FileMode.Open))
            {
                img = Image.FromStream(fs);
            }

            img = Resizer.Resize(img, 220, 310);
            img.Save(dest);
        }
    }
}
