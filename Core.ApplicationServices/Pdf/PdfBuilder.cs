﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using PdfSharp.Drawing;
using PdfSharp.Pdf;

namespace Core.ApplicationServices.Pdf
{
    /// <summary>
    /// Base class for PDF builders.
    /// Implemented as GoF Template Method.
    /// Currently only support single-page PDF files.
    /// </summary>
    public abstract class PdfBuilder : IPdfBuilder
    {
        protected PdfDocument Doc;
        protected PdfPage Page;
        protected XGraphics Gfx;

        /// <summary>
        /// Build the document
        /// </summary>
        /// <param name="dest">Path of file to write to</param>
        public void Build(string dest)
        {
            Doc = CreateDocument();
            Page = CreatePage();
            Gfx = CreateGraphics();

            BuildDocument();

            Doc.Save(dest);
        }

        /// <summary>
        /// Create a new document
        /// </summary>
        /// <returns>New document</returns>
        protected virtual PdfDocument CreateDocument()
        {
            return new PdfDocument();
        }

        /// <summary>
        /// Create a new page
        /// </summary>
        /// <returns>New page</returns>
        protected virtual PdfPage CreatePage()
        {
            return Doc.AddPage();
        }

        /// <summary>
        /// Create a new graphics object
        /// </summary>
        /// <returns>New graphics object</returns>
        protected virtual XGraphics CreateGraphics()
        {
            var gfx = XGraphics.FromPdfPage(Page);
            gfx.MUH = PdfFontEncoding.Unicode;
            gfx.MFEH = PdfFontEmbedding.Always;

            return gfx;
        }

        /// <summary>
        /// Populate PDF document with content.
        /// Subclass and overwrite me!
        /// </summary>
        protected abstract void BuildDocument();


        protected static void AddFont(string name, string path)
        {
            var fonts = XPrivateFontCollection.Global;
            var uri = new Uri(path, UriKind.Absolute);
            fonts.Add(uri, "./#" + name);
        }

        /// <summary>
        /// This is a consequence of SharpPdf being unable create XImage objects
        /// from memory. Given an Image object, write it to the drive and load it
        /// back in to memory.
        /// </summary>
        /// <param name="image">Image source</param>
        /// <returns>XImage object</returns>
        protected XImage ConvertImage(Image image)
        {
            var path = Path.GetTempFileName();

            using (var fs = new FileStream(path, FileMode.OpenOrCreate))
            {
                image.Save(fs, ImageFormat.Bmp);
            }

            return XImage.FromFile(path);
        }
    }
}
