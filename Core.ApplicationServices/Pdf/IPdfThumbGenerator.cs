﻿
namespace Core.ApplicationServices.Pdf
{
    /// <summary>
    /// Interface for converting a PDF file to an image
    /// </summary>
    public interface IPdfThumbGenerator
    {
        /// <summary>
        /// Convert a PDF file to an JPEG image
        /// </summary>
        /// <param name="source">Path to PDF file</param>
        /// <param name="dest">Path to write image to</param>
        void GenerateThumbnail(string source, string dest);
    }
}
