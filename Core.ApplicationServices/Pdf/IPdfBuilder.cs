﻿
namespace Core.ApplicationServices.Pdf
{
    /// <summary>
    /// An interface for building PDF files and saving them to disc
    /// </summary>
    public interface IPdfBuilder
    {
        /// <summary>
        /// Build the document
        /// </summary>
        /// <param name="dest">Path of file to write to</param>
        void Build(string dest);
    }
}
