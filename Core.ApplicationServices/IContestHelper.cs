﻿using System;
using Core.DomainModel;

namespace Core.ApplicationServices
{
    /// <summary>
    /// Helper methods for working with contests
    /// </summary>
    public interface IContestHelper
    {
        /// <summary>
        /// Get the active contest for a given type
        /// </summary>
        /// <param name="type">Type of contest</param>
        /// <returns>Contest, or null if no active contests exists for this type</returns>
        Contest GetActiveContest(ContestType type);

        // TODO: Comment
        Contest GetCurrentContest(ContestType type);

        /// <summary>
        /// Check whether a contest has a nomination with a given e-mail.
        /// Can be used to ensure unique e-mails within contests.
        /// </summary>
        /// <param name="contest">The contest to check in</param>
        /// <param name="email">The e-mail to check for</param>
        /// <returns>True if e-mail is nominated in contest, false otherwise</returns>
        bool HasNominatedEmail(Contest contest, string email);

        /// <summary>
        /// Check whether a contest has a nomination with a given e-mail.
        /// Can be used to ensure unique e-mails within contests.
        /// </summary>
        /// <param name="contest">The contest to check in</param>
        /// <param name="email">The e-mail to check for</param>
        /// <param name="exclude">Exclude this user from consideration</param>
        /// <returns>True if e-mail is nominated in contest, false otherwise</returns>
        bool HasNominatedEmail(Contest contest, string email, Nominee exclude);

        /// <summary>
        /// Check whether a contest is active or not
        /// </summary>
        /// <param name="contest">Contest to check status for</param>
        /// <returns>True if contest is active, false otherwise</returns>
        bool ContestIsActive(Contest contest);

        /// <summary>
        /// Check whether a contest is active within a given period.
        /// Can be used to ensure only a single active contest at a time
        /// for a given type.
        /// </summary>
        /// <param name="type">Type of contest</param>
        /// <param name="start">Date from</param>
        /// <param name="end">Date to</param>
        /// <param name="exclude">Contest to exclude</param>
        /// <returns>True if a contest with the given type exists for the given time period</returns>
        bool HasContestInPeriod(ContestType type, DateTime start, DateTime end, Contest exclude = null);

        /// <summary>
        /// Maintains the ranks of all accepted nominees within a contest.
        /// </summary>
        /// <param name="contest">Contest to maintain ranks within</param>
        void UpdateRanks(Contest contest);

        /// <summary>
        /// Counts the number of nominees in a given contest, based on the contest ID
        /// </summary>
        /// <param name="contestId"></param>
        /// <returns>Number of nominees in a contest</returns>
        int NumberOfNominees(int contestId);
    }
}
