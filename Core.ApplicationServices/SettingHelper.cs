﻿using System;
using Core.DomainModel;
using Core.DomainServices;
using System.Linq;

namespace Core.ApplicationServices
{
    public class SettingHelper : ISettingHelper
    {
        private IUnitOfWork _unitOfWork { get; set; }
        private IGenericRepository<Setting> _settingsRepo { get; set; }

        private const string FrontpageTextForeningKey = "FrontpageTextForeningKey";
        private const string FrontpageTextIldsjaelKey = "FrontpageTextIldsjaelKey";
        private const string FrontpageTitleForeningKey = "FrontpageTitleForeningKey";
        private const string FrontpageTitleIldsjaelKey = "FrontpageTitleIldsjaelKey";
        private const string ForeningAboutTextKey = "ForeningAboutText";
        private const string IldsjaelAboutTextKey = "IldsjaelAboutText";
        private const string DefaultContestTypeKey = "DefaultContestType";
        private const string ForeningHiddenKey = "ForeningHiddenKey";
        private const string IldsjaelHiddenKey = "IldsjaelHiddenKey";

        public SettingHelper(IUnitOfWork unitOfWork, IGenericRepository<Setting> settingsRepo)
        {
            _unitOfWork = unitOfWork;
            _settingsRepo = settingsRepo;
        }

        private string GetTextSetting(string key, string defaultValue)
        {
            var s = _settingsRepo.AsQueryable()
                .OfType<TextSetting>()
                .Where(x => x.Key == key)
                .FirstOrDefault();

            return (s != null) ? s.Value : defaultValue;
        }

        private void SetTextSetting(string key, string text)
        {
            var s = _settingsRepo.AsQueryable()
                .OfType<TextSetting>()
                .Where(x => x.Key == key)
                .FirstOrDefault();

            if (s != null)
            {
                s.Value = text;
                _settingsRepo.Update(s);
            }
            else
            {
                _settingsRepo.Insert(new TextSetting()
                {
                    Key = key,
                    Value = text,
                });
            }

            _unitOfWork.Save();
        }

        private bool GetBoolSetting(string key, bool defaultValue)
        {
            var s = _settingsRepo.AsQueryable()
                .OfType<BoolSetting>()
                .Where(x => x.Key == key)
                .FirstOrDefault();

            return (s != null) ? s.State : defaultValue;
        }

        private void SetBoolSetting(string key, bool val)
        {
            var s = _settingsRepo.AsQueryable()
                .OfType<BoolSetting>()
                .Where(x => x.Key == key)
                .FirstOrDefault();

            if (s != null)
            {
                s.State = val;
                _settingsRepo.Update(s);
            }
            else
            {
                _settingsRepo.Insert(new BoolSetting()
                {
                    Key = key,
                    State = val,
                });
            }

            _unitOfWork.Save();
        }

        public string GetForeningAboutText()
        {
            return GetTextSetting(ForeningAboutTextKey, "");
        }

        public void SetForeningAboutText(string text)
        {
            SetTextSetting(ForeningAboutTextKey, text);
        }

        public string GetIldsjaelAboutText()
        {
            return GetTextSetting(IldsjaelAboutTextKey, "");
        }

        public void SetIldsjaelAboutText(string text)
        {
            SetTextSetting(IldsjaelAboutTextKey, text);
        }

        public string GetFrontpageTextForening()
        {
            return GetTextSetting(FrontpageTextForeningKey, "homePrisen handler om at hylde den lokale stolthed over hele landet. For andet år i træk uddeler vi pengepræmier til tre projekter, der skaber lokal stolthed, eller på anden vis inspirerer og engagerer nærmiljø.");
        }
        public string GetFrontpageTextIldsjael()
        {
            return GetTextSetting(FrontpageTextIldsjaelKey, "");
        }
        public string GetFrontpageTitleForening()
        {
            return GetTextSetting(FrontpageTitleForeningKey, "homePrisen 2018");
        }
        public string GetFrontpageTitleIldsjael()
        {
            return GetTextSetting(FrontpageTitleIldsjaelKey, "");
        }
        public void SetFrontpageTextForening(string text)
        {
            SetTextSetting(FrontpageTextForeningKey, text);
        }
        public void SetFrontpageTextIldsjael(string text)
        {
            SetTextSetting(FrontpageTextIldsjaelKey, text);
        }
        public void SetFrontpageTitleIldsjael(string text)
        {
            SetTextSetting(FrontpageTitleIldsjaelKey, text);
        }
        public void SetFrontpageTitleForening(string text)
        {
            SetTextSetting(FrontpageTitleForeningKey, text);
        }
        public bool GetForeningHidden()
        {
            return GetBoolSetting(ForeningHiddenKey, false);
        }
        public bool GetIldsjaelHidden()
        {
            return GetBoolSetting(IldsjaelHiddenKey, false);
        }
        public void SetForeningHidden(bool value)
        {
            SetBoolSetting(ForeningHiddenKey,value);
        }
        public void SetIldsjaelHidden(bool value)
        {
            SetBoolSetting(IldsjaelHiddenKey, value);
        }
        public ContestType GetDefaultContestType()
        {
            var s = _settingsRepo.AsQueryable()
                .OfType<ContestTypeSeting>()
                .Where(x => x.Key == DefaultContestTypeKey)
                .FirstOrDefault();

            return (s != null) ? s.ContestType : ContestType.Ildsjael;
        }

        public void SetDefaultContestType(ContestType type)
        {
            var s = _settingsRepo.AsQueryable()
                .OfType<ContestTypeSeting>()
                .Where(x => x.Key == DefaultContestTypeKey)
                .FirstOrDefault();

            if (s != null)
            {
                s.ContestType = type;
                _settingsRepo.Update(s);
            }
            else
            {
                _settingsRepo.Insert(new ContestTypeSeting()
                {
                    Key = DefaultContestTypeKey,
                    ContestType = type,
                });
            }

            _unitOfWork.Save();
        }
    }
}
