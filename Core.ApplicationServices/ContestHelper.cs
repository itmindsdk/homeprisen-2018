﻿using System;
using System.Linq;
using Core.DomainModel;
using Core.DomainServices;

namespace Core.ApplicationServices
{
    /// <summary>
    /// Helper methods for working with contests
    /// </summary>
    public class ContestHelper : IContestHelper
    {
        public DateTime Now = DateTime.Now; // For unit-testing purposes

        private readonly IUnitOfWork _unitOfWork;
        private readonly IGenericRepository<Contest> _contestRepo;
        private readonly IGenericRepository<Nominee> _nomineeRepo;

        public ContestHelper(IUnitOfWork unitOfWork, IGenericRepository<Contest> contestRepo, IGenericRepository<Nominee> nomineeRepo)
        {
            _unitOfWork = unitOfWork;
            _contestRepo = contestRepo;
            _nomineeRepo = nomineeRepo;
        }

        /// <summary>
        /// Get the active contest for a given type
        /// </summary>
        /// <param name="type">Type of contest</param>
        /// <returns>Contest, or null if no active contests exists for this type</returns>
        public Contest GetActiveContest(ContestType type)
        {
            var contest = GetCurrentContest(type);

            if (contest != null && ContestIsActive(contest))
                return contest;

            return null;
        }

        // TODO: Comment
        public Contest GetCurrentContest(ContestType type)
        {
            var query = _contestRepo.AsQueryable()
                .Where(c => c.Type == type && c.Starts <= Now)
                .OrderByDescending(c => c.Ends);

            return query.FirstOrDefault();
        }

        /// <summary>
        /// Check whether a contest has a nomination with a given e-mail.
        /// Can be used to ensure unique e-mails within contests.
        /// </summary>
        /// <param name="contest">The contest to check in</param>
        /// <param name="email">The e-mail to check for</param>
        /// <returns>True if e-mail is nominated in contest, false otherwise</returns>
        public bool HasNominatedEmail(Contest contest, string email)
        {
            return _nomineeRepo.AsQueryable()
                .Any(n => n.ContestId == contest.Id &&
                          n.Email.ToLower() == email.ToLower());
        }

        /// <summary>
        /// Check whether a contest has a nomination with a given e-mail.
        /// Can be used to ensure unique e-mails within contests.
        /// </summary>
        /// <param name="contest">The contest to check in</param>
        /// <param name="email">The e-mail to check for</param>
        /// <param name="exclude">Exclude this user from consideration</param>
        /// <returns>True if e-mail is nominated in contest, false otherwise</returns>
        public bool HasNominatedEmail(Contest contest, string email, Nominee exclude)
        {
            return _nomineeRepo.AsQueryable()
                .Any(n => n.ContestId == contest.Id &&
                          n.Email == email.ToLower() &&
                          n.Guid != exclude.Guid);
        }

        /// <summary>
        /// Check whether a contest is active or not
        /// </summary>
        /// <param name="contest">Contest to check status for</param>
        /// <returns>True if contest is active, false otherwise</returns>
        public bool ContestIsActive(Contest contest)
        {
            return (contest.IsActive && contest.Starts <= Now && contest.Ends >= Now);
        }

        /// <summary>
        /// Check whether a contest is active within a given period.
        /// Can be used to ensure only a single active contest at a time
        /// for a given type.
        /// </summary>
        /// <param name="type">Type of contest</param>
        /// <param name="from">Date from</param>
        /// <param name="to">Date to</param>
        /// <returns>True if a contest with the given type exists for the given time period</returns>
        public bool HasContestInPeriod(ContestType type, DateTime start, DateTime end, Contest exclude = null)
        {
            if (exclude != null)
                return _contestRepo.AsQueryable()
                    .Any(n => n.Type == type &&
                              n.Id != exclude.Id &&
                              (start <= n.Ends && n.Starts <= end));
            else
                return _contestRepo.AsQueryable()
                    .Any(n => n.Type == type && (start <= n.Ends && n.Starts <= end));
        }

        /// <summary>
        /// Maintains the ranks of all accepted nominees within a contest.
        /// </summary>
        /// <param name="contest">Contest to maintain ranks within</param>
        public void UpdateRanks(Contest contest)
        {
            var query = from n in _nomineeRepo.AsQueryable()
                        where (n.IsAccepted && n.ContestId == contest.Id)
                        group n by n.VoteCount into grp
                        orderby grp.Key ascending
                        select grp;

            // Query is grouped by amount of votes. Therefore Count() returns
            // amount of different values for VoteCount. The maximum possible
            // rank is awarded to the nominees with the least amount of votes
            // (most votes = lowest value of Rank).
            var rank = query.Count();

            // Each group contains nominees with the same amount of votes.
            // Therefore they have the same rank.
            foreach (var grp in query)
            {
                foreach (var n in grp)
                    if (n.Rank != rank)
                        n.Rank = rank;

                rank--;
            }

            _unitOfWork.Save();
        }

        /// <summary>
        /// Counts the number of nominees in a given contest, based on the contest ID
        /// </summary>
        /// <param name="contestId"></param>
        /// <returns>Number of nominees in a contest</returns>
        public int NumberOfNominees(int contestId)
        {
            return _nomineeRepo.AsQueryable().Count(n => n.ContestId == contestId && n.IsAccepted);
        }
    }
}
