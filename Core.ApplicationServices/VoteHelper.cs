﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.ApplicationServices.Facebook;
using Core.DomainModel;
using Core.DomainServices;

namespace Core.ApplicationServices
{
    public class VoteHelper : IVoteHelper
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IGenericRepository<Vote> _voteRepo;
        private readonly IGenericRepository<Nominee> _nomineeRepo;

        public VoteHelper(
            IUnitOfWork unitOfWork,
            IGenericRepository<Vote> voteRepo,
            IGenericRepository<Nominee> nomineeRepo)
        {
            _unitOfWork = unitOfWork;
            _voteRepo = voteRepo;
            _nomineeRepo = nomineeRepo;
        }

        public bool HasVotedOn(string facebookUserId, Nominee nominee)
        {
            return _voteRepo.AsQueryable()
                   .Any(v => v.FacebookUserId == facebookUserId
                             && v.Nominee.Guid == nominee.Guid);
        }

        public void PlaceVote(Nominee nominee, IFacebookAuthHelper fbAuth)
        {
            var vote = new Vote() {
                FacebookUserId = fbAuth.UserId,
                Nominee = nominee
            };

            _voteRepo.Insert(vote);
            _unitOfWork.Save();

            nominee.VoteCount = CountVotes(nominee);
            _nomineeRepo.Update(nominee);
            _unitOfWork.Save();
        }

        public int CountVotes(Nominee nominee)
        {
            return _voteRepo.AsQueryable().Count(x => x.NomineeGuid == nominee.Guid);
        }
    }
}
