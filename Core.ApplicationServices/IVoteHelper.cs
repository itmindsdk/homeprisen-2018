﻿using Core.ApplicationServices.Facebook;
using Core.DomainModel;

namespace Core.ApplicationServices
{
    // TODO: Comment
    public interface IVoteHelper
    {
        /// <summary>
        /// Check whether a user has votes on a nominee
        /// </summary>
        /// <param name="facebookUserId">ID of user</param>
        /// <param name="nominee">Nominee to check for votes on</param>
        /// <returns>True if user with facebookUserId has votes on nominee, False otherwise</returns>
        bool HasVotedOn(string facebookUserId, Nominee nominee);

        // TODO: Comment
        void PlaceVote(Nominee nominee, IFacebookAuthHelper fbAuth);

        // TODO: Comment
        int CountVotes(Nominee nominee);
    }
}
