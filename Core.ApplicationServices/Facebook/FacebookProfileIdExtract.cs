﻿using System;
using System.Text.RegularExpressions;
using System.Web;

namespace Core.ApplicationServices.Facebook
{
    public class FacebookProfileIdExtract : IFacebookProfileIdExtract
    {
        private const string ProfilePictureUrl = "https://graph.facebook.com/{0}/picture?width=1000";
        private const string EntityIdPattern = @"""entity_id""\s*:\s*""([0-9]+)""";

        public string ExtractFromUrl(string profileUrl)
        {
            Uri uri;

            try
            {
                uri = new Uri(profileUrl);
            }
            catch (UriFormatException e)
            {
                return null;
            }

            var query = HttpUtility.ParseQueryString(uri.Query);

            if (query["id"] != null)
            {
                int id;
                var res = int.TryParse(query["id"], out id);

                if (res)
                    return id.ToString();
            }

            return null;
        }

        public string ExtractFromSource(string source)
        {
            var r = new Regex(EntityIdPattern, RegexOptions.IgnoreCase);
            var m = r.Match(source);

            return (m.Success) ? m.Groups[1].Value : null;
        }

        public string CreateProfilePictureUrl(string profileId)
        {
            return string.Format(ProfilePictureUrl, profileId);
        }
    }
}
