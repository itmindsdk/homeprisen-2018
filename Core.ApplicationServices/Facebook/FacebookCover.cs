﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.IO;
using Core.ApplicationServices.Images;

namespace Core.ApplicationServices.Facebook
{
    // TODO: Comment
    public abstract class FacebookCover : IFacebookCover
    {
        public static int Width = 851;
        public static int Height = 315;

        public static string FontName = "Taz Bold";
        public static string FontPath = @"Content\site\utility\fonts\Taz_700_Bold\Taz_700_.ttf";

        public Image Draw(string name, string title)
        {
            Image result = new Bitmap(Width, Height);

            // Draw background on image
            DrawBackground(result);

            // Draw logo on image
            DrawLogo(result);

            // Write text
            WriteText(result, name, title);

            return result;
        }

        // TODO: Comment
        protected abstract void DrawBackground(Image i);

        // TODO: Comment
        private void WriteText(Image i, string name, string title)
        {
            var fonts = new PrivateFontCollection();
            fonts.AddFontFile(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, FontPath));

            using (Graphics g = Graphics.FromImage(i))
            {
                g.SmoothingMode = SmoothingMode.AntiAlias;
                g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                g.TextRenderingHint = TextRenderingHint.AntiAliasGridFit;

                using (Font f = new Font(fonts.Families[0], 30))
                {
                    g.DrawString(name + ", " + title, f, Brushes.White, new Point(810, 40), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                    g.DrawString("Som vinder af homePrisen 2018", f, Brushes.White, new Point(810, 80), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                    g.DrawString("Gå ind på: home.dk/homeprisen", f, new SolidBrush(Color.FromArgb(255, 255, 255, 255)), new Point(810, 160), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                }
            }
        }

        protected void DrawLogo(Image sourceImage)
        {
            var logoPath = @"Content\Images\home\Home-logo-hvid.png";
            var combinedPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, logoPath);
            var homeLogoImage = Image.FromFile(combinedPath);

            // Draw logo onto source image
            using (var gfx = Graphics.FromImage(sourceImage))
            {
                gfx.DrawImage(homeLogoImage, 605, 215, homeLogoImage.Width, homeLogoImage.Height);
            }
        }
    }

    // TODO: Comment
    public class PictureFacebookCover : FacebookCover
    {
        private IImagePreprocessor PreProcessor { get; set; }
        private Image Picture { get; set; }

        // TODO: Comment
        public PictureFacebookCover(Image picture)
        {
            PreProcessor = new GreyscaleImageProcessor();
            Picture = picture;
        }

        // TODO: Comment
        protected override void DrawBackground(Image i)
        {
            var pic = Picture;
            var cropper = new ImageCropper();
            var resizer = new ImageResize();

            // Crop to fit aspect ratio
            var cropArea = cropper.GetCropArea(pic.Width, pic.Height, i.Width, i.Height);
            if (cropper.RequireCrop(cropArea))
                pic = cropper.Crop(pic, cropArea);

            // Resize to exact size
            pic = resizer.Resize(pic, Width, Height);

            // Apply filters
            pic = PreProcessor.ProcessImage(pic, true);

            // Draw processed picture onto source image
            using (var gfx = Graphics.FromImage(i))
            {
                gfx.DrawImage(pic, 0, 0, pic.Width, pic.Height);
            }
        }
    }

    // TODO: Comment
    public class SolidFacebookCover : FacebookCover
    {
        // TODO: Comment
        protected override void DrawBackground(Image i)
        {
            var color = Color.FromArgb(255, 208, 2, 0);
            var brush = new SolidBrush(color);
            var rect = new Rectangle(0, 0, i.Width, i.Height);

            using (var gfx = Graphics.FromImage(i))
            {
                gfx.FillRectangle(brush, rect);
            }
        }
    }
}
