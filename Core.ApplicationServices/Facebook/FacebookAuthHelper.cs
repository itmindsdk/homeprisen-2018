﻿using System.Web.SessionState;

namespace Core.ApplicationServices.Facebook
{
    /// <summary>
    /// Base class for implementing FileLocators.
    /// </summary>
    public class FacebookAuthHelper : IFacebookAuthHelper
    {
        private HttpSessionState _session;

        public string AccessToken
        {
            get { return (string)(_session["FacebookAccessToken"]); }
        }

        public string FullName
        {
            get { return (string)(_session["FacebookFullName"]); }
        }

        public string UserId
        {
            get { return (string)(_session["FacebookUserId"]); }
        }

        public FacebookAuthHelper(HttpSessionState session)
        {
            _session = session;
        }

        public void Register(string accessToken, string fullName, string userId)
        {
            _session["FacebookAccessToken"] = accessToken;
            _session["FacebookFullName"] = fullName;
            _session["FacebookUserId"] = userId;
        }

        public bool IsRegistered()
        {
            return (!string.IsNullOrWhiteSpace(UserId));
        }
    }
}
