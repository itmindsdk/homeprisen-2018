﻿namespace Core.ApplicationServices.Facebook
{
    public interface IFacebookAuthHelper
    {
        string AccessToken { get; }
        string FullName { get; }
        string UserId { get; }

        void Register(string authToken, string fullName, string userId);
        bool IsRegistered();
    }
}
