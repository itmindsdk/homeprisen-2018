﻿
namespace Core.ApplicationServices.Facebook
{
    public interface IFacebookProfileIdExtract
    {
        string ExtractFromUrl(string url);
        string ExtractFromSource(string source);
        string CreateProfilePictureUrl(string profileId);
    }
}
