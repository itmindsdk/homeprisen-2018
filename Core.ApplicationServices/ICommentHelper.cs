﻿using Core.ApplicationServices.Facebook;
using Core.DomainModel;

namespace Core.ApplicationServices
{
    // TODO: Comment
    public interface ICommentHelper
    {
        /// <summary>
        /// Check whether a user has placed a comment on a nominee
        /// </summary>
        /// <param name="facebookUserId">ID of user</param>
        /// <param name="nominee">Nominee to check for comments on</param>
        /// <returns>True if user with facebookUserId has commented on nominee, False otherwise</returns>
        bool HasCommentedOn(string facebookUserId, Nominee nominee);

        // TODO: Comment
        void PlaceComment(Nominee nominee, IFacebookAuthHelper fbAuth, string text);
    }
}
