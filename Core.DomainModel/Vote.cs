﻿using System;

namespace Core.DomainModel
{
    public class Vote
    {
        public int Id { get; set; }
        public DateTime Created { get; set; }

        // The nominee to receive the vote
        public Nominee Nominee { get; set; }
        public Guid NomineeGuid { get; set; }

        // The user who cast the vote
        public string FacebookUserId { get; set; }

        public Vote()
        {
            Created = DateTime.Now;
        }
    }
}
