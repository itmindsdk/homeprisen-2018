﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Core.DomainModel
{
    public class Nominee
    {
        public Guid Guid { get; set; }
        public int Id { get; set; }
        public int RandomToken { get; set; }

        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string Zipcode { get; set; }
        public string City { get; set; }
        public string Description { get; set; }

        // Whether or not the user has been accepted by the nominated person himself
        public bool IsAccepted { get; set; }

        // Amount of votes the nominee has received from unique voters
        public int VoteCount { get; set; }

        // Rank is based on VoteCount and other nominee's VoteCount.
        // A rank of 1 is a leading nominee. The less VoteCount the
        // highter Rank will be
        public int Rank { get; set; }

        // The person who nominated
        public string NominatorName { get; set; }
        public string NominatorEmail { get; set; }
        public string NominatorPhone { get; set; }

        // When the nomination was received
        public DateTime Nominated { get; set; }

        // When the nomination was accepted
        public DateTime Accepted { get; set; }

        // The contest to be nominated in
        public virtual Contest Contest { get; set; }
        public int ContestId { get; set; } // TODO : Check if this works correct

        // Picture to render posters etc. from
        public virtual Picture Picture { get; set; }

        // A list of users who voted on this nominee
        public virtual ICollection<Vote> Votes { get; set; }

        // Even though a category class exists we are not mapping directly to it.
        // This decision has been made to avoid corrupting data in case the categories change later
        public string Category { get; set; }

        public string Municipality { get; set; }

        // An URL-friendly string
        public string Slug
        {
            get
            {
                var value = Name;

                //value = Regex.Replace(value, @"\s+", "-");
                value = Regex.Replace(value, @"[^A-Za-z0-9_\.\-]+", "-");
                value = Regex.Replace(value, @"\-+", "-");

                return value;
            }
        }

        public Nominee()
        {
            Guid = Guid.NewGuid();
            IsAccepted = false;
            VoteCount = 0;
            Rank = 0;
            RandomToken = (new Random()).Next(0, 100);
        }

        public bool HasPicture()
        {
            return Picture != null;
        }
    }

    public class IldsjaelNominee : Nominee
    {
        public string Title { get; set; }
    }

    public class ForeningNominee : Nominee
    {
        public ForeningType ForeningType { get; set; }
        public int MemberCount { get; set; }
        public string ResponsibleName { get; set; }
    }
}
