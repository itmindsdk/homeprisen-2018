﻿using System;

namespace Core.DomainModel
{
    public class Picture
    {
        public Guid Guid { get; set; }
        public string FileName { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }

        public Picture()
        {
            Guid = Guid.NewGuid();
        }

        public Picture(Guid guid)
        {
            Guid = guid;
        }
    }
}
