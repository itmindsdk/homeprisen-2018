﻿namespace Core.DomainModel
{
    public class Category
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
