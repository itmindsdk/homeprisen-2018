﻿
namespace Core.DomainModel
{
    public class Setting
    {
        public string Key { get; set; }
    }

    public class TextSetting : Setting
    {
        public string Value { get; set; }
    }

    public class ContestTypeSeting : Setting
    {
        public ContestType ContestType { get; set; }
    }

    public class BoolSetting : Setting
    {
        public bool State { get; set; }
    }
}
