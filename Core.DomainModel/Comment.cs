﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Core.DomainModel
{
    public class Comment
    {
        public int Id { get; set; }
        public DateTime Submitted { get; set; }

        // The nominee to receive the vote
        public Nominee Nominee { get; set; }
        public Guid NomineeId { get; set; }

        // The user who cast the vote
        public string FacebookUserId { get; set; }

        public string Name { get; set; }
        public string City { get; set; }
        public string Text { get; set; }

        public Comment()
        {
            Submitted = DateTime.Now;;
        }
    }
}
