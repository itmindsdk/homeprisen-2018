﻿namespace Core.DomainModel
{
    public class Realtor
    {
        public string Name { get; set; }
        public string Email { get; set; }
    }
}
