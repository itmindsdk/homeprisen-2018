﻿using System;
using System.Collections.Generic;

namespace Core.DomainModel
{
    public enum ContestType
    {
        Ildsjael,
        Forening,
    }

    public class Contest
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime Starts { get; set; }
        public DateTime Ends { get; set; }
        public bool IsActive { get; set; }

        public ContestType Type { get; set; }

        // A list of nominees in the contest
        public ICollection<Nominee> Nominees { get; set; }
    }
}
