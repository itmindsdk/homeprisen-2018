# This repo has been cloned from [HomePrisen 2017](https://bitbucket.org/itmindsdk/homeprisen-2017/src) as it is a continuation of that. But a breaking change to the data model has made it a seperate project.

mvc-onion-template
==================

MVC template using Onion architecture

![Guiding principles](http://www.matthidinger.com/images/www_matthidinger_com/Windows-Live-Writer/ff0d136aee1f_88EA/image_2.png)