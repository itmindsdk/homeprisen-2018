﻿using Core.DomainModel;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace Infrastructure.DataAccess
{
    public class ApplicationContext : IdentityDbContext<ApplicationUser>
    {
        // throwIfV1Schema is used when upgrading Identity in a database from 1 to 2.
        // It's a one time thing and can be safely removed.
        public ApplicationContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationContext Create()
        {
            return new ApplicationContext();
        }

        // Define you conceptual model here. Entity Framework will include these types and all their references.
        // There are 3 different inheritance strategies:
        // * Table per Hierarchy (TPH) (default): http://weblogs.asp.net/manavi/inheritance-mapping-strategies-with-entity-framework-code-first-ctp5-part-1-table-per-hierarchy-tph
        // * Table per Type (TPT): http://weblogs.asp.net/manavi/inheritance-mapping-strategies-with-entity-framework-code-first-ctp5-part-2-table-per-type-tpt
        // * Table per Concrete class (TPC): http://weblogs.asp.net/manavi/inheritance-mapping-strategies-with-entity-framework-code-first-ctp5-part-3-table-per-concrete-type-tpc-and-choosing-strategy-guidelines

        public IDbSet<Contest> Contests { get; set; }
        public IDbSet<Picture> Pictures { get; set; }
        public IDbSet<Vote> Votes { get; set; }
        public IDbSet<Nominee> Nominees { get; set; }
        public IDbSet<Comment> Comments { get; set; }
        public IDbSet<Setting> Settings { get; set; }
        public IDbSet<Category> Categories { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // The DateTime type in .NET has the same range and precision as datetime2 in SQL Server.
            // Configure DateTime type to use SQL server datetime2 instead.
            modelBuilder.Properties<DateTime>().Configure(c => c.HasColumnType("datetime2"));

            // Consider setting max length on string properties.
            // http://dba.stackexchange.com/questions/48408/ef-code-first-uses-nvarcharmax-for-all-strings-will-this-hurt-query-performan
            modelBuilder.Properties<string>().Configure(c => c.HasMaxLength(100));

            //Descriptions must be text without limit (ntext)
            modelBuilder.Entity<Nominee>()
                .Property(p => p.Description)
                .IsMaxLength();
            modelBuilder.Entity<Comment>()
                .Property(p => p.Text)
                .IsMaxLength();
            modelBuilder.Entity<TextSetting>()
                .Property(p => p.Value)
                .IsMaxLength();

            // Cascade delete nominees when deleting a contest
            modelBuilder.Entity<Nominee>()
                .HasRequired(p => p.Contest)
                .WithMany()
                .HasForeignKey(p => p.ContestId)
                .WillCascadeOnDelete(true);

            // Cascade delete comments when deleting a nominee
            modelBuilder.Entity<Comment>()
                .HasRequired(p => p.Nominee)
                .WithMany()
                .HasForeignKey(p => p.NomineeId)
                .WillCascadeOnDelete(true);

            // Cascade delete votes when deleting a nominee
            modelBuilder.Entity<Vote>()
                .HasRequired(p => p.Nominee)
                .WithMany(x => x.Votes)
                .HasForeignKey(p => p.NomineeGuid)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Nominee>().HasKey(x => x.Guid);
            modelBuilder.Entity<Nominee>().Property(x => x.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<Picture>().HasKey(x => x.Guid);
            modelBuilder.Entity<Setting>().HasKey(x => x.Key);

            base.OnModelCreating(modelBuilder);
        }
    }
}
