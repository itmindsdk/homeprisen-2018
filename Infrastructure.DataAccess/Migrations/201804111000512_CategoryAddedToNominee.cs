namespace Infrastructure.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CategoryAddedToNominee : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Nominees", "Category", c => c.String(maxLength: 100));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Nominees", "Category");
        }
    }
}
