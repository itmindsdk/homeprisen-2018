using System.Collections.Generic;
using Core.DomainModel;

namespace Infrastructure.DataAccess.Migrations
{
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Infrastructure.DataAccess.ApplicationContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ApplicationContext context)
        {
            if (!context.Users.Any(u => u.UserName == "Test"))
            {
                var userStore = new UserStore<ApplicationUser>(context);
                var userManager = new UserManager<ApplicationUser>(userStore);
                var userToInsert = new ApplicationUser { UserName = "Test", Email = "test@test" };
                const string password = "test123";
                var identityResult = userManager.Create(userToInsert, password);
                if (!identityResult.Succeeded)
                {
                    throw new Exception(string.Join(", ", identityResult.Errors));
                }
            }

            if (!context.Contests.Any())
            {
                var contestForening = new Contest
                {
                    Id = 2,
                    Name = "HomePrisen 2018 Test",
                    Starts = new DateTime(2018, 1, 1),
                    Ends = new DateTime(2019, 1, 1),
                    IsActive = true,
                    Type = ContestType.Forening,
                };

                context.Contests.AddOrUpdate(p => p.Id, contestForening);

                var bynavne = new List<string>
                {
                    "Aarhus",
                    "K�benhavn",
                    "Randers",
                    "Vejle",
                    "Silkeborg",
                    "Viborg",
                    "Roskilde",
                    "Odense",
                    "Aalborg",
                    "Kolding",
                    "S�nderborg",
                    "Skanderborg",
                    "Horsens",
                    "Herning",
                    "Skagen",
                    "Fredericia",
                    "Hadsten",
                    "Lang�",
                    "Esbjerg",
                    "Holstebro",
                    "Ringk�bing",
                    "N�stved",
                    "Nyborg",
                    "Middelfart",
                    "Hammel",
                    "Vojens",
                    "Hobro",
                };

                var foreningName = new List<string>
                {
                    "Gymnastikforeningen GYM",
                    "Skakklub for kloge hjerner",
                    "Hesteheste er heste hest"
                };

                var picture = new Picture
                {
                    Guid = new Guid("2fd1f47a-dd5a-4f34-a1a5-26df1997d2dd")
                };
                context.Pictures.AddOrUpdate(p => p.Guid, picture);

                for (var i = 0; i < 12; i++)
                {
                    var nominee = new ForeningNominee
                    {
                        Name = $"{foreningName.OrderBy(x => Guid.NewGuid()).FirstOrDefault()}",
                        ForeningType = ForeningType.Forening,
                        MemberCount = 123,
                        Email = "Email" + 1,
                        City = bynavne.OrderBy(x => Guid.NewGuid()).FirstOrDefault(),
                        Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris fermentum tincidunt efficitur. Sed quis consequat nisl. Maecenas commodo diam enim, vel varius lacus ultricies lobortis. Morbi congue nibh mollis purus eleifend, vitae posuere ex egestas. Nunc aliquam purus ut metus ultrices, vitae molestie purus sollicitudin. Praesent sodales nulla nulla, et.",
                        IsAccepted = true,
                        Nominated = DateTime.Now,
                        Accepted = DateTime.Now,
                        Contest = contestForening,
                        Picture = picture,
                        Rank = 1,
                    };

                    context.Nominees.AddOrUpdate(p => p.Guid, nominee);

                    context.Comments.AddOrUpdate(c => c.Id, new Comment
                    {
                        NomineeId = nominee.Guid,
                        FacebookUserId = "100002363812616",
                        Name = "Jakob Kristensen",
                        City = "Aarhus",
                        Text = "Anders burde v�re �rets ildsj�l fordi han altid har det ekstra overskud der skal til for at v�re behj�lpsom b�de...",
                    });
                    context.Comments.AddOrUpdate(c => c.Id, new Comment
                    {
                        NomineeId = nominee.Guid,
                        FacebookUserId = "110229142706545",
                        Name = "Richard fergieman",
                        City = "Aarhus",
                        Text = "Anders burde v�re �rets ildsj�l fordi han altid har det ekstra overskud der skal til for at v�re behj�lpsom b�de...",
                    });
                    context.Comments.AddOrUpdate(c => c.Id, new Comment
                    {
                        NomineeId = nominee.Guid,
                        FacebookUserId = "137968793262141",
                        Name = "Will fergieson",
                        City = "Aarhus",
                        Text = "Anders burde v�re �rets ildsj�l fordi han altid har det ekstra overskud der skal til for at v�re behj�lpsom b�de...",
                    });
                    context.Comments.AddOrUpdate(c => c.Id, new Comment
                    {
                        NomineeId = nominee.Guid,
                        FacebookUserId = "159370137787068",
                        Name = "Karen Smitchwitz",
                        City = "Aarhus",
                        Text = "Anders burde v�re �rets ildsj�l fordi han altid har det ekstra overskud der skal til for at v�re behj�lpsom b�de...",
                    });
                    context.Comments.AddOrUpdate(c => c.Id, new Comment
                    {
                        NomineeId = nominee.Guid,
                        FacebookUserId = "180795152309173",
                        Name = "Susan Lausen",
                        City = "Aarhus",
                        Text = "Anders burde v�re �rets ildsj�l fordi han altid har det ekstra overskud der skal til for at v�re behj�lpsom b�de...",
                    });
                    context.Comments.AddOrUpdate(c => c.Id, new Comment
                    {
                        NomineeId = nominee.Guid,
                        FacebookUserId = "113966375665627",
                        Name = "Barbare Fallerescu",
                        City = "Aarhus",
                        Text = "Anders burde v�re �rets ildsj�l fordi han altid har det ekstra overskud der skal til for at v�re behj�lpsom b�de...",
                    });
                    context.Comments.AddOrUpdate(c => c.Id, new Comment
                    {
                        NomineeId = nominee.Guid,
                        FacebookUserId = "122292348164941",
                        Name = "Donna Bushakescu",
                        City = "Aarhus",
                        Text = "Anders burde v�re �rets ildsj�l fordi han altid har det ekstra overskud der skal til for at v�re behj�lpsom b�de...",
                    });
                    context.Comments.AddOrUpdate(c => c.Id, new Comment
                    {
                        NomineeId = nominee.Guid,
                        FacebookUserId = "155733958150910",
                        Name = "Bubber Badekarsen",
                        City = "Aarhus",
                        Text = "Anders burde v�re �rets ildsj�l fordi han altid har det ekstra overskud der skal til for at v�re behj�lpsom b�de...",
                    });
                    context.Comments.AddOrUpdate(c => c.Id, new Comment
                    {
                        NomineeId = nominee.Guid,
                        FacebookUserId = "163871224003256",
                        Name = "Lars Larsesen",
                        City = "Aarhus",
                        Text = "Anders burde v�re �rets ildsj�l fordi han altid har det ekstra overskud der skal til for at v�re behj�lpsom b�de...",
                    });
                    context.Comments.AddOrUpdate(c => c.Id, new Comment
                    {
                        NomineeId = nominee.Guid,
                        FacebookUserId = "107797432946317",
                        Name = "Pikachu Pokemonsen",
                        City = "Aarhus",
                        Text = "Anders burde v�re �rets ildsj�l fordi han altid har det ekstra overskud der skal til for at v�re behj�lpsom b�de...",
                    });
                }
            }

            if (!context.Categories.Any())
            {
                string[] categories = new string[]
                {
                    "Idr�tsforeninger og fritidsklubber",
                    "Kulturtilbud og m�desteder",
                    "Parker, naturomr�der og dyreliv",
                    "Grupper og f�llesskaber",
                    "Sociale projekter",
                    "Festivaler og kulturarrangementer",
                    "Ildsj�le og borgerforeninger"
                };
                foreach (var category in categories)
                {
                    context.Categories.Add(new Category() {Name = category});
                }
            }
        }
    }
}
