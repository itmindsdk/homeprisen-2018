namespace Infrastructure.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addBoolSetting : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Settings", "State", c => c.Boolean());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Settings", "State");
        }
    }
}
