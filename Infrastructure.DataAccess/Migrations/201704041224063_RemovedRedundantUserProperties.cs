namespace Infrastructure.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class RemovedRedundantUserProperties : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.AspNetUsers", "Name");
            DropColumn("dbo.AspNetUsers", "Phone");
        }

        public override void Down()
        {
            AddColumn("dbo.AspNetUsers", "Phone", c => c.String(maxLength: 100));
            AddColumn("dbo.AspNetUsers", "Name", c => c.String(maxLength: 100));
        }
    }
}
