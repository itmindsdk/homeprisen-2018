namespace Infrastructure.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class AddedIdToNominee : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Nominees", "Id", c => c.Int(nullable: false, identity: true));
        }

        public override void Down()
        {
            DropColumn("dbo.Nominees", "Id");
        }
    }
}
