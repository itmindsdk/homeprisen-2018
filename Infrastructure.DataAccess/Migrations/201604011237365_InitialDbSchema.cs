namespace Infrastructure.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class InitialDbSchema : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Comments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Submitted = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        NomineeId = c.Guid(nullable: false),
                        FacebookUserId = c.String(maxLength: 100),
                        Name = c.String(maxLength: 100),
                        City = c.String(maxLength: 100),
                        Text = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Nominees", t => t.NomineeId, cascadeDelete: true)
                .Index(t => t.NomineeId);

            CreateTable(
                "dbo.Nominees",
                c => new
                    {
                        Guid = c.Guid(nullable: false),
                        Id = c.Int(nullable: false, identity: true),
                        RandomToken = c.Int(nullable: false),
                        Name = c.String(maxLength: 100),
                        Email = c.String(maxLength: 100),
                        Phone = c.String(maxLength: 100),
                        Address = c.String(maxLength: 100),
                        Zipcode = c.String(maxLength: 100),
                        City = c.String(maxLength: 100),
                        Description = c.String(),
                        IsAccepted = c.Boolean(nullable: false),
                        VoteCount = c.Int(nullable: false),
                        Rank = c.Int(nullable: false),
                        NominatorName = c.String(maxLength: 100),
                        NominatorEmail = c.String(maxLength: 100),
                        NominatorPhone = c.String(maxLength: 100),
                        Nominated = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        Accepted = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        ContestId = c.Int(nullable: false),
                        ForeningType = c.Int(),
                        MemberCount = c.Int(),
                        ResponsibleName = c.String(maxLength: 100),
                        Title = c.String(maxLength: 100),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                        Contest_Id = c.Int(),
                        Picture_Guid = c.Guid(),
                    })
                .PrimaryKey(t => t.Guid)
                .ForeignKey("dbo.Contests", t => t.Contest_Id)
                .ForeignKey("dbo.Contests", t => t.ContestId, cascadeDelete: true)
                .ForeignKey("dbo.Pictures", t => t.Picture_Guid)
                .Index(t => t.ContestId)
                .Index(t => t.Contest_Id)
                .Index(t => t.Picture_Guid);

            CreateTable(
                "dbo.Contests",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 100),
                        Starts = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        Ends = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        IsActive = c.Boolean(nullable: false),
                        Type = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);

            CreateTable(
                "dbo.Pictures",
                c => new
                    {
                        Guid = c.Guid(nullable: false),
                        FileName = c.String(maxLength: 100),
                        Width = c.Int(nullable: false),
                        Height = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Guid);

            CreateTable(
                "dbo.Votes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Created = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        NomineeGuid = c.Guid(nullable: false),
                        FacebookUserId = c.String(maxLength: 100),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Nominees", t => t.NomineeGuid, cascadeDelete: true)
                .Index(t => t.NomineeGuid);

            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 100),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");

            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 100),
                        RoleId = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);

            CreateTable(
                "dbo.Settings",
                c => new
                    {
                        Key = c.String(nullable: false, maxLength: 100),
                        ContestType = c.Int(),
                        Value = c.String(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Key);

            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 100),
                        Name = c.String(maxLength: 100),
                        Email = c.String(maxLength: 256),
                        Phone = c.String(maxLength: 100),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(maxLength: 100),
                        SecurityStamp = c.String(maxLength: 100),
                        PhoneNumber = c.String(maxLength: 100),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(precision: 7, storeType: "datetime2"),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");

            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 100),
                        ClaimType = c.String(maxLength: 100),
                        ClaimValue = c.String(maxLength: 100),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);

            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 100),
                        ProviderKey = c.String(nullable: false, maxLength: 100),
                        UserId = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);

        }

        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.Comments", "NomineeId", "dbo.Nominees");
            DropForeignKey("dbo.Votes", "NomineeGuid", "dbo.Nominees");
            DropForeignKey("dbo.Nominees", "Picture_Guid", "dbo.Pictures");
            DropForeignKey("dbo.Nominees", "ContestId", "dbo.Contests");
            DropForeignKey("dbo.Nominees", "Contest_Id", "dbo.Contests");
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.Votes", new[] { "NomineeGuid" });
            DropIndex("dbo.Nominees", new[] { "Picture_Guid" });
            DropIndex("dbo.Nominees", new[] { "Contest_Id" });
            DropIndex("dbo.Nominees", new[] { "ContestId" });
            DropIndex("dbo.Comments", new[] { "NomineeId" });
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.Settings");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.Votes");
            DropTable("dbo.Pictures");
            DropTable("dbo.Contests");
            DropTable("dbo.Nominees");
            DropTable("dbo.Comments");
        }
    }
}
