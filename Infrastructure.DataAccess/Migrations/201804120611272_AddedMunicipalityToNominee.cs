namespace Infrastructure.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedMunicipalityToNominee : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Nominees", "Municipality", c => c.String(maxLength: 100));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Nominees", "Municipality");
        }
    }
}
